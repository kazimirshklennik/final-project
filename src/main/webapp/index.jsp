<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<fmt:setLocale value="${locale}" />
<!DOCTYPE html>

<html>
<head>
    <title>RentCar</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css" />
<script>
    window.onload = function () {
        document.onkeydown = function (ev) {
            return ev.which || ev.keyCode != 116;
        }
    }
</script>
</head>
<body class=w3-light-grey">

<jsp:include page="jsp/service/header.jsp" />
<jsp:include page="jsp/service/car-reserve.jsp" />
<jsp:include page="jsp/service/info-resgister.jsp" />
<jsp:include page="jsp/error/error-window.jsp" />
<jsp:include page="jsp/service/footer.jsp" />


</body>
</html>


