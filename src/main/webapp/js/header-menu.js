$(document).ready(function () {
    $("#simple-account-dropdown > .account").click(function () {
        $("#simple-account-dropdown > .dropdown").fadeToggle("fast", function () {
            if ($(this).css('display') == "none")
                $("#simple-account-dropdown > .account").removeClass("active");
            else
                $("#simple-account-dropdown > .account").addClass("active");
        });
    });
});