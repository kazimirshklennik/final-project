<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    <title>Profile</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.min.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>

</head>
<body>
<jsp:include page="global/message/window-send-email.jsp"/>
<jsp:include page="global/message/window-send-message.jsp"/>
<jsp:include page="../service/header.jsp"/>


<div class="w3-container w3-padding">
    <div class="w3-card-4">
        <div id="main">
            <div class="row" id="real-estates-detail">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <header class="panel-title">
                                <c:if test="${userprofile.lockStatus == true}">
                                    <div class="text-message-error">
                                        <fmt:message key="page.blocked" bundle="${rb}"/>
                                    </div>
                                </c:if>

                                <div class="text-center">
                                    <strong> ${userprofile.login}(${userprofile.role})
                                        <c:if test="${userprofile.lockStatus == false}">
                                            <c:if test="${!empty role}">
                                                <a href="#win1" class="button9">
                                                    <img src="${pageContext.request.contextPath}/img/icons/sendmessage.png"></a>
                                                <a href="#win2" class="button9">
                                                    <img src="${pageContext.request.contextPath}/img/icons/sendemail.png"></a>

                                            </c:if>
                                        </c:if>
                                    </strong>
                                </div>

                            </header>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <img class="rounded-foto"
                                     src="${pageContext.request.contextPath}/image?command=image-user&idUser=${userprofile.idUser}"/>
                                <
                                <h3>${userprofile.info.firstName} ${userprofile.info.lastName} </h3>
                                <small class="label label-warning">${userprofile.email}</small>
                                <jsp:include page="global/social/social-network-simple.jsp"/>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div id="myTabContent" class="tab-content">
                                <jsp:include page="global/message-info.jsp"/>
                                <hr>
                                <div class="tab-pane fade active in" id="detail">
                                    <h4><fmt:message key="page.info-profile"
                                                     bundle="${rb}"/></h4>
                                    <table class="table table-th-block">

                                        <tr>

                                            <td class="active"><fmt:message key="page.date.of.registration"
                                                                            bundle="${rb}"/>:
                                            </td>
                                            <td>${userprofile.info.dateRegistration}</td>
                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.lasta.activity"
                                                                            bundle="${rb}"/>:
                                            </td>
                                            <td>${userprofile.info.lastActivity}</td>
                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.country" bundle="${rb}"/>:
                                            </td>
                                            <td>${userprofile.info.country}</td>
                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.city" bundle="${rb}"/>:</td>
                                            <td>${userprofile.info.city}</td>
                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.sex" bundle="${rb}"/>:</td>
                                            <td>${userprofile.info.sex}</td>
                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.age" bundle="${rb}"/>:</td>
                                            <td><c:if test="${userprofile.info.age!=0}">
                                                ${userprofile.info.age}
                                            </c:if></td>

                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.birthday" bundle="${rb}"/>:</td>
                                            <td>${userprofile.info.birthday}</td>
                                        </tr>
                                        <tr>
                                            <td class="active"><fmt:message key="page.description" bundle="${rb}"/>:

                                            </td>
                                            <td>${userprofile.info.userText}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div><!-- /.main -->
<jsp:include page="../service/footer.jsp"/>
</body>
</html>
