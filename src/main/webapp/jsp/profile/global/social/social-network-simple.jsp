<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel='stylesheet prefetch'
      href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/soc_icon.css">

<div class="container2">
    <ul class="submenu">
        <c:forEach var="socialNetwork" items="${userprofile.info.socialNetworks}">
            <c:choose>
                <c:when test="${socialNetwork.type == 'FACEBOOK'}">
                    <li><a href="${socialNetwork.url}"><i class="fa fa-facebook"></i></a></li>
                </c:when>
                <c:when test="${socialNetwork.type == 'TWITTER'}">
                    <li><a href="${socialNetwork.url}"><i class="fa fa-twitter"></i></a></li>
                </c:when>
                <c:when test="${socialNetwork.type == 'INSTAGRAM'}">
                    <li><a href="${socialNetwork.url}"><i class="fa fa-instagram"></i></a></li>
                </c:when>
                <c:when test="${socialNetwork.type == 'GOOGLE'}">
                    <li><a href="${socialNetwork.url}"><i class="fa fa-google-plus"></i></a></li>
                </c:when>
                <c:when test="${socialNetwork.type == 'VK_COM'}">
                    <li><a href="${socialNetwork.url}"><i class="fa fa-vk"></i></a></li>
                </c:when>
            </c:choose>
        </c:forEach>
    </ul>
</div>
