<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="addsocialnetwork">

    <table class="table table-th-block">

        <table class="table table-th-block">
            <thead>
            <tr>
                <td class="active"><fmt:message key="page.social.network" bundle="${rb}"/></td>
                <td class="active"><fmt:message key="page.url" bundle="${rb}"/></td>
                <td class="active"><fmt:message key="page.action" bundle="${rb}"/></td>

            </tr>
            <thead>
            <c:forEach var="socialNetwork" items="${user.info.socialNetworks}">
            <tr>
                <td>
                    <ul class="menu">
                    <c:choose>
                        <c:when test="${socialNetwork.type == 'FACEBOOK'}">
                       <li> <a href="${socialNetwork.url}"><i class="fa fa-facebook"></i></a></li>
                        </c:when>
                        <c:when test="${socialNetwork.type == 'TWITTER'}">
                            <li>  <a href="${socialNetwork.url}"><i class="fa fa-twitter"></i></a></li>
                        </c:when>
                        <c:when test="${socialNetwork.type == 'INSTAGRAM'}">
                            <li>  <a href="${socialNetwork.url}"><i class="fa fa-instagram"></i></a></li>
                        </c:when>
                        <c:when test="${socialNetwork.type == 'GOOGLE'}">
                            <li>  <a href="${socialNetwork.url}"><i class="fa fa-google-plus"></i></a></li>
                        </c:when>
                        <c:when test="${socialNetwork.type == 'VK_COM'}">
                            <li><a href="${socialNetwork.url}"><i class="fa fa-vk"></i></a></li>
                        </c:when>
                    </c:choose>
                    </ul>
                </td>
                <td><c:out value="${socialNetwork.url}"/></td>
                <td>
                    <form action="do" method="post" role="form">
                        <input type="hidden" name="idSocialNetwork" value="${socialNetwork.idSocialNetwork}"/>
                        <button type="submit" name="command" value="delete-social" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                            <img src="${pageContext.request.contextPath}/img/icons/delete.png">
                        </button>
                    </form>
                </td>
            </tr>
            </c:forEach>
        </table>



    <form action="do" method="post" role="form" style="display: inline-block;">
        <table class="table table-th-block">
                <td class="active"><fmt:message key="page.url" bundle="${rb}"/>: </td>
                <td><input type="url" name="urlsocial" class="form-control rounded" style="width: 200px;" required="required" maxlength="80"></td>
            <td>
                <button type="submit" name="command" value="add-social"
                        class="btn btn-success" data-original-title=""
                        title="" ><fmt:message key="page.add" bundle="${rb}"/>
                </button>
            </td>
            </tr>
        </table>

    </form>

    <div class="panel-body">
        <ul class="nav nav-pills">
            <li class=""><a href="#detail" data-toggle="tab"><img src="${pageContext.request.contextPath}/img/icons/back.png"/> </a></li>
        </ul>
    </div>

</div>

