<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="changepassword">
    <form action="do" method="post" role="form">
        <table class="table table-th-block">

            <tr>
                <td class="active"><fmt:message key="page.old.password" bundle="${rb}"/>:
                </td>
                <td><input type="password" name="oldpassword" class="form-control rounded" ></td>
            </tr>
            <tr>
                <td class="active"><fmt:message key="page.new.password" bundle="${rb}"/>:
                </td>
                <td>
                    <input type="password" name="newpassword" class="form-control rounded" required minlength="6" maxlength="16">

                </td>
            </tr>

            <tr>
                <td class="active"><fmt:message key="page.confirm.password" bundle="${rb}"/>:
                </td>
                <td>

                    <input type="password" name="confirm-password" class="form-control rounded" required minlength="6" maxlength="16">
                </td>
            </tr>

        </table>

        <div class="form-group">
            <button type="submit" name="command" value="update-password"
                    class="btn btn-success" data-original-title=""
                    title=""><fmt:message key="page.button.update" bundle="${rb}"/>
            </button>

        </div>

    </form>

</div>

