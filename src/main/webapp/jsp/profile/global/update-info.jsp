<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="updateinfo">
    <table class="table table-th-block">
        <form action="do" method="post" role="form" enctype="multipart/form-data">

            <tr>
                <td class="active"><fmt:message key="page.first.name" bundle="${rb}"/>:
                </td>
                <td><input type="text" name="firstname" class="form-control rounded" value="${user.info.firstName}" required="required" maxlength="10"></td>
            </tr>
            <tr>
                <td class="active"><fmt:message key="page.last.name" bundle="${rb}"/>:
                </td>
                <td><input type="text" name="lastname" class="form-control rounded" value="${user.info.lastName}" required="required" maxlength="10"></td>
            </tr>

            <tr>
                <td class="active"><fmt:message key="page.country" bundle="${rb}"/>:
                </td>
                <td><input type="text" name="country" class="form-control rounded" value="${user.info.country}" required="required" maxlength="10"></td>
            </tr>
            <tr>
                <td class="active"><fmt:message key="page.city" bundle="${rb}"/>:</td>
                <td><input type="text" name="city" class="form-control rounded" value="${user.info.city}" required="required" maxlength="10"></td>
            </tr>
            <tr>
                <td class="active"><fmt:message key="page.sex" bundle="${rb}"/>:</td>
                <td>
                    <input type="text" name="sex" class="form-control rounded" value="${user.info.sex}" required="required" maxlength="10">
                </td>
            </tr>

            <tr>
                <td class="active"><fmt:message key="page.birthday" bundle="${rb}"/>:</td>
                <td><input type="date" name="birthday" class="form-control rounded" value="${user.info.birthday}" required="required"></td>
            </tr>

            <tr>
                <td class="active"><fmt:message key="page.description" bundle="${rb}"/>:</td>
                <td><input type="text" name="textinfo" class="form-control rounded" value="${user.info.userText}" required="required" maxlength="200"></td>
            </tr>

            <tr>
                <td class="active"><fmt:message key="page.foto" bundle="${rb}"/>:</td>
                <td>
                    <input type="file" name="image" multiple accept="image/*" style="width: 250px;">
                </td>
            </tr>

            <tr>
                <td>
                    <div class="form-group">
                        <button type="submit" name="command" value="update-info"
                                class="btn btn-success" data-original-title=""
                                title=""><fmt:message key="page.button.update" bundle="${rb}"/>
                        </button>
                    </div>
                </td>
            </tr>

        </form>
    </table>

</div>