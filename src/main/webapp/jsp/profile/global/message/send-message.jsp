<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="contactsite">
    <form action="do" method="post" role="form">
        <div class="form-group">
            <label><fmt:message
                    key="page.recipient.login" bundle="${rb}"/></label>
            <input type="text" name="loginrecipient" class="form-control rounded"/>
        </div>
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" required="required"> <fmt:message
                        key="page.agree.with.the.conditions" bundle="${rb}" />
                </label>
            </div>
        </div>
        <div class="form-group">
            <label><fmt:message key="page.text.message" bundle="${rb}"/></label>
            <textarea class="form-control rounded"
                      style="height: 100px;width: 80%;" type="text" name="textmessage"></textarea>

        </div>
        <div class="form-group">
            <button type="submit" name="command" value="send-message"
                    class="btn btn-success" data-original-title=""
                    title=""><fmt:message key="page.send.message" bundle="${rb}"/>
            </button>

        </div>
    </form>
    <div class="panel-body">
        <ul class="nav nav-pills">
            <li><a href="#detail" data-toggle="tab"><img src="${pageContext.request.contextPath}/img/icons/back.png"/> </a></li>
        </ul>
    </div>
</div>
