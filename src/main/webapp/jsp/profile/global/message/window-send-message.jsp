<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css" />

<a href="#x" class="overlay" id="win1"></a>
<div class="popup">
    <form action="do" method="post" role="form">
        <div class="form-group">
            <input type="hidden" name="loginrecipient" value="${userprofile.login}"/>
            <div class="checkbox">
                <label>
                    <input type="checkbox" required="required"> <fmt:message
                        key="page.agree.with.the.conditions" bundle="${rb}" />
                </label>
            </div>
        </div>
        <div class="form-group">
            <label><fmt:message key="page.text.message" bundle="${rb}"/></label>
            <textarea class="form-control rounded"
                      style="height: 100px;width: 100%" type="text" name="textmessage"></textarea>

        </div>
        <div class="form-group">
            <button type="submit" name="command" value="send-message"
                    class="btn btn-success" data-original-title=""
                    title=""><fmt:message key="page.send.message" bundle="${rb}"/>
            </button>

        </div>
    </form>
    <a class="close"title="<fmt:message key="page.close" bundle="${rb}"/>" href="#close"></a>
</div>