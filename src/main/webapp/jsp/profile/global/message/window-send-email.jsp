<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css" />

<a href="#x" class="overlay" id="win2"></a>
<div class="popup">
    <form action="do" method="post" role="form">
    <div class="form-group">
        <label><fmt:message key="page.subject" bundle="${rb}"/></label>
        <input type="text" class="form-control rounded" name="subject"/>
    </div>
        <input type="hidden" name="loginrecipient" value="${userprofile.login}"/>
    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox" required="required"> <fmt:message
                    key="page.agree.with.the.conditions" bundle="${rb}"/>
            </label>
        </div>
    </div>
    <div class="form-group">
        <label> <fmt:message key="page.text.message" bundle="${rb}"/></label>
        <textarea class="form-control rounded" type="text" name="textmessage"
                  style="height: 100px;width: 100%"></textarea>

    </div>
    <div class="form-group">
        <button type="submit" name="command" value="send-mail"
                class="btn btn-success" data-original-title=""
                title=""><fmt:message key="page.send.message" bundle="${rb}"/>
        </button>
    </div>
    </form>
    </form>
    <a class="close"title="<fmt:message key="page.close" bundle="${rb}"/>" href="#close"></a>
</div>