<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>


<div id="myTabContent" class="tab-content">
    <div class="text-message">
        <script> $('.text-message').delay(2000).animate({'opacity':'0'},500);</script>
        ${sendSuccessfully}
        ${succesfullyUpdated}
        ${succesfullyadded}
        ${successfullydeleted}
        ${deletereserve}
        ${deletesocialnetworkSuccessfully}
    </div>

    <div class="text-message-error">
        <script> $('.text-message-error').delay(2000).animate({'opacity':'0'},500);</script>
        ${errorSendMessage}
        ${unsuccesfullyUpdated}
        ${erroraddcar}
        ${unsuccessfullydeleted}
        ${somethingwentwrong}
        ${userdoesnotexist}
        ${erroraddsocial}
        ${errormaxurlsocialnetwork}
        ${incorrectdata}
        ${incorrectpassword}
    </div>
</div>