<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade active in" id="detail">
    <h4><fmt:message key="page.info-profile"
                     bundle="${rb}"/></h4>
    <table class="table table-th-block">

        <tr>

            <td class="active"><fmt:message key="page.date.of.registration"
                                            bundle="${rb}"/>:
            </td>
            <td>${user.info.dateRegistration}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.lasta.activity"
                                            bundle="${rb}"/>:
            </td>
            <td>${user.info.lastActivity}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.country" bundle="${rb}"/>:
            </td>
            <td>${user.info.country}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.city" bundle="${rb}"/>:</td>
            <td>${user.info.city}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.sex" bundle="${rb}"/>:</td>
            <td>${user.info.sex}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.age" bundle="${rb}"/>:</td>
            <td>
                <c:if test="${user.info.age!=0}">
                    ${user.info.age}</c:if>
            </td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.birthday" bundle="${rb}"/>:</td>
            <td>${user.info.birthday}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.description" bundle="${rb}"/>:
            </td>
            <td>${user.info.userText}</td>
        </tr>
        </tbody>
    </table>
    <div class="panel-body">
        <ul id="myTab" class="nav nav-pills">
            <li class=""><a href="#updateinfo" data-toggle="tab"><fmt:message
                    key="page.change.info" bundle="${rb}"/></a></li>
            <li class=""><a href="#changepassword" data-toggle="tab"><fmt:message
                    key="page.change.password" bundle="${rb}"/></a></li>
        </ul>
    </div>
</div>