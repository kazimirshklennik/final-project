<div class="col-lg-4 col-md-4 col-xs-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <header class="panel-title">
                <div class="text-center">
                    <strong>${user.login}(${user.role})</strong>
                </div>
            </header>
        </div>
        <div class="panel-body">
            <div class="text-center">
                <img class="rounded-foto" src="${pageContext.request.contextPath}/image?command=image-user&idUser=${user.idUser}" />
                <h3>${user.info.firstName} ${user.info.lastName} </h3>
                <small class="label label-warning">${user.email}</small>
                <jsp:include page="social/social-network-user.jsp" />
            </div>
        </div>
    </div>

</div>




