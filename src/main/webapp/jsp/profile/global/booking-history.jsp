<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="history">
    <table class="table table-th-block">
        <thead>
        <tr>
            <td class="active"><fmt:message key="page.car.name" bundle="${rb}"/></td>
            <td class="active"><fmt:message key="page.reserve.date.booking" bundle="${rb}"/></td>
            <td class="active"><fmt:message key="page.reserve.date.return" bundle="${rb}"/></td>
            <td class="active"><fmt:message key="page.reserve.price" bundle="${rb}"/></td>
            <td class="active"><fmt:message key="page.reserve.status" bundle="${rb}"/></td>

        </tr>
        <thead>
        <c:forEach var="reserve" items="${reserveall}">
        <tr>
            <td><c:out value="${reserve.car.name}"/>
                <form action="do" method="post" role="form">
                    <input type="hidden" name="idCar" value="${reserve.car.idCar}"/>
                    <button type="submit" name="command" value="page-info-car"
                            class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                        <img src="${pageContext.request.contextPath}/img/icons/info.png">
                    </button>
                </form>
            </td>
            <td><c:out value="${reserve.booking}"/></td>
            <td><c:out value="${reserve.dReturn}"/></td>
            <td><c:out value="${reserve.totalCost}"/></td>
            <td>
                <c:choose>
                    <c:when test="${reserve.status == true}">
                        <img src="${pageContext.request.contextPath}/img/icons/complete.png">
                    </c:when>
                    <c:otherwise>
                        <form action="do" method="post" role="form">
                            <input type="hidden" name="type" value="simple"/>
                            <input type="hidden" name="idReserve" value="${reserve.idReserve}"/>
                            <button type="submit" name="command" value="delete-reserve"
                                    class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                                <img src="${pageContext.request.contextPath}/img/icons/intheprocess.png">
                            </button>
                        </form>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        </c:forEach>
    </table>

</div>