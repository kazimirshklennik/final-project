<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    <title>Profile</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.min.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/protection-f5.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/social-network-profile-button.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>

    <script>
        window.onload = function () {
            document.onkeydown = function (ev) {
                return ev.which || ev.keyCode != 116;
            }
        }
    </script>
</head>
<body>
<jsp:include page="../service/header.jsp"/>
<div class="w3-container w3-padding">
    <div class="w3-card-4">
        <div id="main">
            <div class="row" id="real-estates-detail">
                <jsp:include page="global/basic-info.jsp"/>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <ul id="myTab" class="nav nav-pills">
                                <li class="active"><a href="#detail" data-toggle="tab"><fmt:message
                                        key="page.about.user" bundle="${rb}"/></a></li>
                                <li class=""><a href="#addsocialnetwork" data-toggle="tab"><fmt:message
                                        key="page.social.network" bundle="${rb}"/></a></li>

                                <li class=""><a href="#contactsite" data-toggle="tab"><fmt:message
                                        key="page.send.message" bundle="${rb}"/></a></li>
                                <li class=""><a href="#history" data-toggle="tab"><fmt:message
                                        key="page.history" bundle="${rb}"/></a></li>

                                <c:if test="${role == 'ADMIN'}">
                                    <li class=""><a href="#admin" data-toggle="tab"><fmt:message
                                            key="page.admin" bundle="${rb}"/></a></li>
                                </c:if>
                            </ul>

                            <div id="myTabContent" class="tab-content">
                                <c:choose>
                                    <c:when test="${!empty role}">
                                        <jsp:include page="global/message-info.jsp"/>
                                        <jsp:include page="global/detail.jsp"/>
                                        <jsp:include page="global/message/send-message.jsp"/>
                                        <jsp:include page="global/update-info.jsp"/>
                                        <jsp:include page="global/change-password.jsp"/>
                                        <jsp:include page="global/booking-history.jsp"/>
                                        <jsp:include page="global/social/add-social-info.jsp"/>
                                        <c:if test="${role == 'ADMIN'}">
                                            <jsp:include page="admin/admin.jsp"/>
                                            <jsp:include page="admin/all-cars.jsp"/>
                                            <jsp:include page="admin/all-users.jsp"/>
                                        </c:if>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../service/footer.jsp"/>
</body>
</html>
