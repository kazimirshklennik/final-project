<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>


<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/protection-f5.js"></script>

<!DOCTYPE html>
<html>
<head>
    <title>Messages</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/messeges.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/messeges.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
    <script src="${pageContext.request.contextPath}/js/protection-f5.js"></script>
</head>
<!--Coded With Love By Mutiullah Samim-->
<body>
<jsp:include page="../service/header.jsp"/>
<div class="container-fluid h-100">
    <div class="row justify-content-center h-100">
        <div class="col-md-4 col-xl-3 chat">
            <div class="card mb-sm-3 mb-md-0 contacts_card">
                <div class="card-header">
                    <div class="input-group">
                    </div>
                </div>
                <div class="card-body contacts_body">
                    <ui class="contacts">
                        <c:forEach var="user" items="${interlocutors}">
                            <form action="do" method="post" role="form">
                                <input type="hidden" name="idInterlocutor" value="${user.idUser}"/>
                                <input type="hidden" name="loginInterlocutor" value="${user.login}"/>
                                <li class="active">
                                    <button type="submit" name="command" value="messages"
                                            class="input-group-text send_btn">
                                        <div class="d-flex bd-highlight">
                                            <div class="img_cont">
                                                <img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${user.idUser}"
                                                     class="rounded-circle user_img">
                                                <c:choose>
                                                    <c:when test="${user.statusOnline}">
                                                        <span class="online_icon"></span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span class="online_icon offline"></span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                            <div class="user_info">
                                                <span>${user.info.firstName}</span>
                                            </div>
                                        </div>

                                    </button>
                                </li>
                            </form>
                        </c:forEach>
                    </ui>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
        <div class="col-md-8 col-xl-6 chat">
            <div class="card">
                <div class="card-header msg_head">
                    <div class="d-flex bd-highlight">
                        <c:choose>
                            <c:when test="${!empty interlocutor.idUser}">
                                <div class="img_cont">
                                    <img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${interlocutor.idUser}"
                                         class="rounded-circle user_img">
                                    <c:choose>
                                        <c:when test="${interlocutor.statusOnline}">
                                            <span class="online_icon"></span>
                                        </c:when>
                                        <c:otherwise>
                                            <span class="online_icon offline"></span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="user_info">
                                    <span>${interlocutor.info.firstName}&nbsp;${interlocutor.info.lastName}</span>
                                </div>
                                <form action="do" method="post" role="form">
                                    <input type="hidden" name="type" value="simple"/>
                                    <input type="hidden" name="login" value="${interlocutor.login}"/>
                                    <div class="video_cam">
                                        <button type="submit" name="command" value="profile"
                                                class="input-group-text send_btn"><i class="fas fa-user-circle"></i>
                                        </button>
                                    </div>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <div class="img_cont">
                                    <img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${idUser}"
                                         class="rounded-circle user_img">
                                    <span class="online_icon"></span>
                                </div>
                                <div class="user_info">
                                    <span>${login}&nbsp;(${role})</span>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="card-body msg_card_body" id="ooo">
                    <c:forEach var="message" items="${messages}">
                        <c:choose>
                            <c:when test="${message.idRecipient==idUser}">

                                <div class="d-flex justify-content-start mb-4">
                                    <div class="img_cont_msg">
                                        <img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${message.idSender}"
                                             class="rounded-circle user_img_msg">
                                    </div>
                                    <div class="msg_cotainer">
                                            ${message.text}
                                        <span class="msg_time">${message.dateString}</span>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="d-flex justify-content-end mb-4">
                                    <div class="msg_cotainer_send">
                                            ${message.text}
                                        <span class="msg_time_send">${message.dateString}</span>
                                    </div>
                                    <div class="img_cont_msg">
                                        <img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${message.idSender}"
                                             class="rounded-circle user_img_msg">
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </div>
                <c:choose>
                    <c:when test="${!empty interlocutor.idUser}">
                        <form action="do" method="GET" role="form">
                            <input type="hidden" name="loginrecipient" value="${interlocutor.login}"/>
                            <input type="hidden" name="type" value="chat"/>
                            <div class="card-footer">
                                <div class="input-group">
                            <textarea name="textmessage" class="form-control type_msg"
                                      placeholder="Type your message..."></textarea>
                                    <div class="input-group-append">
                                        <button type="submit" name="command" value="send-message"
                                                class="input-group-text send_btn"><i class="fas fa-location-arrow"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </c:when>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../service/footer.jsp"/>
</body>
</html>
