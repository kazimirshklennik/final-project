<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="addcar">
    <form action="do" method="post" role="form">
        <table class="table table-th-block">
            <tr>
                <td class="active"><fmt:message key="page.car.name" bundle="${rb}"/>:
                </td>
                <td><input type="text" name="carname" class="form-control rounded"> </td>
            </tr>
            <tr>
                <td class="active"><fmt:message key="page.car.model" bundle="${rb}"/>:
                </td>
                <td><input type="text" name="carmodel" class="form-control rounded"> </td>
            </tr>
            <tr>
                <td class="active"><fmt:message key="page.car.info" bundle="${rb}"/>:</td>
                <td><input type="text" name="carinfo" class="form-control rounded"></td>
            </tr>
        </table>
        <div class="form-group">
            <button type="submit" name="command" value="add-car"
                    class="btn btn-success" data-original-title=""
                    title=""><fmt:message key="page.button.add" bundle="${rb}"/>
            </button>

        </div>
    </form>
    <div class="panel-body">
        <ul class="nav nav-pills">
            <li class=""><a href="#allcars"  data-toggle="tab"><img src="${pageContext.request.contextPath}/img/icons/back.png"/></a></li>
        </ul>
    </div>

</div>

