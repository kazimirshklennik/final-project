<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="allusers">

    <table class="table table-th-block">
        <thead>
        <tr>
            <td class="active"><fmt:message key="page.user" bundle="${rb}"/></td>

            <td><fmt:message key="page.email" bundle="${rb}"/></td>
            <td><fmt:message key="page.navigation" bundle="${rb}"/></td>
        </tr>
        <thead>
        <c:forEach var="num" items="${alluser}">
        <tr>
            <td><c:out value="${num.login}(${num.role})"/></td>
            <td><c:out value="${num.email}"/></td>
            <td>
                <form action="do" method="post" role="form" style="display: inline-block;">
                    <input type="hidden" name="login" value="${num.login}"/>
                    <input type="hidden" name="type" value="simple"/>
                    <button type="submit" name="command" value="simple-profile" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                        <img src="${pageContext.request.contextPath}/img/icons/profile.png">
                    </button>
                </form>
                <form action="do" method="get" role="form" style="display: inline-block;">
                    <input type="hidden" name="idUser" value="${num.idUser}"/>
                    <c:choose>
                        <c:when test="${num.lockStatus == true}">
                            <button type="submit" name="command" value="unlock" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                                <img src="${pageContext.request.contextPath}/img/icons/unlock.png">
                            </button>
                        </c:when>
                        <c:otherwise>
                            <button type="submit" name="command" value="lock" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                                <img src="${pageContext.request.contextPath}/img/icons/lock.png">
                            </button>
                        </c:otherwise>
                    </c:choose>
                </form>
                <form action="do" method="get" role="form" style="display: inline-block;">
                    <input type="hidden" name="login" value="${num.login}"/>
                <button type="submit" name="command" value="delete-user" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                        <img src="${pageContext.request.contextPath}/img/icons/delete.png">
                    </button>
                </form>
            </td>
        </tr>
        </c:forEach>
    </table>
    <div class="panel-body">
        <ul class="nav nav-pills">
            <li class=""><a href="#admin" data-toggle="tab"><img src="${pageContext.request.contextPath}/img/icons/back.png"/></a></li>
        </ul>

    </div>

</div>