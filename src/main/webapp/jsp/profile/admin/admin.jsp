<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="admin">
    <div class="panel-body">
        <ul id="myTab" class="nav nav-pills">
            <li class=""><a href="#allusers" data-toggle="tab"><fmt:message
                    key="page.all.users" bundle="${rb}"/></a></li>
            <li class=""><a href="#allcars" data-toggle="tab"><fmt:message
                    key="page.all.cars" bundle="${rb}"/></a></li>
        </ul>
    </div>

</div>
