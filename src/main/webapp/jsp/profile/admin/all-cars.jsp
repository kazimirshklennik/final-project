<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<div class="tab-pane fade" id="allcars">
    <table class="table table-th-block">
        <thead>
        <tr>
            <td class="active"><fmt:message key="page.car.name" bundle="${rb}"/></td>
            <td><fmt:message key="page.car.model" bundle="${rb}"/></td>
            <td><fmt:message key="page.navigation" bundle="${rb}"/></td>
        </tr>
        <thead>
        <c:forEach var="car" items="${allcar}">
        <tr>
            <td><c:out value="${car.name}"/></td>
            <td><c:out value="${car.model}"/></td>
            <td>
                <form action="do" method="post" role="form">
                    <input type="hidden" name="idCar" value="${car.idCar}"/>
                    <button type="submit" name="command" value="page-info-car" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                        <img src="${pageContext.request.contextPath}/img/icons/info.png">
                    </button>
                    <button type="submit" name="command" value="delete-car" class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                        <img src="${pageContext.request.contextPath}/img/icons/delete.png">
                    </button>
                </form>
            </td>
        </tr>
        </c:forEach>
    </table>
        <div class="panel-body">
            <ul class="nav nav-pills">
                <li class=""><a href="#admin" data-toggle="tab"><img src="${pageContext.request.contextPath}/img/icons/back.png"/></a></li>
                <li><a href="#addcar" data-toggle="tab"class="button9"><img src="${pageContext.request.contextPath}/img/icons/add.png"/> </a></li>
                   </ul>

    </div>
</div>

<jsp:include page="add-car.jsp"/>

