<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    <title>Profile</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.min.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>

</head>
<body>
<jsp:include page="../service/header.jsp"/>
<div class="w3-container w3-padding">
    <div class="w3-card-4">
        <div id="main">
            <div class="row" id="real-estates-detail">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <header class="panel-title">
                                 <div class="text-center">
                                    <strong></strong>
                                </div>

                            </header>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                                <img class="rounded-foto" src="${pageContext.request.contextPath}/image?command=image-user&idUser=0" />                                <

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">

                            <div id="myTabContent" class="tab-content">
                                <hr>
                                <div class="tab-pane fade active in" id="detail">
                                    <table class="table table-th-block">

                                        <tr>
                                            <div class="text-message-error">
                                          <fmt:message key="error.user.does.not.exist" bundle="${rb}"/>
                                            </div>
                                        </tr>

                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div><!-- /.main -->
<jsp:include page="../service/footer.jsp"/>
</body>
</html>
