<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>


<a href="#x" class="overlay" id="error"></a>
<div class="popup">
    <div class="text-message-error">
        ${errorlogin}
    </div>
    <div align="center">
        <form action="do" method="post" role="form">
            <div class="form-group">
                <button type="submit" name="command" value="tologin"
                        class="btn btn-success" data-original-title=""
                        title=""><fmt:message
                        key="page.login" bundle="${rb}"/>
                </button>
            </div>
        </form>
    </div>
</div>
</div>


<a href="#x" class="overlay" id="blocking"></a>
<div class="popup">
    <div class="text-message-error">
        ${blockingstatus}
    </div>
</div>
