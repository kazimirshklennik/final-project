<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/review.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
</head>

<body>

<section id="teaser">
    <div class="w3-content">
        <div id="main">
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="error-number">
                        <c:if test="${pageContext.errorData.statusCode != '0'}">
                            ${pageContext.errorData.statusCode}
                        </c:if>
                    </div>
                    <div class="panel-body">
                        <div class="carlist">
                            <c:if test="${pageContext.errorData.statusCode != '0'}">
                            Request from ${pageContext.errorData.requestURI} is failed
                            <br/> Servlet name or type: ${pageContext.errorData.servletName}
                                <br/> Status code: ${pageContext.errorData.statusCode}
                            <br/> Exception: ${pageContext.errorData.throwable}
                            </c:if>
                            <br/>
                        <c:if test="${!empty error}">
                            <div class="text-error-page"> INFO: ${error}</div>
                        </c:if>
                            <form action="do" method="post" role="form">
                                <button type="submit" name="command" value="to-home"
                                        class="w3-btn w3-red w3-round-large w3-margin-bottom menu-elem">
                                    <fmt:message key="page.home" bundle="${rb}"/>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
</body>
</html>
