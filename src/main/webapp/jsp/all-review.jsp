<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    <title>AllReviews</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/review.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/shadow.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header-profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/animate1.css"/>
</head>
<body>
<jsp:include page="service/header.jsp"/>


    <section id="teaser">
        <div class="w3-content wow pulse">
            <div id="main">
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <c:forEach var="review" items="${allreviews}">
                                <div class="review">
                                    <td class="table table-th-block">
                                        <tr>
                                            <td>
                                                <img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${review.user.idUser}"
                                                     alt="Avatar" style="width:90px;height:90px;"/>
                                                <div class="text-review"> ${review.date}</div>
                                            </td>
                                        <tr>
                                        <form action="do" method="post" role="form" style="display: inline-block">
                                            <input type="hidden" name="type" value="simple"/>
                                            <input type="hidden" name="login" value="${review.user.login}"/>
                                            <button type="submit" name="command" value="simple-profile"
                                                    class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                                                <img src="${pageContext.request.contextPath}/img/icons/profile.png">${review.user.login}
                                            </button>
                                        </form>
                                            <form action="do" method="post" role="form" style="display: inline-block">
                                                <input type="hidden" name="idCar" value="${review.car.idCar}"/>
                                                <button type="submit" name="command" value="page-info-car"
                                                        class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                                                    <img src="${pageContext.request.contextPath}/img/icons/info.png">${review.car.name}&nbsp;${review.car.model}
                                                </button>
                                            </form>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="popupimg"><p>${review.review}</p></div>
                                            </td>
                                        </tr>
                                    </td>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="tag-pages"><ctg:pagesReview pageNumber="${pageNumber}" maxPages="${maxPages}"
                                                        typeTag="${typeTag}" idCar="${idCar}"/></div>
            </div>
        </div>
    </section>

<jsp:include page="../jsp/service/footer.jsp"/>


<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/viewportchecker.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.review').addClass(".hidden").viewportChecker({
            classToAdd: 'visible animated fadeInDown',
            offset: 100
        });
    });
</script>
</body>
</html>




