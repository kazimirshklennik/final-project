<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css" />


<a href="#x" class="overlay" id="win1"></a>
<div class="popup">
    <div class="text-message-register">
        ${registrationSuccessfully}
    </div>
    <a class="close"title="<fmt:message key="page.close" bundle="${rb}"/>" href="#close"></a>
</div>
