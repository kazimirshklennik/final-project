<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/shadow.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/layout.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header-profile.css"/>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/header-menu.js"></script>

<header>
    <div class="w3-container">
        <div class="w3-container w3-left-align">
            <h1><a class="scroll-to" href="#top"><img class="img-responsive"
                                                      src="${pageContext.request.contextPath}/img/logo.gif"
                                                      alt="Car|Rental"></a></h1>
        </div>
        <div class="w3-card-4" id="shadow">
            <div class="cap-container">
                <ul id="simple-account-dropdown-freebie">
                    <c:if test="${!empty role}">
                        <li>
                            <div id="simple-account-dropdown">
                                <div class="account">
                                    <img class="myImage round"
                                         src="${pageContext.request.contextPath}/image?command=image-user&idUser=${idUser}"/>
                                    <span><fmt:message key="page.hello" bundle="${rb}"/> ${login}</b></span>
                                    <img src="${pageContext.request.contextPath}/img/temp/arrow.png" alt="Dropdown"/>
                                </div>
                                <div class="dropdown" style="display: none">
                                    <ul>
                                        <li>
                                            <form action="do" method="get" role="form">
                                                <input type="hidden" name="login" value="${login}"/>
                                                <button type="submit" name="command" value="simple-profile"
                                                        class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem">
                                                    <fmt:message
                                                            key="page.profile" bundle="${rb}"/>
                                                </button>
                                            </form>
                                        </li>
                                        <li>
                                            <form action="do" method="get" role="form">
                                                <button type="submit" name="command" value="simple-messages"
                                                        class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem">
                                                    <fmt:message key="page.messeges" bundle="${rb}"/>
                                                </button>
                                            </form>
                                        </li>
                                        <li>
                                            <form action="do" method="post" role="form">
                                                <button type="submit" name="command" value="logout"
                                                        class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem">
                                                    <fmt:message
                                                            key="page.logout" bundle="${rb}"/>
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </c:if>
                    <li>
                        <form action="do" method="post" role="form">
                            <button type="submit" name="command" value="index"
                                    class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem">
                                <fmt:message key="page.home" bundle="${rb}"/>
                            </button>
                        </form>
                    </li>
                    <li>
                        <form action="do" method="get" role="form" style="display: inline-block">
                            <input type="hidden" name="typeTag" value="review">
                            <button type="submit" name="command" value="reviews-page"
                                    class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem"><fmt:message
                                    key="page.reviews" bundle="${rb}"/>
                            </button>
                        </form>
                            <form action="do" method="get" role="form" style="display: inline-block">
                            <button type="submit" name="command" value="cars-page"
                                    class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem"><fmt:message
                                    key="page.car.park" bundle="${rb}"/>
                            </button>
                            <c:choose>
                                <c:when test="${empty role}">
                                    <button type="submit" name="command" value="tologin"
                                            class="w3-btn w3-myblue w3-round-large w3-margin-bottom  menu-elem">
                                        <fmt:message
                                                key="page.login" bundle="${rb}"/>
                                    </button>
                                </c:when>
                            </c:choose>
                        </form>
                    </li>
                    <li>
                        <form action="do" method="post" role="form">
                            <input type="hidden" name="page" value="${pageContext.request.requestURI}">
                            <button type="submit" name="command" value="en"
                                    class="w3-btn w3-myblue w3-round-large w3-margin-bottom  menu-elem">
                                <img src="${pageContext.request.contextPath}/img/United-Kingdom.png" alt="usa">
                            </button>
                            <button type="submit" name="command" value="ru"
                                    class="w3-btn w3-myblue w3-round-large w3-margin-bottom menu-elem">
                                <img src="${pageContext.request.contextPath}/img/Russia.png" alt="russia">
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>

