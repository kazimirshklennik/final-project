<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<!DOCTYPE html>
<html>

<head>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.min.css">
    <link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style-reserve-car.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>

    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</head>

<body>

<section id="teaser">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xs-12 pull-right">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="teaser2">
                        <div class="carousel-inner">

                            <div class="item active">
                                <h1 class="title"><fmt:message
                                        key="page.luxury.car.message" bundle="${rb}"/>
                                    <span class="subtitle"><fmt:message
                                            key="page.tread.yourself.message" bundle="${rb}"/></span></h1>
                                <div class="car-img">
                                    <img src="${pageContext.request.contextPath}/image?command=image-car&idImage=0"
                                         class="img-responsive" alt="car1">
                                </div>
                            </div>
                            <c:forEach var="car" items="${allimagescar}">
                                <div class="item">
                                    <h1 class="title"><fmt:message
                                            key="page.get.off.car.message" bundle="${rb}"/>
                                        <span class="subtitle"><fmt:message
                                                key="page.plan.tour.trip.message" bundle="${rb}"/></span></h1>
                                    <div class="car-img">
                                        <img src="${pageContext.request.contextPath}/image?command=image-car&idImage=${car}"
                                             alt="car1" style="width: 500px;height: auto;border-radius: 20px;">

                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <!-- Wrapper for slides end -->


                    <!-- Slider Controls start -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    <!-- Slider Controls end -->
                </div>
            </div>
            <div class="col-md-5 col-xs-12 pull-left">
                <div class="reservation-form-shadow">
                        <form action="do" method="post" name="car-select-form" id="car-select-form">

                            <div class="styled-select-car">
                                <select name="car-select" id="car-select" required="required">

                                    <option value=""><fmt:message
                                            key="page.select.car" bundle="${rb}"/></option>
                                    <c:forEach var="car" items="${allcar}">
                                        <c:if test="${car.status == true}">
                                            <option value="${car.idCar}">${car.name}&nbsp;${car.model}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="datetime pick-up">
                                <div class="date pull-left">
                                    <div class="input-group">
                                    <span class="input-group-addon pixelfix"><span
                                            class="glyphicon glyphicon-calendar"></span>Pick-up&nbsp;&nbsp;</span>
                                        <input type="date" name="pick-up-date"
                                               class="form-control" placeholder="mm/dd/yyyy"
                                               required="required">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="datetime drop-off">
                                <div class="date pull-left">
                                    <div class="input-group">
                                    <span class="input-group-addon pixelfix"><span
                                            class="glyphicon glyphicon-calendar"></span>Drop-off</span>
                                        <input type="date" name="drop-off-date"
                                               class="form-control" placeholder="mm/dd/yyyy"
                                               required="required">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="text-message-error">
                                <script> $('.text-message-error').delay(2000).animate({'opacity':'0'},500);</script>
                                ${errordate}
                                ${erroractivereserve}
                            </div>
                            <div class="text-message">
                                <script> $('.text-message').delay(2000).animate({'opacity':'0'},500);</script>
                                ${reservesuccesfully}
                            </div>
                            <button type="submit" name="command" value="car-reservation"
                                    class="w3-btn w3-reserve w3-round-large w3-margin-bottom"><fmt:message
                                    key="page.reserve" bundle="${rb}"/>
                            </button>
                        </form>

                    </div>
                </div>

            </div>
        </div>
</section>

</body>
</html>