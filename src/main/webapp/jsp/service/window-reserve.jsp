<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/datepicker.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/style-reserve-car.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


<a href="#x" class="overlay" id="reservecar"></a>
<div class="popup">

    <form action="do" method="post" name="car-select-form" id="car-select-form">
        <input type="hidden" name="car-select" value="${car.idCar}">


        <div class="date center-block">
            <div class="input-group">
              <span class="input-group-addon pixelfix">
                <span class="glyphicon glyphicon-calendar"></span>Pick-up&nbsp;</span>
                <input type="date" name="pick-up-date"
                       class="form-control" placeholder="mm/dd/yyyy"
                       required="required" style="width: 150px;">
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="date center-block">
            <div class="input-group">
                  <span class="input-group-addon pixelfix">
                   <span class="glyphicon glyphicon-calendar"></span>Drop-off</span>
                <input type="date" name="drop-off-date"
                       class="form-control" placeholder="mm/dd/yyyy"
                       required="required" style="width: 150px;">
            </div>
            <div class="clearfix"></div>
        </div>

        <button type="submit" name="command" value="car-reservation"
                class="w3-btn w3-reserve w3-round-large w3-margin-bottom"><fmt:message
                key="page.reserve" bundle="${rb}"/>
        </button>
    </form>

</div>
