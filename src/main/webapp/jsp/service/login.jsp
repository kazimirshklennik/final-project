<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<head>
    <title>Login</title>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/animate-custom.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
<script src="${pageContext.request.contextPath}/js/protection-f5.js"></script>

</head>

<jsp:include page="header.jsp"/>


<div class="w3-container w3-center-center">
    <div class="container">
        <section>
            <div id="container_demo">
                <a class="hiddenanchor" id="toregister"></a>
                <a class="hiddenanchor" id="tologin"></a>
                <div id="wrapper">

                    <div id="login" class="animate form">
                        <form action="do" method="post" role="form" class="w3-selection w3-padding">
                            <h1><fmt:message
                                    key="page.login" bundle="${rb}"/></h1>
                            <div class="text-message-error">
                                <script> $('.text-message-error').delay(2000).animate({'opacity': '0'}, 500);</script>
                                ${errorLoginPassMessage}
                                ${errorEmailExist}
                                ${errorLoginExist}
                                ${incorrectdata}
                            </div>
                            <p>
                                <label for="username" class="uname" data-icon="u"> <fmt:message
                                        key="page.login" bundle="${rb}"/></label>
                                <input id="username" name="login" required="required" type="text" maxlength="20"/>
                            </p>
                            <p>
                                <label for="password" class="youpasswd" data-icon="p"><fmt:message
                                        key="page.password" bundle="${rb}"/> </label>
                                <input id="password" name="password" required="required" type="password"
                                       maxlength="16"/>
                            </p>
                            <p class="login button">
                                <button type="submit" name="command" value="login"
                                        class="w3-btn w3-all-but w3-round-large w3-margin-bottom"><fmt:message
                                        key="page.login" bundle="${rb}"/></button>
                                <br/>

                            </p>
                            <p class="change_link">

                                <a href="#toregister" class="to_register"><fmt:message
                                        key="page.register" bundle="${rb}"/></a>
                            </p>

                        </form>
                    </div>


                    <div id="register" class="animate form">

                        <form action="do" method="post" role="form" class="w3-selection w3-padding">

                            <h1><fmt:message
                                    key="page.register" bundle="${rb}"/></h1>

                            <p>
                                <label for="usernamesignup" class="uname" data-icon="u"><fmt:message
                                        key="page.login" bundle="${rb}"/></label>
                                <input id="usernamesignup" name="login" required="required" type="text" maxlength="10"/>
                            </p>
                            <p>
                                <label for="emailsignup" class="youmail" data-icon="e"> <fmt:message
                                        key="page.email" bundle="${rb}"/></label>
                                <input id="emailsignup" name="email" required="required" type="email" maxlength="30"/>
                            </p>

                            <p class="signin button">
                                <button type="submit" name="command" value="register"
                                        class="w3-btn w3-all-but w3-round-large w3-margin-bottom"><fmt:message
                                        key="page.register" bundle="${rb}"/></button>
                            </p>
                            <p class="change_link">
                                <a href="#tologin" class="to_register"> <fmt:message
                                        key="page.login" bundle="${rb}"/></a>
                            </p>
                        </form>
                    </div>

                </div>
            </div>
        </section>

    </div>
</div>

<jsp:include page="footer.jsp"/>
