<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<script src="${pageContext.request.contextPath}/js/protection-f5.js"></script>

<div class="tab-pane fade" id="addreview">
    <form action="do" method="post" role="form">

        <div class="form-group">
            <input type="hidden" name="idCar" value="${car.idCar}"/>
            <label><fmt:message key="page.text.message" bundle="${rb}"/></label>
            <textarea class="form-control rounded"
                      style="height: 100px;width: 80%;" type="text" name="review"></textarea>

        </div>
        <div class="form-group">
            <button type="submit" name="command" value="add-review"
                    class="btn btn-success" data-original-title=""
                    title=""><fmt:message key="page.send.message" bundle="${rb}"/>
            </button>

        </div>
    </form>
</div>