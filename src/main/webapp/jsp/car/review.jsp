<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/review.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>

<div class="tab-pane fade" id="review">
    <table class="table table-th-block">
        <thead>
        <tr>
            <td class="active"><fmt:message key="page.review.autor" bundle="${rb}"/></td>
            <td style="width:50px;"><fmt:message key="page.review.text" bundle="${rb}"/></td>
        </tr>
        <thead>
        <c:forEach var="review" items="${allreviewscar}">
        <tr>
            <td style="width:50px;"><img src="${pageContext.request.contextPath}/image?command=image-user&idUser=${review.user.idUser}"
                     alt="Avatar" style="width:90px;height:90px;border-radius: 10px;"/>
                <p style="font-family: 'Lato', sans-serif;
                color: rgba(40,40,40,0.63);
                font-size: 10px;">${review.date}</p>
                <form action="do" method="get" role="form">
                    <input type="hidden" name="login" value="${review.user.login}"/>
                    <input type="hidden" name="type" value="simple"/>
                    <button type="submit" name="command" value="simple-profile"
                            class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                        <img src="${pageContext.request.contextPath}/img/icons/profile.png"> ${review.user.login}
                    </button>
                </form>
            </td>
            <td>  <div class="popupimg">
                <c:if test="${role == 'ADMIN'}">
                    <a class="close" title="<fmt:message key="page.delete" bundle="${rb}"/>"
                       href="${pageContext.request.contextPath}/do?command=delete-review&idReview=${review.idReview}&idCar=${review.idCar}"></a>
                </c:if>
                <p style="font-family: 'Lato', 'Segoe Print';
                color: rgb(0, 12, 99);
                text-align: justify;">${review.review}</p>
            </div>
            </td>
        </tr>
        </c:forEach>
    </table>
    <c:if test="${!empty role}">
        <div class="panel-body">
            <ul class="nav nav-pills">
                <li><a href="#addreview" data-toggle="tab"><img
                        src="${pageContext.request.contextPath}/img/icons/add.png"/> </a></li>
            </ul>
        </div>
    </c:if>
</div>
<jsp:include page="add-review.jsp"/>
