<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    <title>InfoCar</title>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.min.css"/>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>

</head>
<body>
<jsp:include page="../service/header.jsp"/>
<div class="w3-container w3-padding">
    <div class="w3-card-4">
        <div id="main">
            <div class="row" id="real-estates-detail">
                <jsp:include page="basic-info.jsp"/>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <ul id="myTab" class="nav nav-pills">
                                <li class=""><a href="#detailcar" data-toggle="tab"><fmt:message
                                        key="page.car.info" bundle="${rb}"/></a></li>
                                <li class=""><a href="#review" data-toggle="tab"><fmt:message
                                        key="page.car.review" bundle="${rb}"/></a></li>
                                <li class=""><a href="#photocar" data-toggle="tab"><fmt:message
                                        key="page.car.photo" bundle="${rb}"/></a></li>

                            </ul>

                            <div id="myTabContent" class="tab-content">
                                <jsp:include page="message-info.jsp"/>
                                <jsp:include page="car-detail.jsp"/>
                                <jsp:include page="review.jsp"/>
                                <jsp:include page="update-info.jsp"/>
                                <jsp:include page="photocar.jsp"/>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
