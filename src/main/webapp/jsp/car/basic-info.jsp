<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/button.css"/>

<div class="col-lg-4 col-md-4 col-xs-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            <header class="panel-title">
                <div class="text-center">

                    <strong> ${car.name}&nbsp;${car.model}</strong>
                    <c:if test="${car.status == false}">
                        <img src="${pageContext.request.contextPath}/img/reserved.png"/>
                    </c:if>
                </div>
            </header>
        </div>
        <div class="panel-body">
            <div class="text-center">
<c:choose>
    <c:when test="${!empty car.idGeneralPhoto}">
        <img alt="Avatar" style="width:auto;height:160px;border-radius: 10px;"
             src="${pageContext.request.contextPath}/image?command=image-car&idImage=${car.idGeneralPhoto}"/>
    </c:when>
    <c:otherwise>
        <img alt="Avatar" style="width:auto;height:160px; border-radius: 10px;"
             src="${pageContext.request.contextPath}/image?command=image-car&idImage=0"/>
    </c:otherwise>
</c:choose>

            </div>
            <c:if test="${car.status ==true}">
                <div class="text-center">
                    <a href="#reservecar" class="button9"><fmt:message key="page.reserve" bundle="${rb}"/></a>
                </div>
            </c:if>
        </div>
    </div>

</div>
<jsp:include page="../service/window-reserve.jsp"/>




