<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>

<div class="tab-pane active fade in" id="detailcar">
    <h4></h4>
    <table class="table table-th-block">

        <tr>

            <td class="active"><fmt:message key="page.car.name"
                                            bundle="${rb}"/>:
            </td>
            <td>${car.name}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.car.model"
                                            bundle="${rb}"/>:
            </td>
            <td>${car.model}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.car.price" bundle="${rb}"/>:
            </td>
            <td>${car.price}</td>
        </tr>
        <tr>
            <td class="active"><fmt:message key="page.car.info" bundle="${rb}"/>:</td>
            <td>${car.info}</td>
        </tr>
    </table>

    <c:if test="${role == 'ADMIN'}">
    <div class="panel-body">
        <ul id="myTab" class="nav nav-pills">
            <li class=""><a href="#updatecar" data-toggle="tab"><fmt:message
                    key="page.change.info" bundle="${rb}"/></a></li>
        </ul>

        </ul>

    </div>
    </c:if>
</div>