<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>

<div class="tab-pane fade" id="photocar">
    <table class="table table-th-block">
        <thead>
        <tr>
            <td class="active" align="center">${car.name}&nbsp;${car.model}</td>
        </tr>
        <thead>
        <c:forEach var="photo" items="${allphoto}">
        <tr>
            <td align="center">
               <div class="popupimg">
                   <c:if test="${role == 'ADMIN'}">
                   <a class="close" title="<fmt:message key="page.delete" bundle="${rb}"/>" href="${pageContext.request.contextPath}/do?command=delete-image-car&idCar=${car.idCar}&idImage=${photo}"></a>
                   </c:if>
                <img  src="${pageContext.request.contextPath}/image?command=image-car&idImage=${photo}" alt="foto"/>
               </div>

                  </td>


        </tr>
        </c:forEach>
    </table>
</div>