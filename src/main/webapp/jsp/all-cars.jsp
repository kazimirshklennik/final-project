<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="pagecontent.pagecontent" var="rb"/>
<html>
<head>
    c
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/w3.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/review.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/shadow.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header-profile.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style-text.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/animate1.css"/>
</head>

<body>
<jsp:include page="service/header.jsp"/>
<section id="teaser">
    <div class="w3-content">
        <div id="main">
            <div class="col-lg-8 col-md-8 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <c:forEach var="car" items="${allcars}">
                            <div class="carlist">
                                <td class="table table-th-block">
                                <td>
                                    <tr>
                                        <c:choose>
                                            <c:when test="${!empty car.idGeneralPhoto}">
                                                <img alt="Avatar" style="width:auto;height:90px;border-radius: 10px;"
                                                     src="${pageContext.request.contextPath}/image?command=image-car&idImage=${car.idGeneralPhoto}"/>
                                            </c:when>
                                            <c:otherwise>
                                                <img alt="Avatar" style="width:auto;height:120px;border-radius: 10px;"
                                                     src="${pageContext.request.contextPath}/image?command=image-car&idImage=0"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </tr>
                                </td>
                                <td>
                                    <tr>
                                        <form action="do" method="post" role="form" style="display: inline-block">
                                            <input type="hidden" name="idCar" value="${car.idCar}"/>
                                            <button type="submit" name="command" value="page-info-car"
                                                    class="w3-btn w3-light-gray w3-round-large w3-margin-bottom">
                                                <img src="${pageContext.request.contextPath}/img/icons/info.png">${car.name}&nbsp;${car.model}
                                            </button>
                                        </form>
                                            <form action="do" method="post" role="form" style="display: inline-block">
                                                <input type="hidden" name="idCar" value="${car.idCar}"/>
                                                <input type="hidden" name="typeTag" value="carreview"/>
                                                <input type="hidden" name="type" value="carreview"/>
                                            <button type="submit" name="command" value="reviews-page"
                                                    class="w3-btn w3-light-gray w3-round-large w3-margin-bottom"><img
                                                    src="${pageContext.request.contextPath}/img/icons/reviews.png">
                                                <fmt:message key="page.reviews" bundle="${rb}"/>
                                            </button>
                                        </form>
                                    </tr>
                                    <tr><p>${car.info}</p></tr>
                                </td>

                                <td>
                                    <tr>
                                        <div class="text-price">${car.price}<fmt:message key="page.day"
                                                                                         bundle="${rb}"/></div>
                                    </tr>
                                </td>

                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
            <div class="tag-pages"><ctg:pagesCar pageNumber="${pageNumber}" maxPages="${maxPages}"/></div>
        </div>
    </div>
    </div>
</section>
<jsp:include page="../jsp/service/footer.jsp"/>

<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/viewportchecker.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.carlist').addClass(".hidden").viewportChecker({
            classToAdd: 'visible animated fadeInDown',
            offset: 100
        });
    });
</script>
</body>
</html>




