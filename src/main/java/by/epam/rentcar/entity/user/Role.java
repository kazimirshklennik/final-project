package by.epam.rentcar.entity.user;

/**
 * The enum Role.
 */
public enum Role {

    /**
     * The admin.
     */
    ADMIN("admin"),

    /**
     * The user.
     */
    USER("user"),

    /**
     * The unknown.
     */
    UNKNOWN("unknown");

    /**
     * The role.
     */
    private String role;

    /**
     * Instantiates a new role.
     *
     * @param role the role
     */
    Role(String role){
        this.role=role;
    }

    /**
     * get Role.
     *
     * @return string string
     */
    public String getRole(){
        return role;
    }


}
