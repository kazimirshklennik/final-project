package by.epam.rentcar.entity.user;

import by.epam.rentcar.entity.social.SocialNetwork;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * The Class UserInfo.
 */
public class UserInfo implements Serializable {

    /**
     * The id User info.
     */
    private Long idUserInfo;

    /**
     * The first name.
     */
    private String firstName;

    /**
     * The last name
     */
    private String lastName;

    /**
     * The birthday.
     */
    private String birthday;

    /**
     * The country.
     */
    private String country;

    /**
     * The city.
     */
    private String city;

    /**
     * The age.
     */
    private int age;

    /**
     * The sex.
     */
    private String sex;

    /**
     * The userText.
     */
    private String userText;

    /**
     * The date Registration.
     */
    private String dateRegistration;

    /**
     * The date last Activity.
     */
    private String lastActivity;

    /**
     * The date social Networks.
     */
    private List<SocialNetwork> socialNetworks;


    /**
     * Instantiates a new User info.
     */
    public UserInfo() {
    }

    /**
     * Instantiates a new User info.
     *
     * @param firstName the first name
     * @param lastName  the last name
     */
    public UserInfo(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Gets the First Name.
     *
     * @return the First Name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets the Last Name
     *
     * @return the Last Name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the First Name.
     *
     * @param firstName the new first Name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Sets the last Name.
     *
     * @param lastName the new last Name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the City.
     *
     * @return the City
     */
    public String getCity() {
        return city;
    }

    /**
     * Gets the Age.
     *
     * @return the Age
     */
    public int getAge() {
        return age;
    }

    /**
     * Gets the Sex.
     *
     * @return the Sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * Gets the User Text.
     *
     * @return the User Text
     */
    public String getUserText() {
        return userText;
    }

    /**
     * Sets the country.
     *
     * @param country the new country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Sets the city.
     *
     * @param city the newcity
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Sets the age.
     *
     * @param age the age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Sets the sex.
     *
     * @param sex the sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * Sets the user Text.
     *
     * @param userText the userText
     */
    public void setUserText(String userText) {
        this.userText = userText;
    }

    /**
     * Gets the Social Networks.
     *
     * @return the Social Networks
     */
    public List<SocialNetwork> getSocialNetworks() {
        return socialNetworks;
    }

    /**
     * Sets the social Networks.
     *
     * @param socialNetworks the socialNetworks
     */
    public void setSocialNetworks(List<SocialNetwork> socialNetworks) {
        this.socialNetworks = socialNetworks;
    }

    /**
     * Gets the Birthday.
     *
     * @return the Birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Gets the Date Registration.
     *
     * @return the Date Registration
     */
    public String getDateRegistration() {
        return dateRegistration;
    }

    /**
     * Sets the dirthday
     *
     * @param dirthday the dirthday
     */
    public void setBirthday(String dirthday) {
        this.birthday = dirthday;
    }

    /**
     * Sets the date Registration
     *
     * @param dateRegistration the date Registration
     */
    public void setDateRegistration(String dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    /**
     * Gets the Id User Info.
     *
     * @return the Id User Info
     */
    public Long getIdUserInfo() {
        return idUserInfo;
    }

    /**
     * Sets the Id User Info
     *
     * @param idUserInfo the Id User Info
     */
    public void setIdUserInfo(Long idUserInfo) {
        this.idUserInfo = idUserInfo;
    }

    /**
     * Gets the Last Activity.
     *
     * @return the ILast Activity.
     */
    public String getLastActivity() {
        return lastActivity;
    }

    /**
     * Gets the Last Activity.
     *
     * @param lastActivity the last activity
     * @return the Last Activity
     */
    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserInfo)) return false;
        UserInfo userInfo = (UserInfo) o;
        return getAge() == userInfo.getAge() &&
                getIdUserInfo().equals(userInfo.getIdUserInfo()) &&
                getFirstName().equals(userInfo.getFirstName()) &&
                getLastName().equals(userInfo.getLastName()) &&
                getBirthday().equals(userInfo.getBirthday()) &&
                getCountry().equals(userInfo.getCountry()) &&
                getCity().equals(userInfo.getCity()) &&
                getSex().equals(userInfo.getSex()) &&
                getUserText().equals(userInfo.getUserText()) &&
                getDateRegistration().equals(userInfo.getDateRegistration()) &&
                getSocialNetworks().equals(userInfo.getSocialNetworks());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIdUserInfo(), getFirstName(), getLastName(), getBirthday(), getCountry(), getCity(), getAge(), getSex(), getUserText(), getDateRegistration(), getSocialNetworks());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserInfo{" +
                "idUserInfo=" + idUserInfo +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dirthdayString='" + birthday + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", userText='" + userText + '\'' +
                ", dateRegistrationString='" + dateRegistration + '\'' +
                ", socialNetworks=" + socialNetworks +
                '}';
    }
}
