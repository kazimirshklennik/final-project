package by.epam.rentcar.entity.user;

import java.util.Objects;

/**
 * The Class User.
 */
public class User{

    /**
     * The id User.
     */
    private Long idUser;

    /**
     * The login.
     */
    private String login;

    /**
     * The password.
     */
    private String password;

    /**
     * The email.
     */
    private String email;

    /**
     * The role.
     */
    private Role role;

    /**
     * The lockStatus.
     */
    private boolean lockStatus;

    /**
     * The statusOnline.
     */
    private boolean statusOnline;

    /**
     * The info.
     */
    private UserInfo info=new UserInfo();

    /**
     * Instantiates a new User.
     */
    public User() {
    }

    /**
     * Instantiates a new User.
     *
     * @param login    the login
     * @param password the password
     * @param email    the email
     */
    public User(String login, String password, String email) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.role= Role.USER;
    }

    /**
     * Gets the Login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the id user.
     *
     * @param login the new login
     */
    public void setLogin(String login) {

        this.login = login;
    }

    /**
     * Gets the Email.
     *
     * @return the Email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the Email.
     *
     * @param email the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Password.
     *
     * @return the Password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {

        this.password = password;
    }

    /**
     * Gets the Role.
     *
     * @return the Role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets the role.
     *
     * @param role the new role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Gets the Info.
     *
     * @return the Info
     */
    public UserInfo getInfo() {
        return info;
    }

    /**
     * Sets the Info.
     *
     * @param info the new info
     */
    public void setInfo(UserInfo info) {
        this.info = info;
    }

    /**
     * Gets the user id.
     *
     * @return the user id
     */
    public Long getIdUser() {
        return idUser;
    }

    /**
     * Sets the id user.
     *
     * @param idUser the new id User
     */
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    /**
     * Gets the Lock Status.
     *
     * @return the Lock Status
     */
    public boolean isLockStatus() {
        return lockStatus;
    }

    /**
     * Sets the Lock Status.
     *
     * @param lockStatus the new lock Status
     */
    public void setLockStatus(boolean lockStatus) {
        this.lockStatus = lockStatus;
    }

    /**
     * Gets the Status Online.
     *
     * @return the Status Online
     */
    public boolean isStatusOnline() {
        return statusOnline;
    }

    /**
     * Sets the Status Online.
     *
     * @param statusOnline the new status Online
     */
    public void setStatusOnline(boolean statusOnline) {
        this.statusOnline = statusOnline;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return isLockStatus() == user.isLockStatus() &&
                isStatusOnline() == user.isStatusOnline() &&
                getIdUser().equals(user.getIdUser()) &&
                getLogin().equals(user.getLogin()) &&
                getPassword().equals(user.getPassword()) &&
                getEmail().equals(user.getEmail()) &&
                getInfo().equals(user.getInfo()) &&
                getRole() == user.getRole();
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIdUser(), getLogin(), getPassword(), getEmail(), getInfo(), getRole(), isLockStatus(), isStatusOnline());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", info=" + info +
                ", role=" + role +
                ", lockStatus=" + lockStatus +
                ", statusOnline=" + statusOnline +
                '}';
    }
}
