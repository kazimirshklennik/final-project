package by.epam.rentcar.entity.social;

import java.util.Objects;

/**
 * The Class SocialNetwork.
 */
public class SocialNetwork {

    /**
     * The id Social Network.
     */
    private Long idSocialNetwork;

    /**
     * The id User Info.
     */
    private Long idUserInfo;

    /**
     * The id User.
     */
    private Long idUser;

    /**
     * The url.
     */
    private String url;

    /**
     * The Social Notwork Type.
     */
    private SocialNotworkType type;

    /**
     * Instantiates a new Social network.
     */
    public SocialNetwork() {
    }

    /**
     * Instantiates a new Social network.
     *
     * @param idUser the id user
     * @param url    the url
     */
    public SocialNetwork(Long idUser, String url) {
        this.idUserInfo = idUser;
        this.url = url;
    }

    /**
     * Gets the Id User Info.
     *
     * @return the User Info id
     */
    public Long getIdUserInfo() {
        return idUserInfo;
    }

    /**
     * Gets the Url.
     *
     * @return the Url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the Id User Info.
     *
     * @param idUser the new id User info
     */
    public void setIdUserInfo(Long idUser) {
        this.idUserInfo = idUser;
    }

    /**
     * Sets the url.
     *
     * @param url the new url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets the Type url.
     *
     * @return the Type
     */
    public SocialNotworkType getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type the new type url.
     */
    public void setType(SocialNotworkType type) {
        this.type = type;
    }

    /**
     * Gets Id Social Network.
     *
     * @return the Social Network id.
     */
    public Long getIdSocialNetwork() {
        return idSocialNetwork;
    }

    /**
     * Sets the Id Social Network.
     *
     * @param idSocialNetwork the new id Social Network
     */
    public void setIdSocialNetwork(Long idSocialNetwork) {
        this.idSocialNetwork = idSocialNetwork;
    }

    /**
     * Gets the user id.
     *
     * @return the user id
     */
    public Long getIdUser() {
        return idUser;
    }

    /**
     * Sets the id user.
     *
     * @param idUser the new id User
     */
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SocialNetwork{" +
                "idSocialNetwork=" + idSocialNetwork +
                ", idUserInfo=" + idUserInfo +
                ", idUser=" + idUser +
                ", url='" + url + '\'' +
                ", type=" + type +
                '}';
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SocialNetwork)) return false;
        SocialNetwork that = (SocialNetwork) o;
        return getIdSocialNetwork().equals(that.getIdSocialNetwork()) &&
                getIdUserInfo().equals(that.getIdUserInfo()) &&
                getIdUser().equals(that.getIdUser()) &&
                getUrl().equals(that.getUrl()) &&
                getType() == that.getType();
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIdSocialNetwork(), getIdUserInfo(), getIdUser(), getUrl(), getType());
    }
}
