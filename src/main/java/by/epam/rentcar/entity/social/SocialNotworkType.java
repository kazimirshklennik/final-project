package by.epam.rentcar.entity.social;

/**
 * The enum SocialNotworkType.
 */
public enum SocialNotworkType {

    /**
     * Facebook social notwork type.
     */
    FACEBOOK,
    /**
     * Twitter social notwork type.
     */
    TWITTER,
    /**
     * Instagram social notwork type.
     */
    INSTAGRAM,
    /**
     * Google social notwork type.
     */
    GOOGLE,
    /**
     * Vk com social notwork type.
     */
    VK_COM,
    /**
     * Unknown social notwork type.
     */
    UNKNOWN
}
