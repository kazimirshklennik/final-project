package by.epam.rentcar.entity;

import by.epam.rentcar.entity.user.User;

import java.util.Objects;

/**
 * The Class Message.
 */
public class Message {

    /**
     * The id Message.
     */
    private Long idMessage;

    /**
     * The id Sender.
     */
    private Long idSender;

    /**
     * The id Recipient.
     */
    private Long idRecipient;

    /**
     * The user Sender.
     */
    private User userSender;

    /**
     * The  user Recipient.
     */
    private User userRecipient;

    /**
     * The  text.
     */
    private String text;

    /**
     * The  date date.
     */
    private Long date;

    /**
     * The  date String.
     */
    private String dateString;

    /**
     * The  mail Recipient.
     */
    private String mailRecipient;

    /**
     * The subject.
     */
    private String subject;

    /**
     * The id EMAIL_SENDER.
     */
    private final String EMAIL_SENDER = "rent_car@gmail.com";

    /**
     * Instantiates a new Message.
     */
    public Message() {
    }

    /**
     * Instantiates a new Message.
     *
     * @param idSender    the id sender
     * @param idRecipient the id recipient
     * @param text        the text
     */
    public Message(Long idSender, Long idRecipient, String text) {
        this.idSender = idSender;
        this.idRecipient = idRecipient;
        this.text = text;
        this.date = System.currentTimeMillis();
    }

    /**
     * Instantiates a new Message.
     *
     * @param subject       the subject
     * @param mailRecipient the mail recipient
     * @param text          the text
     */
    public Message(String subject, String mailRecipient, String text) {
        this.mailRecipient = mailRecipient;
        this.text = text;
        this.date = System.currentTimeMillis();
        this.subject = subject;
    }

    /**
     * Gets the id Sender.
     *
     * @return the id Sender
     */
    public Long getIdSender() {
        return idSender;
    }

    /**
     * Gets the Id Recipient.
     *
     * @return the Id Recipient
     */
    public Long getIdRecipient() {
        return idRecipient;
    }

    /**
     * Gets the Text.
     *
     * @return the Text
     */
    public String getText() {
        return text;
    }

    /**
     * Gets the Date.
     *
     * @return the Date
     */
    public long getDate() {
        return date;
    }

    /**
     * Sets the id Sender.
     *
     * @param idSender the new id Sender
     */
    public void setIdSender(Long idSender) {
        this.idSender = idSender;
    }

    /**
     * Sets the id Recipient.
     *
     * @param idRecipient the new id Recipient
     */
    public void setIdRecipient(Long idRecipient) {
        this.idRecipient = idRecipient;
    }

    /**
     * Sets the text.
     *
     * @param text the new text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * Gets the Email Recipient.
     *
     * @return the Email Recipient
     */
    public String getEmailRecipient() {
        return mailRecipient;
    }

    /**
     * Sets the Fmail Recipient.
     *
     * @param mailRecipient the new email Recipient
     */
    public void setEmailRecipient(String mailRecipient) {
        this.mailRecipient = mailRecipient;
    }

    /**
     * Gets the Email Sender.
     *
     * @return the Email Sender
     */
    public String getEmailSender() {
        return EMAIL_SENDER;
    }

    /**
     * Gets the Subject.
     *
     * @return the Subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the subject.
     *
     * @param subject the new subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Gets the User Sender.
     *
     * @return the User Sender.
     */
    public User getUserSender() {
        return userSender;
    }

    /**
     * Gets the User Recipient.
     *
     * @return the User Recipient.
     */
    public User getUserRecipient() {
        return userRecipient;
    }

    /**
     * Sets the user Sender.
     *
     * @param userSender the new user Sender
     */
    public void setUserSender(User userSender) {
        this.userSender = userSender;
    }

    /**
     * Sets the user Recipient.
     *
     * @param userRecipient the new user Recipient
     */
    public void setUserRecipient(User userRecipient) {
        this.userRecipient = userRecipient;
    }

    /**
     * Gets the Id Message.
     *
     * @return the Id Message
     */
    public Long getIdMessage() {
        return idMessage;
    }

    /**
     * Sets the id Message.
     *
     * @param idMessage the new id Message
     */
    public void setIdMessage(Long idMessage) {
        this.idMessage = idMessage;
    }

    /**
     * Gets the Date.
     *
     * @return the Date
     */
    public String getDateString() {
        return dateString;
    }

    /**
     * Sets the date.
     *
     * @param dateString the new date
     */
    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        if (message.getIdRecipient() == ((Message) o).idSender && message.getIdSender() == ((Message) o).idRecipient) {
            return true;
        }
        return idSender == message.idSender &&
                idRecipient == message.idRecipient &&
                date == message.date &&
                Objects.equals(userSender, message.userSender) &&
                Objects.equals(userRecipient, message.userRecipient) &&
                Objects.equals(text, message.text) &&
                Objects.equals(mailRecipient, message.mailRecipient) &&
                Objects.equals(subject, message.subject);
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(idSender, idRecipient, userSender, userRecipient, text, date, mailRecipient, subject, EMAIL_SENDER);
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Message{" +
                "idSender=" + idSender +
                ", idRecipient=" + idRecipient +
                ", userSender=" + userSender +
                ", userRecipient=" + userRecipient +
                ", text='" + text + '\'' +
                ", date=" + date +
                ", mailRecipient='" + mailRecipient + '\'' +
                ", subject='" + subject + '\'' +
                ", mailSender='" + EMAIL_SENDER + '\'' +
                '}';
    }
}
