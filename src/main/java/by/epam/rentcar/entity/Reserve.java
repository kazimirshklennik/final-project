package by.epam.rentcar.entity;

import by.epam.rentcar.entity.user.User;

import java.util.Objects;

/**
 * The Class Reserve.
 */
public class Reserve {

    /**
     * The id Reserve.
     */
    private Long idReserve;

    /**
     * The id car.
     */
    private Long idCar;

    /**
     * The id user.
     */
    private Long idUser;

    /**
     * The user.
     */
    private User user;

    /**
     * The car.
     */
    private Car car;

    /**
     * The date Booking.
     */
    private String dateBooking;

    /**
     * The date Return.
     */
    private String dateReturn;

    /**
     * The total Cost.
     */
    private int totalCost;

    /**
     * The status.
     */
    private boolean status;

    /**
     * Instantiates a new Reserve.
     */
    public Reserve() {
    }

    /**
     * Instantiates a new Reserve.
     *
     * @param dateBooking the date booking
     * @param dateReturn  the date return
     * @param idCar       the id car
     * @param idUser      the id user
     */
    public Reserve(String dateBooking, String dateReturn, Long idCar, Long idUser) {
        this.dateBooking = dateBooking;
        this.dateReturn = dateReturn;
        this.idCar = idCar;
        this.idUser = idUser;
    }

    /**
     * Gets the TDate Booking
     *
     * @return the Date Booking
     */
    public String getDateBooking() {
        return dateBooking;
    }

    /**
     * Gets the Date Return.
     *
     * @return the Date Return
     */
    public String getDateReturn() {
        return dateReturn;
    }

    /**
     * Gets the Id Car.
     *
     * @return the Id Car
     */
    public Long getIdCar() {
        return idCar;
    }

    /**
     * Gets the Id User.
     *
     * @return the Id User
     */
    public Long getIdUser() {
        return idUser;
    }

    /**
     * Sets the date Booking
     *
     * @param dateBooking the newdate Booking
     */
    public void setDateBooking(String dateBooking) {
        this.dateBooking = dateBooking;
    }

    /**
     * Sets the Id User Info
     *
     * @param dateReturn the new newdate Return
     */
    public void setDateReturn(String dateReturn) {
        this.dateReturn = dateReturn;
    }

    /**
     * Sets the Id car
     *
     * @param idCar the new Id car
     */
    public void setIdCar(Long idCar) {
        this.idCar = idCar;
    }

    /**
     * Sets the Id User
     *
     * @param idUser the new id User
     */
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    /**
     * Gets the User.
     *
     * @return the TUser
     */
    public User getUser() {
        return user;
    }

    /**
     * Gets the Car.
     *
     * @return the Car
     */
    public Car getCar() {
        return car;
    }

    /**
     * Gets the Booking.
     *
     * @return the Booking
     */
    public String getBooking() {
        return dateBooking;
    }

    /**
     * Gets the Return.
     *
     * @return the Return
     */
    public String getdReturn() {
        return dateReturn;
    }

    /**
     * Sets the user
     *
     * @param user the new user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Sets the car
     *
     * @param car the new car
     */
    public void setCar(Car car) {
        this.car = car;
    }

    /**
     * Sets the booking
     *
     * @param booking the new booking
     */
    public void setBooking(String booking) {
        this.dateBooking = booking;
    }

    /**
     * Sets the dReturn
     *
     * @param dReturn the new dReturn
     */
    public void setdReturn(String dReturn) {
        this.dateReturn = dReturn;
    }

    /**
     * Gets the Total Cost.
     *
     * @return the Total Cost
     */
    public int getTotalCost() {
        return totalCost;
    }

    /**
     * Sets the total Cost
     *
     * @param totalCost the new total Cost
     */
    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    /**
     * Gets the Status.
     *
     * @return the Status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status
     *
     * @param status the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Gets the Id Reserve.
     *
     * @return the Id Reserve
     */
    public Long getIdReserve() {
        return idReserve;
    }

    /**
     * Sets the id Reserve
     *
     * @param idReserve the new id Reserve
     */
    public void setIdReserve(Long idReserve) {
        this.idReserve = idReserve;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reserve)) return false;
        Reserve reserve = (Reserve) o;
        return getTotalCost() == reserve.getTotalCost() &&
                isStatus() == reserve.isStatus() &&
                getIdReserve().equals(reserve.getIdReserve()) &&
                getDateBooking().equals(reserve.getDateBooking()) &&
                getDateReturn().equals(reserve.getDateReturn()) &&
                getIdCar().equals(reserve.getIdCar()) &&
                getIdUser().equals(reserve.getIdUser()) &&
                getUser().equals(reserve.getUser()) &&
                getCar().equals(reserve.getCar()) &&
                getBooking().equals(reserve.getBooking()) &&
                getdReturn().equals(reserve.getdReturn());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIdReserve(), getDateBooking(), getDateReturn(), getIdCar(), getIdUser(), getUser(), getCar(), getBooking(), getdReturn(), getTotalCost(), isStatus());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Reserve{" +
                "idReserve=" + idReserve +
                ", dateBooking=" + dateBooking +
                ", dateReturn=" + dateReturn +
                ", idCar=" + idCar +
                ", idUser=" + idUser +
                ", user=" + user +
                ", car=" + car +
                ", booking='" + dateBooking + '\'' +
                ", dReturn='" + dateReturn + '\'' +
                ", totalCost=" + totalCost +
                ", status=" + status +
                '}';
    }
}
