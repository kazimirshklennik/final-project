package by.epam.rentcar.entity.test;
import javax.persistence.*;

/**
 * The type Role entity test.
 */
@Entity
@Table(name = "role")
@NamedQuery(name = "DbUnitRoleEntity.getAll", query = "select r from RoleEntityTest r")
public class RoleEntityTest {
    @Id
    @GeneratedValue
    private int id_role;
     private String role;

    /**
     * Instantiates a new Role entity test.
     */
    public RoleEntityTest() {}

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Gets id role.
     *
     * @return the id role
     */
    public int getId_role() {
        return id_role;
    }

    /**
     * Sets id role.
     *
     * @param id_role the id role
     */
    public void setId_role(int id_role) {
        this.id_role = id_role;
    }
}