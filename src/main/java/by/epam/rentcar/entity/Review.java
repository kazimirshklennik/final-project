package by.epam.rentcar.entity;

import by.epam.rentcar.entity.user.User;

import java.util.Objects;

/**
 * The Class Review.
 */
public class Review {

    /**
     * The id Review.
     */
    private Long idReview;

    /**
     * The review.
     */
    private String review;

    /**
     * The id User.
     */
    private Long idUser;

    /**
     * The id car.
     */
    private Long idCar;

    /**
     * The date.
     */
    private String date;

    /**
     * The rating.
     */
    private int rating;

    /**
     * The car.
     */
    private Car car = new Car();

    /**
     * The user.
     */
    private User user = new User();


    /**
     * Instantiates a new Review.
     */
    public Review() {
    }

    /**
     * Gets the Review.
     *
     * @return the Review
     */
    public String getReview() {
        return review;
    }

    /**
     * Sets the review.
     *
     * @param review the new review
     */
    public void setReview(String review) {
        this.review = review;
    }


    /**
     * Sets id user.
     *
     * @param idUser the id user
     */
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    /**
     * Sets id car.
     *
     * @param idCar the id car
     */
    public void setIdCar(Long idCar) {
        this.idCar = idCar;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(String date) {
        this.date = date;
    }


    /**
     * Gets rating.
     *
     * @return the rating
     */
    public int getRating() {
        return rating;
    }

    /**
     * Sets the rating.
     *
     * @param rating the new rating
     */
    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     * Gets the Id User.
     *
     * @return the Id User
     */
    public Long getIdUser() {
        return idUser;
    }


    /**
     * Gets id car.
     *
     * @return the id car
     */
    public Long getIdCar() {
        return idCar;
    }

    /**
     * Gets the Date.
     *
     * @return the Date
     */
    public String getDate() {
        return date;
    }

    /**
     * Gets the Car.
     *
     * @return the Car
     */
    public Car getCar() {
        return car;
    }

    /**
     * Gets the User.
     *
     * @return the User
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the car.
     *
     * @param car the new car
     */
    public void setCar(Car car) {
        this.car = car;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets the First Name.
     *
     * @return the First Name
     */
    public Long getIdReview() {
        return idReview;
    }


    /**
     * Sets id review.
     *
     * @param idReview the id review
     */
    public void setIdReview(Long idReview) {
        this.idReview = idReview;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Review)) return false;
        Review review1 = (Review) o;
        return getRating() == review1.getRating() &&
                Objects.equals(getIdReview(), review1.getIdReview()) &&
                Objects.equals(getReview(), review1.getReview()) &&
                Objects.equals(getIdUser(), review1.getIdUser()) &&
                Objects.equals(getIdCar(), review1.getIdCar()) &&
                Objects.equals(getDate(), review1.getDate()) &&
                Objects.equals(getCar(), review1.getCar()) &&
                Objects.equals(getUser(), review1.getUser());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIdReview(), getReview(), getIdUser(), getIdCar(), getDate(), getRating(), getCar(), getUser());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Review{" +
                "idReview=" + idReview +
                ", review='" + review + '\'' +
                ", idUser=" + idUser +
                ", idCar=" + idCar +
                ", date='" + date + '\'' +
                ", rating=" + rating +
                ", car=" + car +
                ", user=" + user +
                '}';
    }
}
