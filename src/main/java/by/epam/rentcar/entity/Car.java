package by.epam.rentcar.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * The Class Car.
 */
public class Car implements Serializable {

    /**
     * The id Car.
     */
    private Long idCar;

    /**
     * The id General Photo.
     */
    private Long idGeneralPhoto;

    /**
     * The name.
     */
    private String name;

    /**
     * The model.
     */
    private String model;

    /**
     * The price.
     */
    private int price;

    /**
     * The status.
     */
    private boolean status;

    /**
     * The info.
     */
    private String info;

    /**
     * Instantiates a new Car.
     */
    public Car() {
    }

    /**
     * Instantiates a new Car.
     *
     * @param name  the name
     * @param model the model
     */
    public Car(String name, String model) {
        this.name = name;
        this.model = model;
    }

    /**
     * Gets the Name.
     *
     * @return the Name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the Model.
     *
     * @return the Model
     */
    public String getModel() {
        return model;
    }

    /**
     * Gets the Price.
     *
     * @return the Price
     */
    public int getPrice() {
        return price;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the model.
     *
     * @param model the new model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Gets the Status.
     *
     * @return the Status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status the new status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Gets the Info.
     *
     * @return the Info
     */
    public String getInfo() {
        return info;
    }

    /**
     * Sets the info.
     *
     * @param info the new info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * Sets the id Car.
     *
     * @param idCar the new id Car
     */
    public void setIdCar(Long idCar) {
        this.idCar = idCar;
    }

    /**
     * Gets the Id Car.
     *
     * @return the Id Car
     */
    public Long getIdCar() {
        return idCar;
    }

    /**
     * Gets the Id General Photo.
     *
     * @return the Id General Photo
     */
    public Long getIdGeneralPhoto() {
        return idGeneralPhoto;
    }

    /**
     * Sets the id General Photo.
     *
     * @param idGeneralPhoto the new id General Photo.
     */
    public void setIdGeneralPhoto(Long idGeneralPhoto) {
        this.idGeneralPhoto = idGeneralPhoto;
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return
                getPrice() == car.getPrice() &&
                        isStatus() == car.isStatus() &&
                        getIdCar().equals(car.getIdCar()) &&
                        getIdGeneralPhoto().equals(car.getIdGeneralPhoto()) &&
                        getName().equals(car.getName()) &&
                        getModel().equals(car.getModel()) &&
                        getInfo().equals(car.getInfo());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(getIdCar(), getIdGeneralPhoto(), getName(), getModel(), getPrice(), isStatus(), getInfo());
    }

    /**
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Car{" +
                "idCar=" + idCar +
                ", idGeneralPhoto=" + idGeneralPhoto +
                ", name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", status=" + status +
                ", info='" + info + '\'' +
                '}';
    }
}
