package by.epam.rentcar.dao;

import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.DaoException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The Interface ParseResult.
 *
 * @param <T> the generic type
 */
public interface ParseResult<T> {

    /**
     * Parses the result set.
     *
     * @param resultSet the result set
     * @return the list
     * @throws DaoException the dao exception
     */
    default List<T> parseResultSet(ResultSet resultSet) throws DaoException{
       List<T> data = new ArrayList<>();
       try {
           while (resultSet.next()) {
               data.add(build(resultSet));
           }
       } catch (SQLException e) {
           throw new DaoException("SQLException in method parseResultSet(ResultSet resultSet)" + e);
       }
       return data;
   }

    /**
     * Builds the.
     *
     * @param resultSet the result set
     * @return the T
     * @throws DaoException the dao exception
     */
    T build(ResultSet resultSet) throws DaoException;

    /**
     * Builds the.
     *
     * @param resultSet the result set
     * @return the User
     * @throws DaoException the dao exception
     */
    default User buildAllInfo(ResultSet resultSet) throws DaoException{
        throw new UnsupportedOperationException();
    }
}
