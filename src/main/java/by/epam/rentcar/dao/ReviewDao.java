package by.epam.rentcar.dao;

import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.DaoException;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The Interface ReviewDao.
 */
public interface ReviewDao {

    /**
     * find All By Id Car.
     *
     * @param idCar the idCar
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Review> findAllByIdCar(@NotNull Long idCar) throws DaoException;

    /**
     * find Part ReviewS.
     *
     * @param limit  the limit
     * @param offset the offset
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Review> findPartReview(int limit, int offset) throws DaoException;

    /**
     * find Part Reviews by id car.
     *
     * @param limit  the limit
     * @param offset the offset
     * @param idCar  the id car
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Review> findPartReviewByIdCar(int limit, int offset, @NotNull Long idCar) throws DaoException;
}
