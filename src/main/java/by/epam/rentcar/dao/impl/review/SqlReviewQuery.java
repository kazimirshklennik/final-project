package by.epam.rentcar.dao.impl.review;

/**
 * The Enum SqlReserveQuery.
 */
public enum SqlReviewQuery {

    /**
     * The sql add review.
     */
    SQL_ADD_REVIEW("INSERT INTO `car_rent`.`review` (`review`, `review_date`, `rating`, `user_id_user`, `car_id_auto`) " +
            "VALUES (?, ?, ?, ?, ?);"),

    /**
     * The sql delete review.
     */
    SQL_DELETE_REVIEW("DELETE FROM `car_rent`.`review` WHERE (`id_review` = ?);"),

    /**
     * The sql delete all reviews by id car.
     */
    SQL_DELETE_ALL_REVIEWS_BY_ID_CAR("DELETE FROM `car_rent`.`review` WHERE (`car_id_auto` = ?);"),

    /**
     * The sql delete all reviews by id user.
     */
    SQL_DELETE_ALL_REVIEWS_BY_ID_USER("DELETE FROM `car_rent`.`review` WHERE (`user_id_user` = ?);"),

    /**
     * The sql find review by id.
     */
    SQL_FIND_REVIEW_BY_ID("SELECT `id_review`,`review`, `user_id_user`,`car_id_auto`,`review_date`" +
            "FROM `car_rent`.`review`" +
            "WHERE `id_review`=?"),

    /**
     * The sql find reviews by id user.
     */
    SQL_FIND_REVIEWS_BY_USER_ID("SELECT `id_review`,`review`, `user_id_user`,`car_id_auto`,`review_date`" +
            "FROM `car_rent`.`review`" +
            "WHERE `user_id_user`=?"),

    /**
     * The sql find reviews by id car.
     */
    SQL_FIND_REVIEWS_BY_CAR_ID("SELECT `id_review`,`review`,`rating`,`user_id_user`,`car_id_auto`,`review_date`" +
            "FROM `car_rent`.`review`" +
            "WHERE `car_id_auto`=?"),

    /**
     * The sql find part reviews by id.
     */
    SQL_FIND_PART_REVIEWS_BY_ID_CAR("SELECT `id_review`,`review`,`rating`, `user_id_user`,`car_id_auto`,`review_date`" +
            "FROM `car_rent`.`review`" +
            "WHERE `car_id_auto`=? LIMIT ? OFFSET ?;"),

    /**
     * The sql find all reviews.
     */
    SQL_FIND_ALL_REVIEWS("SELECT `id_review`,`review`, `user_id_user`,`car_id_auto`,`review_date`,`rating` FROM `car_rent`.`review`;"),

    SQL_FIND_PART_REVIEWS("SELECT  `id_review`,`review`, `user_id_user`,`car_id_auto`,`review_date`,`rating` FROM `car_rent`.`review` LIMIT ? OFFSET ?;");


    /**
     * The query.
     */
    private final String query;

    /**
     * Instantiates a new sql user query.
     *
     * @param text the text
     */
    SqlReviewQuery(String text) {

        this.query = text;
    }

    /**
     * getQuery.
     *
     * @return string
     */
    public String getQuery() {
        return query;
    }
}