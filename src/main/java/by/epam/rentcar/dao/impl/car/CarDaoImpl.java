package by.epam.rentcar.dao.impl.car;

import by.epam.rentcar.dao.CarDao;
import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.ParseResult;
import by.epam.rentcar.dao.impl.reserve.ReserveCarDaoImpl;
import by.epam.rentcar.dao.impl.review.ReviewDaoImpl;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CarDaoImpl.
 */
public class CarDaoImpl implements CarRentDao<Car>, ParseResult<Car>, CarDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(CarDaoImpl.class);

    /**
     * The dao.
     */
    private static CarDaoImpl instance = new CarDaoImpl();


    /**
     * Instantiates a new CarDaoImpl dao impl.
     */
    private CarDaoImpl() {
    }

    /**
     * Gets the single instance of Car Dao Impl.
     *
     * @return single instance of CarDaoImpl
     */
    public static CarDaoImpl getInstance() {
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#add(Object)
     */
    @Override
    public Long add(@NotNull Car car) throws DaoException {

        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatementCar = null;
        ResultSet resultSet = null;

        long idCar = 0;
        try {
            connection.setAutoCommit(false);
            preparedStatementCar = connection.prepareStatement(SqlCarQuery.SQL_INSERT_CAR.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatementCar.setString(1, car.getName());
            preparedStatementCar.setString(2, car.getModel());
            preparedStatementCar.executeUpdate();

            resultSet = preparedStatementCar.getGeneratedKeys();

            if (resultSet.next()) {
                idCar = resultSet.getLong(1);
            }
            setCarInfo(connection, idCar, car.getInfo());
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.warn("SQLException in method add(@NotNull Car car) (connection.rollback())", ex);
            }
            throw new DaoException("SQLException in method add(Car car)", e);
        } finally {
            close(connection, preparedStatementCar, resultSet);

        }
        return idCar;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#delete(Long)
     */
    @Override
    public void delete(@NotNull Long idCar) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;

        try {
            connection.setAutoCommit(false);

            ReviewDaoImpl.getInstance().deleteAllReviewByIdCar(connection, idCar);
            ReserveCarDaoImpl.getInstance().deleteAllReserveByIdCar(connection, idCar);
            deleteInfoCar(connection, idCar);
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_DELETE_CAR.getQuery());
            preparedStatement.setLong(1, idCar);
            preparedStatement.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.warn("SQLException in method delete(@NotNull Long idCar) (connection.rollback())", ex);
            }
            throw new DaoException("SQLException in method deleteCarById(long idCar)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * set Car Info
     *
     * @param connection the connection
     * @param idCar      the idCar
     * @param info       the info
     * @throws DaoException the dao exception
     */
    private void setCarInfo(Connection connection, @NotNull Long idCar, String info) throws DaoException {
        final String DEFAULT_INFO = "none";
        try {
            PreparedStatement preparedStatementCarInfo = connection.prepareStatement(SqlCarQuery.SQL_SET_CAR_INFO.getQuery());

            preparedStatementCarInfo.setLong(2, idCar);
            if (info != null && !info.isEmpty()) {
                preparedStatementCarInfo.setString(1, info);
            } else {
                preparedStatementCarInfo.setString(1, DEFAULT_INFO);
            }

            preparedStatementCarInfo.executeUpdate();
            preparedStatementCarInfo.close();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method setCarInfo(ConnectionProxy connection, long idCar)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#update(Long, Object)
     */
    @Override
    public void update(@NotNull Long idCar, @NotNull Car car) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_UPDATE_CAR.getQuery());
            preparedStatement.setString(1, car.getName());
            preparedStatement.setString(2, car.getModel());
            preparedStatement.setInt(3, car.getPrice());
            preparedStatement.setLong(4, idCar);
            preparedStatement.executeUpdate();

            preparedStatement.clearParameters();
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_UPDATE_CAR_TEXT.getQuery());
            preparedStatement.setString(1, car.getInfo());
            preparedStatement.setLong(2, idCar);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.warn("SQLException in method add() (connection.rollback())", ex);
            }
            throw new DaoException("SQLException in method update(@NotNull Long IdCar, @NotNull Car car)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * delete Info Car
     *
     * @param connection the connection
     * @param idCar      the idCar
     * @throws DaoException the dao exception
     */
    private void deleteInfoCar(Connection connection, @NotNull Long idCar) throws DaoException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_DELETE_CAR_INFO.getQuery());
            preparedStatement.setLong(1, idCar);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteInfoCar(Connection connection, @NotNull Long idCar)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#addImageById(Long, InputStream)
     */
    @Override
    public void addImageById(@NotNull Long idCar, @NotNull InputStream inputStream) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            Long idCarInfo = findIdCarInfo(connection, idCar);
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_ADD_IMAGE_BY_ID_CAR.getQuery());
            preparedStatement.setBinaryStream(1, inputStream);
            preparedStatement.setLong(2, idCarInfo);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method addImageById(@NotNull Long idCar, InputStream inputStream)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * find Id Car Info
     *
     * @param connection the connection
     * @param idCar      the idCar
     * @throws DaoException the dao exception
     */
    private Long findIdCarInfo(Connection connection, Long idCar) throws DaoException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long infoId = null;
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_FIND_ID_CAR_INFO.getQuery());
            preparedStatement.setLong(1, idCar);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                infoId = resultSet.getLong(DaoСolumnName.ID_CAR_INFO.getName());
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findCarById()", e);
        } finally {
            close(connection, preparedStatement, resultSet);

        }
        return infoId;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#findAllImageById(Long)
     */
    @Override
    public List<Long> findAllImageById(@NotNull Long idCar) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Long> result = new ArrayList();
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_FIND_IMAGE_BY_ID_CAR.getQuery());
            preparedStatement.setLong(1, idCar);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Long idImage = resultSet.getLong(DaoСolumnName.ID_CAR_IMAGES.getName());
                result.add(idImage);
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findAllImageById(@NotNull Long idCar)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#findImageById(Long)
     */
    @Override
    public InputStream findImageById(@NotNull Long idImage) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        InputStream image = null;
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_FIND_IMAGE_BY_ID.getQuery());
            preparedStatement.setLong(1, idImage);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                image = resultSet.getBinaryStream(DaoСolumnName.CAR_IMAGES.getName());

            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findImageByIdUser(Long idImage)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return image;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#deleteImageById(Long)
     */
    @Override
    public void deleteImageById(@NotNull Long idImage) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_DELETE_IMAGE_BY_ID.getQuery());
            preparedStatement.setLong(1, idImage);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteImageById(@NotNull Long idImage)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll()
     */
    @Override
    public List<Car> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        Statement statement = null;
        List<Car> cars;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SqlCarQuery.SQL_FIND_ALL.getQuery());
            cars = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findAll()", e);
        } finally {
            close(connection, statement);
        }
        return cars;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#findPartCars(int, int)
     */
    @Override
    public List<Car> findPartCars(int limit, int offset) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Car> cars;
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_FIND_PART_CARS.getQuery());
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            resultSet = preparedStatement.executeQuery();

            cars = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method  findPartCars(int limit, int offset)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return cars;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll(Long)
     */
    @Override
    public List<Car> findAll(@NotNull Long id) {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findById(Long)
     */
    @Override
    public Car findById(@NotNull Long idCar) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Car car = new Car();
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_FIND_CAR_BY_ID.getQuery());
            preparedStatement.setLong(1, idCar);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                car = build(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findCarById()", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return car;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarDao#findAllImage()
     */
    @Override
    public List<Long> findAllImage() throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        List<Long> result = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SqlCarQuery.SQL_FIND_ALL_IMAGES.getQuery());

            while (resultSet.next()) {
                Long idImage = resultSet.getLong(DaoСolumnName.ID_CAR_IMAGES.getName());
                result.add(idImage);
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findAllImage()", e);
        } finally {
            close(connection, statement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#lockCar(Long)
     */
    @Override
    public void lockCar(@NotNull Long idCar) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_SET_LOCK_BY_ID.getQuery());
            preparedStatement.setLong(1, idCar);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method lockСar(long userId)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#unLockCar(Long)
     */
    @Override
    public void unLockCar(@NotNull Long idCar) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_SET_UNLOCK_BY_ID.getQuery());
            preparedStatement.setLong(1, idCar);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method unLockСar(long userId)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarDao#checkCar(Long)
     */
    @Override
    public boolean checkCar(@NotNull Long idCar) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        boolean result;
        try {
            preparedStatement = connection.prepareStatement(SqlCarQuery.SQL_CHECK_CAR.getQuery());
            preparedStatement.setLong(1, idCar);
            resultSet = preparedStatement.executeQuery();
            result = resultSet.next();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method  checkCar(@NotNull Long idCar)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public Car build(ResultSet resultSet) throws DaoException {
        Car car = new Car();
        try {
            car.setIdCar(resultSet.getLong(DaoСolumnName.CAR_ID.getName()));
            car.setInfo(resultSet.getString(DaoСolumnName.CAR_INFO_TEXT.getName()));
            car.setName(resultSet.getString(DaoСolumnName.CAR_NAME.getName()));
            car.setModel(resultSet.getString(DaoСolumnName.CAR_MODEL.getName()));
            car.setPrice(resultSet.getInt(DaoСolumnName.CAR_PRICE.getName()));
            car.setStatus(resultSet.getBoolean(DaoСolumnName.CAR_STATUS.getName()));

        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return car;
    }
}
