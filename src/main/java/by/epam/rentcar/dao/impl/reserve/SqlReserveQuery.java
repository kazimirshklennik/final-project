package by.epam.rentcar.dao.impl.reserve;

/**
 * The Enum SqlReserveQuery.
 */
public enum SqlReserveQuery {

    /**
     * The sql add reserve.
     */
    SQL_ADD_RESERVE("INSERT INTO `car_rent`.`user_has_car` (`status`,`user_id_user`, `car_id_auto`, `date_booking`, `date_return`) VALUES (?,?,?,?,?);"),

    /**
     * The sql find all reserves.
     */
    SQL_FIND_ALL("SELECT `idreserve`,`status`,`date_booking`, `date_return`, `user_id_user`,`car_id_auto` FROM `car_rent`.`user_has_car`"),

    /**
     * The sql find all reserve by id.
     */
    SQL_FIND_ALL_BY_ID("SELECT `idreserve`,`status`,`date_booking`, `date_return`, `user_id_user`,`car_id_auto` FROM `car_rent`.`user_has_car`" +
            "WHERE `user_id_user`=?;"),

    /**
     * The sqldelete reserve.
     */
    SQL_DELETE_RESERVE("DELETE FROM `car_rent`.`user_has_car` WHERE (`idreserve` = ?);"),

    /**
     * The sql delete all reserve by id user.
     */
    SQL_DELETE_ALL_RESERVE_BY_ID_USER("DELETE FROM `car_rent`.`user_has_car` WHERE (`user_id_user` = ?);"),

    /**
     * The sql delete all reserve by id car.
     */
    SQL_DELETE_ALL_RESERVE_BY_ID_CAR("DELETE FROM `car_rent`.`user_has_car` WHERE (`car_id_auto` = ?);"),

    /**
     * The sql check all reserve by id user.
     */
    SQL_CHECK_RESERVE_BY_ID_USER("SELECT `car_id_auto` FROM `car_rent`.`user_has_car` WHERE `user_id_user`=? AND `status`='false';"),

    /**
     * The sql find reserve by id.
     */
    SQL_FIND_RESERVE_BY_ID("SELECT `idreserve`,`status`,`date_booking`, `date_return`, `user_id_user`, `car_id_auto`, `status` FROM `car_rent`.`user_has_car`" +
            "WHERE `idreserve`=?;"),

    /**
     * The sql set status.
     */
    SQL_SET_STATUS("UPDATE `car_rent`.`user_has_car` SET `status` = ? WHERE (`idreserve` = ?);");


    /**
     * The query.
     */
    private final String query;

    /**
     * Instantiates a new sql reserve query.
     *
     * @param text the text
     */
    SqlReserveQuery(String text) {

        this.query = text;
    }

    /**
     * getQuery.
     *
     * @return string
     */
    public String getQuery() {
        return query;
    }
}
