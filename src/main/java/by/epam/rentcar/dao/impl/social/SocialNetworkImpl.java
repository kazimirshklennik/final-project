package by.epam.rentcar.dao.impl.social;

import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.ParseResult;
import by.epam.rentcar.dao.impl.user.SqlUserQuery;
import by.epam.rentcar.entity.social.SocialNetwork;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;

import java.sql.*;
import java.util.List;

/**
 * The Class SocialNetworkImpl.
 */
public class SocialNetworkImpl implements CarRentDao<SocialNetwork>, ParseResult {

    /**
     * The dao.
     */
    private static SocialNetworkImpl instance = new SocialNetworkImpl();

    /**
     * Instantiates a new SocialNetworkImpl dao impl.
     */
    private SocialNetworkImpl() {
    }

    /**
     * Gets the single instance of Social Network Impl.
     *
     * @return single instance of ReviewDaoImpl
     */
    public static SocialNetworkImpl getInstance() {
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#add(Object)
     */
    @Override
    public Long add(SocialNetwork socialNetwork) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatementCar = null;
        ResultSet resultSet = null;
        Long idSocialNetwork = -1L;
        try {

            preparedStatementCar = connection.prepareStatement(SqlSocialNetworkQuery.SQL_ADD_SOCIAL_NETTWORK.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatementCar.setLong(2, socialNetwork.getIdUserInfo());
            preparedStatementCar.setString(1, socialNetwork.getUrl());
            preparedStatementCar.executeUpdate();

            resultSet = preparedStatementCar.getGeneratedKeys();

            if (resultSet.next()) {
                idSocialNetwork = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method add(SocialNetwork socialNetwork)", e);
        } finally {
            close(connection, preparedStatementCar, resultSet);

        }
        return idSocialNetwork;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#delete(Long)
     */
    @Override
    public void delete(Long idSocialNetwork) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlSocialNetworkQuery.SQL_DELETE_SOCIAL_NETWORK.getQuery());
            preparedStatement.setLong(1, idSocialNetwork);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(Long idSocialNetwork)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * delete All Social Network By Id user
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    public void deleteAllSocialNetworkByIdUser(Connection connection, Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long idInfo = 0L;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_ID_INFO_BY_ID_USER.getQuery());
            preparedStatement.setLong(1, idUser);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                idInfo = resultSet.getLong(DaoСolumnName.ID_INFO_USER.getName());
            }
            preparedStatement.clearParameters();
            preparedStatement = connection.prepareStatement(SqlSocialNetworkQuery.SQL_DELETE_ALL_SOCIAL_NETWORK_BY_ID.getQuery());
            preparedStatement.setLong(1, idInfo);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteAllSocialNetworkByIdUser(Connection connection,Long idUser)", e);
        } finally {
            close(preparedStatement, resultSet);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findById(Long)
     */
    @Override
    public SocialNetwork findById(Long id) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#update(Long, Object)
     */
    @Override
    public void update(Long idsocialNetwork, SocialNetwork socialNetwork) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlSocialNetworkQuery.SQL_UPDATE_SOCIAL_NETWORK.getQuery());


            preparedStatement.setString(1, socialNetwork.getUrl());
            preparedStatement.setLong(2, idsocialNetwork);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method update(Long idsocialNetwork, SocialNetwork socialNetwork)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findAll(Long)
     */
    @Override
    public List<SocialNetwork> findAll(Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<SocialNetwork> socialNetworks;
        try {

            preparedStatement = connection.prepareStatement(SqlSocialNetworkQuery.SQL_FIND_ALL_BY_ID.getQuery());
            preparedStatement.setLong(1, idUser);
            resultSet = preparedStatement.executeQuery();
            socialNetworks = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<SocialNetwork> findAll(Long idUser)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return socialNetworks;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll()
     */
    @Override
    public List<SocialNetwork> findAll() throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public SocialNetwork build(ResultSet resultSet) throws DaoException {
        SocialNetwork socialNetwork = new SocialNetwork();
        try {
            socialNetwork.setIdSocialNetwork(resultSet.getLong(DaoСolumnName.ID_SOCIAL_NETWORK.getName()));
            socialNetwork.setIdUserInfo(resultSet.getLong(DaoСolumnName.ID_INFO_SOCIAL_NETWORK.getName()));
            socialNetwork.setUrl(resultSet.getString(DaoСolumnName.URL.getName()));
            socialNetwork.setIdUser(resultSet.getLong(DaoСolumnName.ID_USER.getName()));

        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return socialNetwork;
    }
}
