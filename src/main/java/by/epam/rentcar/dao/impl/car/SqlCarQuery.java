package by.epam.rentcar.dao.impl.car;

/**
 * The enum SqlCarQuery.
 */
public enum SqlCarQuery {

    /** The sql insert car. */
    SQL_INSERT_CAR("INSERT INTO `car_rent`.`car` (`car_name`, `car_model`, `car_rating`, `car_price`, `car_status`) VALUES (?,?,0,0,1);"),

    /** The sql find all cara. */
    SQL_FIND_ALL("SELECT `car_name`, `id_auto`, `car_model`,`car_rating`,`car_price`,`car_status`,`car_info_text` FROM `car_rent`.`car`" +
                         "JOIN `car_rent`.`car_info`" +
                         "ON `car`.`id_auto`=`car_info`.`car_id_auto`;"),

    /** The sql find part cara. */
    SQL_FIND_PART_CARS("SELECT `car_name`, `id_auto`, `car_model`,`car_rating`,`car_price`,`car_status`,`car_info_text` FROM `car_rent`.`car`" +
            "JOIN `car_rent`.`car_info`" +
            "ON `car`.`id_auto`=`car_info`.`car_id_auto` LIMIT ? OFFSET ?;"),

    /** The sql check car. */
    SQL_CHECK_CAR("SELECT `car_name` FROM `car_rent`.`car` where `id_auto`=?;"),

    /** The sql find all images. */
    SQL_FIND_ALL_IMAGES("SELECT `idcar_images` FROM `car_rent`.`car_images`;"),

    /** The sql find image by id. */
    SQL_FIND_IMAGE_BY_ID("SELECT `car_images` FROM `car_rent`.`car_images`" +
            "WHERE idcar_images=?;"),

    /** The sql delete image by id. */
    SQL_DELETE_IMAGE_BY_ID("DELETE FROM `car_rent`.`car_images` WHERE (`idcar_images` = ?);"),

    /** The sql find car by id. */
    SQL_FIND_CAR_BY_ID("SELECT `car_name`, `id_auto`, `car_model`,`car_rating`,`car_price`,`car_status`,`car_info_text` FROM `car_rent`.`car`" +
                            "JOIN `car_rent`.`car_info`" +
                            "ON `car`.`id_auto`=`car_info`.`car_id_auto`" +
                            "WHERE `id_auto`=?;"),

    /** The sql find image by id car. */
    SQL_FIND_IMAGE_BY_ID_CAR("SELECT `idcar_images` FROM `car_rent`.`car_info`" +
            "JOIN `car_rent`.`car_images` ON `car_info_id_car_info`=`id_car_info`" +
            "JOIN `car_rent`.`car` ON `car_id_auto`=`id_auto`" +
            "WHERE `id_auto`=?;"),

    /** The sql add image by id car. */
    SQL_ADD_IMAGE_BY_ID_CAR("INSERT INTO `car_rent`.`car_images` (`car_images`, `car_info_id_car_info`) VALUES (?, ?);"),

    /** The sql find id car info. */
    SQL_FIND_ID_CAR_INFO("SELECT `id_car_info` FROM `car_rent`.`car_info`" +
            "WHERE `car_id_auto`=?;"),

    /** The sql set default car info. */
    SQL_SET_CAR_INFO("INSERT INTO `car_rent`.`car_info` (`car_info_text`, `car_id_auto`) VALUES (?, ?);"),

    /** The sql set lock by id. */
    SQL_SET_LOCK_BY_ID("UPDATE `car_rent`.`car` SET `car_status` = '0' WHERE (`id_auto` = ?);"),

    /** The sql set unlock by id. */
    SQL_SET_UNLOCK_BY_ID("UPDATE `car_rent`.`car` SET `car_status` = '1' WHERE (`id_auto` = ?);"),

    /** The sql update price by id. */
    SQL_UPDATE_CAR_PRICE9("UPDATE `car_rent`.`car` SET `car_price` = ? WHERE (`id_auto` = ?);"),

    /** The sql update rating by id. */
    SQL_UPDATE_CAR_RATING("UPDATE `car_rent`.`car` SET `car_rating` = ? WHERE (`id_auto` = ?);"),

    /** The sql update car text. */
    SQL_UPDATE_CAR_TEXT("UPDATE `car_rent`.`car_info` SET `car_info_text` = ? WHERE (`car_id_auto` = ?);"),

    /** The sql delete car info. */
    SQL_DELETE_CAR_INFO("DELETE FROM `car_rent`.`car_info` WHERE (`car_id_auto`=?);"),

    /** The sql update car. */
    SQL_UPDATE_CAR("UPDATE `car_rent`.`car` SET `car_name` = ?, `car_model` = ?, `car_price` = ? WHERE (`id_auto` = ?);"),

    /** The sql update info. */
    SQL_UPDATE_INFO("UPDATE `car_rent`.`car_info` SET `car_info_text` = ? WHERE (`car_id_auto` = ?);"),

    /** The sql delete car. */
    SQL_DELETE_CAR("DELETE FROM `car_rent`.`car` WHERE (`id_auto` = ?);");

    /** The query. */
    private final String query;

    /**
     * Instantiates a new sql car query.
     *
     * @param text the text
     */
    SqlCarQuery(String text) {

        this.query = text;
    }

    /**
     * getQuery.
     *
     * @return string
     */
    public String getQuery(){
        return query;
    }
}
