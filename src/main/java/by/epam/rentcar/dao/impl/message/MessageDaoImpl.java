package by.epam.rentcar.dao.impl.message;

import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.MessageDao;
import by.epam.rentcar.dao.ParseResult;
import by.epam.rentcar.entity.Message;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The Class MessageDaoImpl.
 */
public class MessageDaoImpl implements CarRentDao<Message>, ParseResult, MessageDao {

    /**
     * The dao.
     */
    private static MessageDaoImpl instance = new MessageDaoImpl();

    /**
     * Instantiates a new MessageDaoImpl dao impl.
     */
    private MessageDaoImpl() {
    }

    /**
     * Gets the single instance of Message Dao Impl.
     *
     * @return single instance of MessageDaoImpl
     */
    public static MessageDaoImpl getInstance() {
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#add(Object)
     */
    @Override
    public Long add(Message message) throws DaoException {

        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        long messageId = 0;
        try {
            preparedStatement = connection.prepareStatement(SqlMessageQuery.SQL_ADD_MESSAGE.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setLong(1, System.currentTimeMillis());
            preparedStatement.setString(2, message.getText());
            preparedStatement.setLong(3, message.getIdRecipient());
            preparedStatement.setLong(4, message.getIdSender());

            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                messageId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method add()", e);
        } finally {
            close(connection, preparedStatement, resultSet);

        }
        return messageId;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll(Long)
     */
    @Override
    public List<Message> findAll(@NotNull Long senderId) throws DaoException {
        List<Message> message;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(SqlMessageQuery.SQL_FIND_ALL_MESSAGE_BY_SENDER.getQuery());
            preparedStatement.setLong(1, senderId);
            resultSet = preparedStatement.executeQuery();
            message = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<String> findAll(long userId)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return message;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.MessageDao#findChat(Long, Long)
     */
    @Override
    public List<Message> findChat(@NotNull Long idSender, @NotNull Long idRecipient) throws DaoException {
        List<Message> message;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(SqlMessageQuery.SQL_FIND_CHAT.getQuery());

            preparedStatement.setLong(1, idSender);
            preparedStatement.setLong(2, idRecipient);
            preparedStatement.setLong(3, idRecipient);
            preparedStatement.setLong(4, idSender);
            resultSet = preparedStatement.executeQuery();
            message = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<String> findAll(long userId)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return message;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.MessageDao#findAllUserChat(Long)
     */
    @Override
    public List<Long> findAllUserChat(@NotNull Long idUser) throws DaoException {
        List<Long> list = new ArrayList<>();
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(SqlMessageQuery.SQL_FIND_ALL_USERS_CHAT.getQuery());

            preparedStatement.setLong(1, idUser);
            preparedStatement.setLong(2, idUser);
            Set<Long> users = new HashSet<>();
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                users.add(resultSet.getLong(DaoСolumnName.RECIPIENT.getName()));
                users.add(resultSet.getLong(DaoСolumnName.SENDER.getName()));
            }
            list.addAll(users);
            list.remove(idUser);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<String> findAll(long userId)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return list;
    }

    /**
     * delete All Message By Id User.
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    public void deleteAllMessageByIdUser(Connection connection, Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlMessageQuery.SQL_DELETE_ALL_MWSSAGE_BY_ID_USER.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.setLong(2, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(long idMessage)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll()
     */
    @Override
    public List<Message> findAll() throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#delete(Long)
     */
    @Override
    public void delete(@NotNull Long idMessage) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlMessageQuery.SQL_DELETE_MESSAGE_BY_ID.getQuery());
            preparedStatement.setLong(1, idMessage);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(long idMessage)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findById(Long)
     */
    @Override
    public Message findById(@NotNull Long Id) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#update(Long, Object)
     */
    @Override
    public void update(@NotNull Long Id, @NotNull Message message) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public Message build(ResultSet resultSet) throws DaoException {
        Message message = new Message();
        try {
            message.setIdMessage(resultSet.getLong(DaoСolumnName.ID_MESSAGE.getName()));
            message.setText(resultSet.getString(DaoСolumnName.TEXT.getName()));
            message.setDate(resultSet.getLong(DaoСolumnName.DATE_MESSAGE.getName()));
            message.setIdSender(resultSet.getLong(DaoСolumnName.SENDER.getName()));
            message.setIdRecipient(resultSet.getLong(DaoСolumnName.RECIPIENT.getName()));

        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return message;
    }
}
