package by.epam.rentcar.dao.impl.user;

import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.ParseResult;
import by.epam.rentcar.dao.UserDao;
import by.epam.rentcar.dao.impl.message.MessageDaoImpl;
import by.epam.rentcar.dao.impl.reserve.ReserveCarDaoImpl;
import by.epam.rentcar.dao.impl.review.ReviewDaoImpl;
import by.epam.rentcar.dao.impl.social.SocialNetworkImpl;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.sql.*;
import java.util.List;

/**
 * The Class SocialNetworkImpl.
 */
public class UserDaoImpl implements CarRentDao<User>, ParseResult<User>, UserDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(UserDaoImpl.class);

    /**
     * The UserDaoImpl.
     */
    private final static UserDaoImpl instance = new UserDaoImpl();

    /**
     * Instantiates a new UserDaoImpl dao impl.
     */
    private UserDaoImpl() {
    }

    /**
     * Gets the single instance of  User Dao Impl.
     *
     * @return single instance of  UserDaoImpl
     */
    public static UserDaoImpl getInstance() {
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#add(Object)
     */
    @Override
    public Long add(@NotNull User user) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatementUser = null;
        ResultSet resultSet = null;

        Long userId = null;
        try {
            connection.setAutoCommit(false);
            Long roleId = addRole(connection, user.getRole());
            preparedStatementUser = connection.prepareStatement(SqlUserQuery.SQL_INSERT_USER.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatementUser.setString(1, user.getLogin());
            preparedStatementUser.setString(2, user.getPassword());
            preparedStatementUser.setString(3, user.getEmail());
            preparedStatementUser.setLong(4, System.currentTimeMillis());
            preparedStatementUser.setLong(5, roleId);
            preparedStatementUser.executeUpdate();

            resultSet = preparedStatementUser.getGeneratedKeys();
            if (resultSet.next()) {
                userId = resultSet.getLong(1);
                addUserInfo(connection, userId);
            }
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.warn("SQLException in method add() (connection.rollback())", ex);
            }
            throw new DaoException("SQLException in method add()", e);
        } finally {
            close(connection, preparedStatementUser, resultSet);
        }
        return userId;
    }

    /**
     * add Role
     *
     * @param connection the connection
     * @param role       the role
     * @throws DaoException the dao exception
     */
    private long addRole(Connection connection, Role role) throws DaoException {
        PreparedStatement preparedStatementRole = null;
        ResultSet resultSet = null;
        long roleId = 0;
        try {

            preparedStatementRole = connection.prepareStatement(SqlUserQuery.SQL_INSERT_ROLE.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);

            preparedStatementRole.setString(1, role.getRole());
            preparedStatementRole.executeUpdate();

            resultSet = preparedStatementRole.getGeneratedKeys();
            if (resultSet.next()) {
                roleId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method addRole()", e);
        } finally {
            close(preparedStatementRole, resultSet);
        }
        return roleId;
    }

    /**
     * add User Info
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    private void addUserInfo(Connection connection, Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_INSERT_USER_INFO.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setLong(1, idUser);
            preparedStatement.setLong(2, System.currentTimeMillis());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method addRole()", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#addLastActivity(Long)
     */
    @Override
    public void addLastActivity(@NotNull Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_INSERT_LAST_ACTIVITY.getQuery());
            preparedStatement.setLong(1, System.currentTimeMillis());
            preparedStatement.setLong(2, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method addLastActivity(@NotNull Long idUser)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#findEmailByLogin(String)
     */
    @Override
    public String findEmailByLogin(@NotNull String login) throws DaoException {
        String result = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_EMAIL_BY_LOGIN.getQuery());
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString(DaoСolumnName.EMAIL.getName());
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findEmailByLogin(String login)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#findIdByLogin(String)
     */
    @Override
    public Long findIdByLogin(@NotNull String login) throws DaoException {
        long result = -1;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_ID_BY_LOGIN.getQuery());
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getLong(DaoСolumnName.ID_USER.getName());
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findIdByLogin(String login)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#update(Long, Object)
     */
    @Override
    public void update(@NotNull Long id, @NotNull User user) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        DateCreator dateCreator = new DateCreator();

        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_UPDATE_USER_INFO.getQuery());
            preparedStatement.setLong(1, dateCreator.createLong(user.getInfo().getBirthday()));
            preparedStatement.setString(2, user.getInfo().getCountry());
            preparedStatement.setString(3, user.getInfo().getCity());
            preparedStatement.setString(4, user.getInfo().getSex());
            preparedStatement.setString(5, user.getInfo().getUserText());
            preparedStatement.setString(6, user.getInfo().getFirstName());
            preparedStatement.setString(7, user.getInfo().getLastName());
            preparedStatement.setInt(8, user.getInfo().getAge());
            preparedStatement.setLong(9, id);
            preparedStatement.executeUpdate();
            checkAge(connection, user.getIdUser(), user.getInfo().getBirthday());

        } catch (SQLException e) {
            throw new DaoException("SQLException in method update(long Id, User user)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#updatePhotoUser(Long, InputStream)
     */
    @Override
    public void updatePhotoUser(@NotNull Long userId, @NotNull InputStream inputStream) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_UPDATE_USER_IMAGE.getQuery());
            preparedStatement.setBinaryStream(1, inputStream);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("SQLException in method updatePhotoUser", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#checkLogin(String)
     */
    @Override
    public boolean checkLogin(@NotNull String login) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        boolean result;
        try {

            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_CHECK_BY_LOGIN.getQuery());
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            result = resultSet.next();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method checkLogin(String login)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#checkEmail(String)
     */
    @Override
    public boolean checkEmail(@NotNull String email) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        boolean result;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_CHECK_BY_EMAIL.getQuery());
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            result = resultSet.next();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method checkEmail(String email)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#checkLoginPssword(String, String)
     */
    @Override
    public boolean checkLoginPssword(@NotNull String login, @NotNull String password) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean result;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_CHECK_BY_LOGIN_PASSWORD.getQuery());
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            result = resultSet.next();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method checkLoginPssword(String login, String password)", e);
        } finally {

            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#changePassword(String, String)
     */
    @Override
    public void changePassword(@NotNull String login, @NotNull String password) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_CHANGE_PASSWORD.getQuery());
            preparedStatement.setString(2, login);
            preparedStatement.setString(1, password);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("SQLException in method changePassword(String login, String password)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll()
     */
    @Override
    public List<User> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        ResultSet resultSet = null;
        Statement statement = null;
        List<User> users;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SqlUserQuery.SQL_FIND_ALL.getQuery());
            users = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findAll()", e);
        } finally {
            close(connection, statement, resultSet);
        }
        return users;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findAll(Long)
     */
    @Override
    public List<User> findAll(@NotNull Long userid) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findById(Long)
     */
    @Override
    public User findById(@NotNull Long userId) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = new User();
        try {

            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_USER_BY_ID.getQuery());
            preparedStatement.setLong(1, userId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = buildAllInfo(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findById(long userId)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return user;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#findIdByLogin(String)
     */
    @Override
    public User findByLogin(@NotNull String login) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = new User();
        try {

            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_USER_BY_LOGIN.getQuery());
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = buildAllInfo(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findByLogin(@NotNull String login)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return user;
    }

    /**
     * check Age
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @param bith       the birthday
     * @throws DaoException the dao exception
     */
    private void checkAge(Connection connection, @NotNull Long idUser, String bith) throws DaoException {

        PreparedStatement preparedStatement = null;
        DateCreator dateCreator = new DateCreator();
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_CHECK_AGE.getQuery());
            preparedStatement.setInt(1, dateCreator.getAge(bith));
            preparedStatement.setLong(2, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method checkAge(@NotNull Long idUser, int age)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#lockUser(Long)
     */
    @Override
    public void lockUser(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_SET_LOCK_BY_ID.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method lockUser(long userId)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#setOnline(Long)
     */
    @Override
    public void setOnline(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_SET_ONLINE.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method setOnline(@NotNull Long userId)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#setOffline(Long)
     */
    @Override
    public void setOffline(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_SET_OFFLINE.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method setOffline(@NotNull Long userId)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#unLockUser(Long)
     */
    @Override
    public void unLockUser(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_SET_UNLOCK_BY_ID.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method unLockUser(long userId)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * find Id Role
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    private Long findIdRole(Connection connection, @NotNull Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long idRole = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_ID_ROLE.getQuery());
            preparedStatement.setLong(1, idUser);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                idRole = resultSet.getLong(DaoСolumnName.ROLE_ID_ROLE.getName());
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findIdRole(Connection connection, Long idUser)", e);
        } finally {
            close(preparedStatement, resultSet);
        }
        return idRole;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#delete(Long)
     */
    @Override
    public void delete(@NotNull Long idUser) throws DaoException {

        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;

        try {
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit(false);
            SocialNetworkImpl.getInstance().deleteAllSocialNetworkByIdUser(connection, idUser);
            MessageDaoImpl.getInstance().deleteAllMessageByIdUser(connection, idUser);
            ReviewDaoImpl.getInstance().deleteAllreviewsByIdUser(connection, idUser);
            ReserveCarDaoImpl.getInstance().deleteAllReserveByIdUser(connection, idUser);
            Long idRole = findIdRole(connection, idUser);
            deleteUserInfo(connection, idUser);
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_DELETE_USER_BY_ID.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

            deleteRoleById(connection, idRole);
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.warn("SQLException in method add() (connection.rollback())", ex);
            }
            throw new DaoException("SQLException in method deleteUserById(long userid)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * delete User Info
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    private void deleteUserInfo(Connection connection, @NotNull Long idUser) throws DaoException {

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_DELETE_USER_INFO.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            throw new DaoException("SQLException in method deleteUserInfo(Connection connection, Long idUser)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * delete Role By Id
     *
     * @param connection the connection
     * @param idRole     the idRole
     * @throws DaoException the dao exception
     */
    private void deleteRoleById(Connection connection, @NotNull Long idRole) throws DaoException {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_DELETE_ROLE_BY_ID.getQuery());
            preparedStatement.setLong(1, idRole);
            preparedStatement.executeUpdate();
            close(preparedStatement);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteRoleById(long userId)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#findByLoginPassword(String, String)
     */
    @Override
    public User findByLoginPassword(@NotNull String login, @NotNull String hashedPassword) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = new User();
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_USER_BY_LOGIN_PASSWORD.getQuery());
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, hashedPassword);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = build(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method fingByLoginPassword((String login, String hashedPassword)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return user;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.UserDao#findImageByIdUser(Long)
     */
    @Override
    public InputStream findImageByIdUser(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        InputStream result = null;
        try {
            preparedStatement = connection.prepareStatement(SqlUserQuery.SQL_FIND_IMAGE_BY_ID_USER.getQuery());
            preparedStatement.setLong(1, idUser);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getBinaryStream(DaoСolumnName.USER_PHOTO.getName());
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in methodfindImageById(Long idUser)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public User buildAllInfo(ResultSet resultSet) throws DaoException {
        User user = new User();
        DateCreator dateCreator = new DateCreator();
        try {
            user.getInfo().setIdUserInfo(resultSet.getLong(DaoСolumnName.ID_INFO_USER.getName()));
            user.setIdUser(resultSet.getLong(DaoСolumnName.ID_USER.getName()));
            user.setStatusOnline(resultSet.getBoolean(DaoСolumnName.STATUS_ONLINE.getName()));
            user.setLogin(resultSet.getString(DaoСolumnName.LOGIN.getName()));
            user.setLockStatus(resultSet.getBoolean(DaoСolumnName.LOCK_STATUS.getName()));
            user.setEmail(resultSet.getString(DaoСolumnName.EMAIL.getName()));
            user.setRole(Role.valueOf(resultSet.getString(DaoСolumnName.ROLE.getName()).toUpperCase()));
            user.getInfo().setFirstName(resultSet.getString(DaoСolumnName.FIRST_NAME.getName()));
            user.getInfo().setLastName(resultSet.getString(DaoСolumnName.LAST_NAME.getName()));
            user.getInfo().setAge(resultSet.getInt(DaoСolumnName.USER_AGE.getName()));
            user.getInfo().setSex(resultSet.getString(DaoСolumnName.USER_SEX.getName()));
            user.getInfo().setCity(resultSet.getString(DaoСolumnName.USER_CITY.getName()));
            user.getInfo().setCountry(resultSet.getString(DaoСolumnName.USER_COUNTRY.getName()));
            user.getInfo().setUserText(resultSet.getString(DaoСolumnName.USER_TEXT.getName()));
            user.getInfo().setDateRegistration(dateCreator.create(resultSet.getLong(DaoСolumnName.USER_DATE_REGISTRATION.getName()), DateType.DATE_TIME));
            user.getInfo().setBirthday(dateCreator.create(resultSet.getLong(DaoСolumnName.USER_BIRTHDAY.getName()), DateType.DATE));
            user.getInfo().setLastActivity(dateCreator.create(resultSet.getLong(DaoСolumnName.USER_LAST_ACTIVITY.getName()), DateType.DATE_TIME));

        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return user;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public User build(ResultSet resultSet) throws DaoException {
        User user = new User();
        try {
            user.setStatusOnline(resultSet.getBoolean(DaoСolumnName.STATUS_ONLINE.getName()));
            user.setIdUser(resultSet.getLong(DaoСolumnName.ID_USER.getName()));
            user.setLockStatus(resultSet.getBoolean(DaoСolumnName.LOCK_STATUS.getName()));
            user.setLogin(resultSet.getString(DaoСolumnName.LOGIN.getName()));
            user.setEmail(resultSet.getString(DaoСolumnName.EMAIL.getName()));
            user.setRole(Role.valueOf(resultSet.getString(DaoСolumnName.ROLE.getName()).toUpperCase()));

        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return user;
    }
}
