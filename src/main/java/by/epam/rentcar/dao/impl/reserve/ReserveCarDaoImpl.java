package by.epam.rentcar.dao.impl.reserve;

import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.ParseResult;
import by.epam.rentcar.dao.ReserveCarDao;
import by.epam.rentcar.entity.Reserve;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.List;

/**
 * The Class ReserveCarDaoImpl.
 */
public class ReserveCarDaoImpl implements CarRentDao<Reserve>, ParseResult, ReserveCarDao {

    /**
     * The dao.
     */
    private static ReserveCarDaoImpl instance = new ReserveCarDaoImpl();

    /**
     * Instantiates a new ReserveCarDaoImpl dao impl.
     */
    private ReserveCarDaoImpl() {
    }

    /**
     * Gets the single instance of Reserve Car Dao Impl.
     *
     * @return single instance of ReserveCarDaoImpl
     */
    public static ReserveCarDaoImpl getInstance() {
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#add(Object)
     */
    @Override
    public Long add(@NotNull Reserve reserve) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        DateCreator dateCreator = new DateCreator();
        ResultSet resultSet = null;
        Long idReserve = -1L;
        try {
            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_ADD_RESERVE.getQuery(),
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setBoolean(1, reserve.isStatus());
            preparedStatement.setLong(2, reserve.getIdUser());
            preparedStatement.setLong(3, reserve.getIdCar());
            preparedStatement.setLong(4, dateCreator.createLong(reserve.getDateBooking()));
            preparedStatement.setLong(5, dateCreator.createLong(reserve.getDateReturn()));
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                idReserve = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method add()", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return idReserve;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findAll(Long)
     */
    @Override
    public Reserve findById(@NotNull Long idReserve) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Reserve reserve = new Reserve();
        try {

            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_FIND_RESERVE_BY_ID.getQuery());
            preparedStatement.setLong(1, idReserve);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                reserve = build(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findById(@NotNull Long idReserve)", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return reserve;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#delete(Long)
     */
    @Override
    public void delete(Long idReserve) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_DELETE_RESERVE.getQuery());
            preparedStatement.setLong(1, idReserve);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(long idReserve)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * delete All Reserve By Id User.
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    public void deleteAllReserveByIdUser(@NotNull Connection connection, @NotNull Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_DELETE_ALL_RESERVE_BY_ID_USER.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteAllReserveByIdUser(@NotNull Connection connection, @NotNull Long idUser)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * delete All Reserve By Id Car.
     *
     * @param connection the connection
     * @param idCar      the idCar
     * @throws DaoException the dao exception
     */
    public void deleteAllReserveByIdCar(@NotNull Connection connection, @NotNull Long idCar) throws DaoException {
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_DELETE_ALL_RESERVE_BY_ID_CAR.getQuery());
            preparedStatement.setLong(1, idCar);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteAllReserveByIdCar(@NotNull Connection connection, @NotNull Long idCar)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ReserveCarDao#checkActiveReserveByIdUser(Long)
     */
    @Override
    public Long checkActiveReserveByIdUser(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Long result = null;
        try {
            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_CHECK_RESERVE_BY_ID_USER.getQuery());
            preparedStatement.setLong(1, idUser);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getLong(DaoСolumnName.RESERVE_ID_CAR.getName());
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method checkActiveReserveByIdUser(Long idUser)", e);
        } finally {

            close(connection, preparedStatement, resultSet);
        }
        return result;

    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#update(Long, Object)
     */
    @Override
    public void update(@NotNull Long id, @NotNull Reserve object) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findAll(Long)
     */
    @Override
    public List<Reserve> findAll(@NotNull Long idUser) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Reserve> reserves;
        try {

            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_FIND_ALL_BY_ID.getQuery());
            preparedStatement.setLong(1, idUser);
            resultSet = preparedStatement.executeQuery();
            reserves = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findAll()", e);
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return reserves;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll()
     */
    @Override
    public List<Reserve> findAll() throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        List<Reserve> reserves;
        try {

            statement = connection.createStatement();
            resultSet = statement.executeQuery(SqlReserveQuery.SQL_FIND_ALL.getQuery());
            reserves = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findAll()", e);
        } finally {
            close(connection, statement, resultSet);
        }
        return reserves;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ReserveCarDao#setStatus(Long, boolean)
     */
    @Override
    public void setStatus(@NotNull Long idReserve, boolean status) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlReserveQuery.SQL_SET_STATUS.getQuery());
            preparedStatement.setLong(2, idReserve);
            preparedStatement.setBoolean(1, status);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(long idReserve)", e);
        } finally {
            close(connection, preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public Reserve build(ResultSet resultSet) throws DaoException {
        Reserve reserve = new Reserve();
        DateCreator dateCreator = new DateCreator();
        try {
            reserve.setIdReserve(resultSet.getLong(DaoСolumnName.ID_RESERVE.getName()));
            reserve.setStatus(resultSet.getBoolean(DaoСolumnName.RESERVE_STATUS.getName()));
            reserve.setIdUser(resultSet.getLong(DaoСolumnName.RESERVE_ID_USER.getName()));
            reserve.setIdCar(resultSet.getLong(DaoСolumnName.RESERVE_ID_CAR.getName()));
            reserve.setBooking(dateCreator.create(resultSet.getLong(DaoСolumnName.RESERVE_DATE_BOOKING.getName()), DateType.DATE));
            reserve.setdReturn(dateCreator.create(resultSet.getLong(DaoСolumnName.RESERVE_DATE_RETURN.getName()), DateType.DATE));

        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return reserve;
    }
}
