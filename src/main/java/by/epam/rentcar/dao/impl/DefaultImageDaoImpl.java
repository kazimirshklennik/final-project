package by.epam.rentcar.dao.impl;

import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.DefaultImageDao;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DefaultImageDaoImpl implements DefaultImageDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(DefaultImageDaoImpl.class);

    /**
     * The Constant SQL_SET_DEFAUIL_IMAGE.
     */
    private final String SQL_SET_DEFAUIL_IMAGE = "INSERT INTO `car_rent`.`default_data` (`id_default_data`,`default_image_user`,`default_image_car`) VALUES (?,?,?);";

    /**
     * The Constant SQL_FIND_DEFAUIL_IMAGE.
     */
    private final String SQL_FIND_DEFAUIL_IMAGE = "SELECT `default_image_user`, `default_image_car` FROM `car_rent`.`default_data`" +
            " WHERE `id_default_data`=?";

    /**
     * The DefaultImageDaoImpl.
     */
    private static DefaultImageDaoImpl instance= new DefaultImageDaoImpl();


    private DefaultImageDaoImpl() {
    }

    /**
     * Gets the single instance of Default Image Dao Impl.
     *
     * @return single instance of  DefaultImageDaoImpl
     */
    public static DefaultImageDaoImpl getInstance(){
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.DefaultImageDao#addDefaultImage(FileInputStream, InputStream)
     */
    @Override
    public void addDefaultImage(FileInputStream fileInputStream, InputStream image) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;

        try {

            preparedStatement = connection.prepareStatement(SQL_SET_DEFAUIL_IMAGE);
            preparedStatement.setLong(1, 2);
            preparedStatement.setBinaryStream(2, image);
            preparedStatement.setBinaryStream(3, image);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method addDefaultImage(InputStream image)", e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                if(connection!=null) {
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.warn("SQLException in method addDefaultImage(InputStream image) (connection.close())", e);
            }
        }

    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.DefaultImageDao#findDefaultImage(Long)
     */
    @Override
    public InputStream findDefaultImage(Long id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;

        ResultSet resultSet = null;
        InputStream result = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_FIND_DEFAUIL_IMAGE);
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getBinaryStream(DaoСolumnName.DEFAULT_IMAGE_USER.getName());
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findDefaultImage()", e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                if(connection!=null) {
                    connection.close();
                }

            } catch (SQLException e) {
                throw new DaoException("SQLException in method findImageByLogin(String login)" +
                        " close connection / preparedStatement", e);
            }
        }
        return result;
    }
}

