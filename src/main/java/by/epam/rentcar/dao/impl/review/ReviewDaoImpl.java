package by.epam.rentcar.dao.impl.review;

import by.epam.rentcar.dao.DaoСolumnName;
import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.ParseResult;
import by.epam.rentcar.dao.ReviewDao;
import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.pool.ConnectionPool;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;

import javax.validation.constraints.NotNull;
import java.sql.*;
import java.util.List;

/**
 * The Class ReviewDaoImpl.
 */
public class ReviewDaoImpl implements CarRentDao<Review>, ParseResult<Review>, ReviewDao {

    /**
     * The dao.
     */
    private static ReviewDaoImpl instance = new ReviewDaoImpl();

    /**
     * Instantiates a new ReviewDaoImpl dao impl.
     */
    private ReviewDaoImpl(){}

    /**
     * Gets the single instance of Review Dao Impl.
     *
     * @return single instance of ReviewDaoImpl
     */
    public static ReviewDaoImpl getInstance() {
        return instance;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#add(Object)
     */
    @Override
    public Long add(@NotNull Review review) throws DaoException {

        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;

        Long reviewId = -1L;
        try {
            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_ADD_REVIEW.getQuery(), Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setLong(5, review.getIdCar());
            preparedStatement.setLong(4, review.getIdUser());
            preparedStatement.setLong(3, 0);
            preparedStatement.setLong(2, System.currentTimeMillis());
            preparedStatement.setString(1, review.getReview());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                reviewId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("SQLException in method add() class ReviewDaoImpl", e);
        } finally {

            close(connection,preparedStatement,resultSet);
        }
        return reviewId;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#delete(Long)
     */
    @Override
    public void delete(@NotNull Long idReview) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_DELETE_REVIEW.getQuery());

            preparedStatement.setLong(1, idReview);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(long idReview)", e);
        } finally {
            close(connection,preparedStatement);
        }
    }

    /**
     * delete All Review By Id car.
     *
     * @param connection the connection
     * @param idCar     the idCar
     * @throws DaoException the dao exception
     */
    public void deleteAllReviewByIdCar(@NotNull Connection connection, @NotNull Long idCar) throws DaoException {

        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_DELETE_ALL_REVIEWS_BY_ID_CAR.getQuery());

            preparedStatement.setLong(1, idCar);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteAllReviewByIdCar(@NotNull Connection connection, @NotNull Long idCar)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * delete All Review By Id car.
     *
     * @param connection the connection
     * @param idUser     the idUser
     * @throws DaoException the dao exception
     */
    public void deleteAllreviewsByIdUser(@NotNull Connection connection, @NotNull Long idUser) throws DaoException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_DELETE_ALL_REVIEWS_BY_ID_USER.getQuery());
            preparedStatement.setLong(1, idUser);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete(long idReview)", e);
        } finally {
            close(preparedStatement);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#update(Long, Object)
     */
    @Override
    public void update(@NotNull Long idReview, Review review) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * (non-Javadoc)
     *
     * @see CarRentDao#findAll()
     */
    @Override
    public List<Review> findAll() throws DaoException {
        List<Review> reviews;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        Statement statement = null;
        ResultSet resultSet=null;
        try {

            statement = connection.createStatement();
            resultSet = statement.executeQuery(SqlReviewQuery.SQL_FIND_ALL_REVIEWS.getQuery());
            reviews = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<String> findAll()", e);
        } finally {
            close(connection,statement,resultSet);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findById(Long)
     */
    @Override
    public List<Review> findAll(@NotNull Long idUser) throws DaoException {
        List<Review> reviews;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        try {
            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_FIND_REVIEWS_BY_USER_ID.getQuery());

            preparedStatement.setLong(1,idUser);
            resultSet = preparedStatement.executeQuery();
            reviews = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<String> findAll(long userId)", e);
        } finally {
            close(connection,preparedStatement,resultSet);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ReviewDao#findPartReview(int, int)
     */
    @Override
    public List<Review> findPartReview(int limit, int offset) throws DaoException {
        List<Review> reviews;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        try {
            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_FIND_PART_REVIEWS.getQuery());
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            resultSet = preparedStatement.executeQuery();
            reviews = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findPartReview(int limit, int offset)", e);
        } finally {
            close(connection,preparedStatement,resultSet);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ReviewDao#findAllByIdCar(Long)
     */
    @Override
    public List<Review> findAllByIdCar(@NotNull Long idCar) throws DaoException {
        List<Review> reviews;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        try {
            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_FIND_REVIEWS_BY_CAR_ID.getQuery());
            preparedStatement.setLong(1, idCar);
            resultSet = preparedStatement.executeQuery();
            reviews = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method List<String> findAllByIdCar(@NotNull Long idCar)", e);
        } finally {
            close(connection,preparedStatement,resultSet);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ReviewDao#findPartReviewByIdCar(int, int, Long)
     */
    @Override
    public List<Review> findPartReviewByIdCar(int limit, int offset, @NotNull Long idCar) throws DaoException {
        List<Review> reviews;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        try {
            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_FIND_PART_REVIEWS_BY_ID_CAR.getQuery());
            preparedStatement.setLong(1, idCar);
            preparedStatement.setInt(2, limit);
            preparedStatement.setInt(3, offset);
            resultSet = preparedStatement.executeQuery();
            reviews = parseResultSet(resultSet);

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findPartReview(int limit, int offset)", e);
        } finally {
            close(connection,preparedStatement,resultSet);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.CarRentDao#findById(Long)
     */
    @Override
    public Review findById(@NotNull Long reviewId) throws DaoException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        Review review = new Review();
        try {

            preparedStatement = connection.prepareStatement(SqlReviewQuery.SQL_FIND_REVIEW_BY_ID.getQuery());
            preparedStatement.setLong(1, reviewId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                review = build(resultSet);
            }

        } catch (SQLException e) {
            throw new DaoException("SQLException in method findById(long reviewId)", e);
        } finally {
            close(connection,preparedStatement,resultSet);
        }
        return review;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.dao.ParseResult#build(ResultSet)
     */
    @Override
    public Review build(ResultSet resultSet) throws DaoException {
        Review review = new Review();
        DateCreator dateCreator = new DateCreator();
        try {
            review.setIdCar(resultSet.getLong(DaoСolumnName.CAR_ID_REVIEW.getName()));
            review.setReview(resultSet.getString(DaoСolumnName.REVIEW.getName()));
            review.setIdReview(resultSet.getLong(DaoСolumnName.ID_REVIEW.getName()));
            review.setDate(dateCreator.create(resultSet.getLong(DaoСolumnName.REVIEW_DATE.getName()), DateType.DATE_TIME));
            review.setIdUser(resultSet.getLong(DaoСolumnName.REVIEW_USER_ID.getName()));
        } catch (SQLException e) {
            throw new DaoException("SQLException in method build(ResultSet resultSet)" + e);
        }
        return review;
    }
}

