package by.epam.rentcar.dao.impl.message;

/**
 * The enum SqlMessageQuery.
 */
public enum SqlMessageQuery {

    /** The sql iadd message. */
    SQL_ADD_MESSAGE("INSERT INTO `car_rent`.`message` (`date_message`, `text`, `recipient`, `sender`) " +
            "VALUES (?, ?, ?, ?);"),

    /** The sql sind all messages by sender. */
    SQL_FIND_ALL_MESSAGE_BY_SENDER( "SELECT `date_message`, `text`,`recipient`,`sender` FROM `car_rent`.`message`" +
            "WHERE `sender`=?;"),

    /** The sql delete messege by id. */
    SQL_DELETE_MESSAGE_BY_ID("DELETE FROM `car_rent`.`message` WHERE (`idmessage` = ?);"),

    /** The sql find chat. */
    SQL_FIND_CHAT("SELECT `date_message`,`idmessage`, `text`,`recipient`,`sender` FROM `car_rent`.`message`" +
            "WHERE `recipient`=? AND `sender`=? OR `recipient`=? AND `sender`=?"),

    /** The sql find all user chat. */
    SQL_FIND_ALL_USERS_CHAT("SELECT `recipient`,`sender` FROM `car_rent`.`message`" +
            "WHERE `recipient`=? OR `sender`=?"),

    /** The sql find all messages by id user. */
    SQL_DELETE_ALL_MWSSAGE_BY_ID_USER("DELETE FROM `car_rent`.`message` WHERE `sender`=? OR `recipient`=?;");

    /** The query. */
    private final String query;

    /**
     * Instantiates a new sql message query.
     *
     * @param text the text
     */
    SqlMessageQuery(String text) {

        this.query = text;
    }

    /**
     * getQuery.
     *
     * @return string
     */
    public String getQuery(){
        return query;
    }
}
