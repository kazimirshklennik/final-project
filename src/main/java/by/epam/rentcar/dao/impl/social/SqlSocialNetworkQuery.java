package by.epam.rentcar.dao.impl.social;

public enum SqlSocialNetworkQuery {

    /**
     * The sql add social network.
     */
    SQL_ADD_SOCIAL_NETTWORK("INSERT INTO `car_rent`.`social_network` (`url`, `user_info_id`) VALUES (?, ?);"),

    /**
     * The sql delete social network.
     */
    SQL_DELETE_SOCIAL_NETWORK("DELETE FROM `car_rent`.`social_network` WHERE (`id_social_network` = ?);"),

    /**
     * The sql delete all social network by id user info.
     */
    SQL_DELETE_ALL_SOCIAL_NETWORK_BY_ID("DELETE FROM `car_rent`.`social_network` WHERE (`user_info_id` = ?);"),

    /**
     * The sql update social network.
     */
    SQL_UPDATE_SOCIAL_NETWORK("UPDATE `car_rent`.`social_network` SET `url` = ? WHERE (`id_social_network` = ?);"),

    /**
     * The sql delete all social network by id user.
     */
    SQL_FIND_ALL_BY_ID("SELECT `id_social_network`,`user_info_id`,`url`,`id_user` FROM `car_rent`.`social_network` " +
            "JOIN `car_rent`.`user_info`" +
            "ON `social_network`.`user_info_id` = `user_info`.`id_info`" +
            "JOIN `car_rent`.`user` u ON `user_info`.`user_id_user` = `u`.`id_user`" +
            "WHERE `id_user`=?;");

    /**
     * The query.
     */

    private final String query;

    /**
     * Instantiates a new sql Social Network query.
     *
     * @param text the text
     */
    SqlSocialNetworkQuery(String text) {

        this.query = text;
    }

    /**
     * getQuery.
     *
     * @return string
     */
    public String getQuery() {
        return query;
    }
}

