package by.epam.rentcar.dao.impl.user;

/**
 * The Enum SqlUserQuery.
 */
public enum SqlUserQuery {

    /** The sql insert user. */
    SQL_INSERT_USER("INSERT INTO `car_rent`.`user` (`login`, `password`, `email`, `date_registration`, `lock_status`, `role_id_role`) VALUES (?,?,?,?,0,?);"),

    /** The sql insert role. */
    SQL_INSERT_ROLE("INSERT INTO `car_rent`.`role` (`role`) VALUES (?);"),

    SQL_INSERT_USER_INFO("INSERT INTO `car_rent`.`user_info` (`user_id_user`,`last_activity`) VALUES (?,?);"),

    /** The sql find role by id. */
    SQL_FIND_ROLE_BY_ID("SELECT `role` FROM `car_rent`.`role` WHERE `id_role` =(?);"),

    /** The sql find all. */
    SQL_FIND_ALL("SELECT `id_user`,`status_online`, `login`, `email`,`role`,`lock_status`  FROM `car_rent`.`user`" +
            "JOIN `car_rent`.`role` ON `role`.`id_role`=`user`.`role_id_role`"),

    SQL_FIND_ID_ROLE("SELECT `role_id_role` FROM `car_rent`.`user` WHERE `id_user`=?;"),

    SQL_FIND_ID_INFO_BY_ID_USER("SELECT `id_info` FROM `car_rent`.`user_info` WHERE `user_id_user`=?;"),

    /** The sql find user by id. */
    SQL_FIND_USER_BY_ID("SELECT `id_user`,`id_info`,`login`,`last_activity`,`email`,`role`, `status_online`,`date_registration`,`sex`, `first_name`,`last-name`, `user_photo`, `user_birthday`,"+
            "`id_info`,`country`, `city`,`age`,`info_text`,`lock_status` FROM `car_rent`.`user`" +
            "JOIN `car_rent`.`role` ON `role`.`id_role`=`user`.`role_id_role`" +
            "JOIN `car_rent`.`user_info` ON `user`.`id_user` = `user_info`.`user_id_user`" +
            "WHERE (`id_user`=?);"),

    /** The sql find user by login. */
    SQL_FIND_USER_BY_LOGIN("SELECT `id_user`,`id_info`,`last_activity`,`login`,`email`,`status_online`,`role`,`date_registration`,`sex`, `first_name`,`last-name`, `user_photo`, `user_birthday`," +
            "`country`, `city`,`age`,`info_text`,`lock_status` FROM `car_rent`.`user`" +
            "JOIN `car_rent`.`role` ON `role`.`id_role`=`user`.`role_id_role`" +
            "JOIN `car_rent`.`user_info` ON `user`.`id_user` = `user_info`.`user_id_user`" +
            "WHERE (`login`=?);"),

    /** The sql find image by id user. */
    SQL_FIND_IMAGE_BY_ID_USER("SELECT `user_photo` FROM `car_rent`.`user`" +
             "JOIN `car_rent`.`user_info` ON `user`.`id_user` = `user_info`.`user_id_user`" +
            "WHERE `id_user`=?;"),

    /** The sql insert last activity. */
    SQL_INSERT_LAST_ACTIVITY("UPDATE `car_rent`.`user_info` SET `last_activity` = ? WHERE (`user_id_user` = ?);"),

    /** The sql delete user info. */
    SQL_DELETE_USER_INFO("DELETE FROM `car_rent`.`user_info` WHERE (`user_id_user` = ?);"),

    /** The sql delete role by id. */
    SQL_DELETE_ROLE_BY_ID("DELETE FROM `car_rent`.`role` WHERE `id_role` = ?;"),

    /** The sql delete user by id. */
    SQL_DELETE_USER_BY_ID("DELETE FROM `car_rent`.`user` WHERE `id_user` = ?;"),

    /** The sql set lock by id. */
    SQL_SET_LOCK_BY_ID("UPDATE `car_rent`.`user` SET `lock_status` = '1' WHERE (`id_user` = ?);"),

    /** The sql set online. */
    SQL_SET_ONLINE("UPDATE `car_rent`.`user` SET `status_online` = '1' WHERE (`id_user` = ?);"),

    /** The sql set offline. */
    SQL_SET_OFFLINE("UPDATE `car_rent`.`user` SET `status_online` = '0' WHERE (`id_user` = ?);"),

    /** The sql set unlock by id. */
    SQL_SET_UNLOCK_BY_ID("UPDATE `car_rent`.`user` SET `lock_status` = '0' WHERE (`id_user` = ?);"),

    /** The sql check user by login. */
    SQL_CHECK_BY_LOGIN("SELECT `login` FROM `car_rent`.`user` WHERE `login` =(?);"),

    /** The sql find id by login. */
    SQL_FIND_ID_BY_LOGIN("SELECT `id_user` FROM `car_rent`.`user` WHERE `login` =(?);"),

    /** The sql email by login. */
    SQL_FIND_EMAIL_BY_LOGIN("SELECT `email` FROM `car_rent`.`user` WHERE `login`=?;"),

    /** The sql update user photo. */
    SQL_UPDATE_USER_IMAGE("UPDATE `car_rent`.`user_info` SET `user_photo` = ? WHERE (`user_id_user` = ?);"),

    /** The sql find user by id. */
    SQL_UPDATE_USER_INFO("UPDATE `car_rent`.`user_info` SET `user_birthday` = ?," +
            "`country` = ?, `city` = ?, `sex` = ?, `info_text` = ?, `first_name`=?, `last-name`=?, `age`=?" +
            " WHERE `user_id_user` = ?;"),

    /** The sql change password. */
    SQL_CHANGE_PASSWORD("UPDATE `car_rent`.`user` SET `password` = ? WHERE (`login` = ?);"),

    /** The sql find user by login and password. */
    SQL_FIND_USER_BY_LOGIN_PASSWORD("SELECT `id_user`,`status_online`,`login`, `password`,`email`,`role`,`lock_status` FROM `car_rent`.`user`" +
            "JOIN `car_rent`.`role`" +
            "ON `role`.`id_role`=`user`.`role_id_role`" +
            "WHERE `login`=? AND `password`=?;"),

    /** The sql check login and password. */
    SQL_CHECK_BY_LOGIN_PASSWORD("SELECT `login`, `password` FROM `car_rent`.`user` " +
            "WHERE `login`=? AND `password`=?;"),

    /** The sql check age. */
    SQL_CHECK_AGE("UPDATE `car_rent`.`user_info` SET `age` = ? WHERE (`user_id_user` = ?);"),

    /** The sql check by email. */
    SQL_CHECK_BY_EMAIL("SELECT `login`, `password`,`email` FROM `car_rent`.`user` WHERE `email`=?");



    /** The query. */

    private final String query;

    /**
     * Instantiates a new sql user query.
     *
     * @param text the text
     */
    SqlUserQuery(String text) {

        this.query = text;
    }

    /**
     * getQuery.
     *
     * @return string
     */
    public String getQuery(){
        return query;
    }
}
