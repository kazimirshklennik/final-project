package by.epam.rentcar.dao;

import by.epam.rentcar.entity.Message;
import by.epam.rentcar.exception.DaoException;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The Interface MessageDao.
 */
public interface MessageDao {

    /**
     * find Chat.
     *
     * @param idSender    the idSender
     * @param idRecipient the idRecipient
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Message> findChat(@NotNull Long idSender, @NotNull Long idRecipient) throws DaoException;

    /**
     * find All User Chat.
     *
     * @param idUser the idUser
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Long> findAllUserChat(Long idUser) throws DaoException;
}
