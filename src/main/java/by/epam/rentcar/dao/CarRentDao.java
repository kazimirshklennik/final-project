package by.epam.rentcar.dao;

import by.epam.rentcar.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * The Interface CarRentDao.
 *
 * @param <T> the generic type
 */
public interface CarRentDao<T> {

    /**
     * The Constant LOGGER.
     */
    Logger LOGGER = LogManager.getLogger(CarRentDao.class);


    /**
     * Delete.
     *
     * @param id the id
     * @throws DaoException the dao exception
     */
    void delete(@NotNull Long id) throws DaoException;


    /**
     * Add long.
     *
     * @param t the t
     * @return the long
     * @throws DaoException the dao exception
     */
    Long add(@NotNull T t) throws DaoException;


    /**
     * Find by id t.
     *
     * @param id the id
     * @return the t
     * @throws DaoException the dao exception
     */
    T findById(@NotNull Long id) throws DaoException;

    /**
     * delete.
     *
     * @param id     the id
     * @param object the object
     * @throws DaoException the dao exception
     */
    void update(@NotNull Long id, T object) throws DaoException;


    /**
     * Find all list.
     *
     * @param id the id
     * @return the list
     * @throws DaoException the dao exception
     */
    List<T> findAll(@NotNull Long id) throws DaoException;


    /**
     * Find all list.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<T> findAll() throws DaoException;


    /**
     * Close.
     *
     * @param statement the statement
     */
    default void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                LOGGER.warn("SQLException in method close(Statement statement)", e);
            }
        }
    }

    /**
     * Close.
     *
     * @param connection the connection
     * @param statement  the statement
     */
    default void close(Connection connection, Statement statement) {
        if (statement != null && connection != null) {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                LOGGER.warn("SQLException in method close(Statement statement, ResultSet resultSet)", e);
            }
        }
    }


    /**
     * Close.
     *
     * @param statement the statement
     * @param resultSet the result set
     */
    default void close(Statement statement, ResultSet resultSet) {
        if (statement != null && resultSet != null) {
            try {
                resultSet.close();
                statement.close();
            } catch (SQLException e) {
                LOGGER.warn("SQLException in method close(Statement statement, ResultSet resultSet)", e);
            }
        }
    }


    /**
     * Close.
     *
     * @param connection the connection
     * @param statement  the statement
     * @param resultSet  the result set
     */
    default void close(Connection connection, Statement statement, ResultSet resultSet) {
        if (statement != null && resultSet != null && connection != null) {
            try {
                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                LOGGER.warn("SQLException in method close(Connection connection, Statement statement, ResultSet resultSet)", e);
            }
        }
    }
}
