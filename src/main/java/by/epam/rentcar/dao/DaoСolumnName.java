package by.epam.rentcar.dao;

/**
 * The enum DaoСolumnName.
 */
public enum DaoСolumnName {

    /**
     * The user login.
     */
    LOGIN("login"),

    /**
     * The id_user.
     */
    ID_USER("id_user"),

    /**
     * Id info user dao сolumn name.
     */
    ID_INFO_USER("id_info"),

    /**
     * The user_info_id.
     */
    ID_INFO_SOCIAL_NETWORK("user_info_id"),

    /**
     * The id_social_network.
     */
    ID_SOCIAL_NETWORK("id_social_network"),

    /**
     * The status_online.
     */
    STATUS_ONLINE("status_online"),

    /**
     * The role_id_role.
     */
    ROLE_ID_ROLE("role_id_role"),

    /**
     * The user password.
     */
    PASSWORD("password"),

    /**
     * The lock_status.
     */
    LOCK_STATUS("lock_status"),

    /**
     * The user email.
     */
    EMAIL("email"),

    /**
     * The ruser role.
     */
    ROLE("role"),

    /**
     * The car_model.
     */
    CAR_MODEL("car_model"),

    /**
     * The car_info_text.
     */
    CAR_INFO_TEXT("car_info_text"),

    /**
     * The id_auto.
     */
    CAR_ID("id_auto"),

    /**
     * The car_name.
     */
    CAR_NAME("car_name"),

    /**
     * The car_price.
     */
    CAR_PRICE("car_price"),

    /**
     * The car_status.
     */
    CAR_STATUS("car_status"),

    /**
     * The car_foto.
     */
    CAR_FOTO("car_foto"),

    /**
     * The command.
     */
    COMMAND("command"),

    /**
     * The review.
     */
    REVIEW("review"),

    /**
     * The user_id_user.
     */
    USER_ID_REVIEW("user_id_user"),

    /**
     * The car_id_auto.
     */
    CAR_ID_REVIEW("car_id_auto"),

    /**
     * The id_review.
     */
    ID_REVIEW("id_review"),

    /**
     * The rating.
     */
    REVIEW_RATING("rating"),

    /**
     * The foto.
     */
    FOTO_REVIEW("foto"),

    /**
     * The review_date.
     */
    REVIEW_DATE("review_date"),

    /**
     * The user_id_user.
     */
    REVIEW_USER_ID("user_id_user"),

    /**
     * The first_name.
     */
    FIRST_NAME("first_name"),

    /**
     * The last-name.
     */
    LAST_NAME("last-name"),

    /**
     * The user_photo.
     */
    USER_PHOTO("user_photo"),

    /**
     * The user_birthday
     */
    USER_BIRTHDAY("user_birthday"),

    /**
     * The last_activity.
     */
    USER_LAST_ACTIVITY("last_activity"),

    /**
     * The country.
     */
    USER_COUNTRY("country"),

    /**
     * The city.
     */
    USER_CITY("city"),

    /**
     * The age.
     */
    USER_AGE("age"),

    /**
     * The sex.
     */
    USER_SEX("sex"),

    /**
     * The info_text.
     */
    USER_TEXT("info_text"),

    /**
     * The date_registration.
     */
    USER_DATE_REGISTRATION("date_registration"),

    /**
     * The car_images.
     */
    CAR_IMAGES("car_images"),

    /**
     * The idcar_images.
     */
    ID_CAR_IMAGES("idcar_images"),

    /**
     * The id_car_info.
     */
    ID_CAR_INFO("id_car_info"),

    /**
     * The recipient.
     */
    RECIPIENT("recipient"),

    /**
     * The sender.
     */
    SENDER("sender"),

    /**
     * The idmessage.
     */
    ID_MESSAGE("idmessage"),

    /**
     * The text.
     */
    TEXT("text"),

    /**
     * The date_message.
     */
    DATE_MESSAGE("date_message"),

    /**
     * The status.
     */
    RESERVE_STATUS("status"),

    /**
     * The date_booking.
     */
    RESERVE_DATE_BOOKING("date_booking"),

    /**
     * The date_return.
     */
    RESERVE_DATE_RETURN("date_return"),

    /**
     * The user_id_user.
     */
    RESERVE_ID_USER("user_id_user"),

    /**
     * The car_id_auto.
     */
    RESERVE_ID_CAR("car_id_auto"),

    /**
     * The idreserve.
     */
    ID_RESERVE("idreserve"),

    /**
     * The url.
     */
    URL("url"),

    /**
     * The user_info_id.
     */
    ID_USER_INFO("user_info_id"),

    /**
     * The default_image_user.
     */
    DEFAULT_IMAGE_USER("default_image_user");


    /** The sql name. */
    private final String name;

    /**
     * Instantiates a new dao enum.
     *
     * @param name the sql name
     */
    DaoСolumnName(String name) {
        this.name = name;

    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

}
