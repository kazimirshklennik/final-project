package by.epam.rentcar.dao;

import by.epam.rentcar.exception.DaoException;

import javax.validation.constraints.NotNull;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * The interface Default image dao.
 */
public interface DefaultImageDao {

    /**
     * Add default image.
     *
     * @param fileInputStream the file input stream
     * @param image           the image
     * @throws DaoException the dao exception
     */
    void addDefaultImage(FileInputStream fileInputStream, InputStream image) throws DaoException;

    /**
     * Find default image input stream.
     *
     * @param id the id
     * @return the input stream
     * @throws DaoException the dao exception
     */
    InputStream findDefaultImage(@NotNull Long id) throws DaoException;
}
