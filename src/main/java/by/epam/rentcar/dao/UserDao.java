package by.epam.rentcar.dao;

import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.DaoException;

import javax.validation.constraints.NotNull;
import java.io.InputStream;

/**
 * The Interface UserDao.
 */
public interface UserDao {

    /**
     * find Email By Login.
     *
     * @param login the login
     * @return the String
     * @throws DaoException the dao exception
     */
    String findEmailByLogin(@NotNull String login) throws DaoException;

    /**
     * find id By Login.
     *
     * @param login the login
     * @return the Long
     * @throws DaoException the dao exception
     */
    Long findIdByLogin(@NotNull String login) throws DaoException;

    /**
     * update Photo User
     *
     * @param userId      the userId
     * @param inputStream the inputStream
     * @throws DaoException the dao exception
     */
    void updatePhotoUser(@NotNull Long userId, @NotNull InputStream inputStream) throws DaoException;

    /**
     * check Login.
     *
     * @param login the login
     * @return the boolean
     * @throws DaoException the dao exception
     */
    boolean checkLogin(@NotNull String login) throws DaoException;

    /**
     * check Email.
     *
     * @param email the email
     * @return the boolean
     * @throws DaoException the dao exception
     */
    boolean checkEmail(@NotNull String email) throws DaoException;

    /**
     * check Login and Pssword.
     *
     * @param login    the login
     * @param password the password
     * @return the boolean
     * @throws DaoException the dao exception
     */
    boolean checkLoginPssword(@NotNull String login, @NotNull String password) throws DaoException;

    /**
     * change Password
     *
     * @param login    the login
     * @param password the password
     * @throws DaoException the dao exception
     */
    void changePassword(@NotNull String login, @NotNull String password) throws DaoException;

    /**
     * find By Login
     *
     * @param login the login
     * @return the User
     * @throws DaoException the dao exception
     */
    User findByLogin(@NotNull String login) throws DaoException;

    /**
     * lock User
     *
     * @param idUser the idUser
     * @throws DaoException the dao exception
     */
    void lockUser(@NotNull Long idUser) throws DaoException;

    /**
     * unLock User
     *
     * @param idUser the idUser
     * @throws DaoException the dao exception
     */
    void unLockUser(@NotNull Long idUser) throws DaoException;

    /**
     * set Online
     *
     * @param idUser the idUser
     * @throws DaoException the dao exception
     */
    void setOnline(@NotNull Long idUser) throws DaoException;

    /**
     * set Offline
     *
     * @param idUser the idUser
     * @throws DaoException the dao exception
     */
    void setOffline(@NotNull Long idUser) throws DaoException;

    /**
     * find By Login Password
     *
     * @param login          the login
     * @param hashedPassword the hashedPassword
     * @return the User
     * @throws DaoException the dao exception
     */
    User findByLoginPassword(@NotNull String login, @NotNull String hashedPassword) throws DaoException;

    /**
     * find Image By Id User
     *
     * @param idUser the idUser
     * @return the InputStream
     * @throws DaoException the dao exception
     */
    InputStream findImageByIdUser(@NotNull Long idUser) throws DaoException;

    /**
     * add Last Activity
     *
     * @param idUser the idUser
     * @throws DaoException the dao exception
     */
    void addLastActivity(@NotNull Long idUser) throws DaoException;


}
