package by.epam.rentcar.dao;

import by.epam.rentcar.exception.DaoException;

import javax.validation.constraints.NotNull;

/**
 * The Interface ReserveCarDao.
 */
public interface ReserveCarDao {

    /**
     * check Active Reserve By Id User.
     *
     * @param idUser the idUser
     * @return the Long
     * @throws DaoException the dao exception
     */
    Long checkActiveReserveByIdUser(@NotNull Long idUser) throws DaoException;

    /**
     * set Status.
     *
     * @param idReserve the idReserve
     * @param status    the status
     * @throws DaoException the dao exception
     */
    void setStatus(@NotNull Long idReserve, boolean status) throws DaoException;
}
