package by.epam.rentcar.dao;

import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.DaoException;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.util.List;

/**
 * The Interface CarDao.
 */
public interface CarDao {

    /**
     * Add image by id.
     *
     * @param idCar       the id car
     * @param inputStream the input stream
     * @throws DaoException the dao exception
     */
    void addImageById(@NotNull Long idCar, @NotNull InputStream inputStream) throws DaoException;

    /**
     * Find all image by id list.
     *
     * @param idCar the id car
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Long> findAllImageById(@NotNull Long idCar) throws DaoException;

    /**
     * Find image by id input stream.
     *
     * @param idImage the id image
     * @return the input stream
     * @throws DaoException the dao exception
     */
    InputStream findImageById(@NotNull Long idImage) throws DaoException;

    /**
     * Find all image list.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Long> findAllImage() throws DaoException;

    /**
     * Lock car.
     *
     * @param idCar the id car
     * @throws DaoException the dao exception
     */
    void lockCar(@NotNull Long idCar) throws DaoException;

    /**
     * Un lock car.
     *
     * @param idCar the id car
     * @throws DaoException the dao exception
     */
    void unLockCar(@NotNull Long idCar) throws DaoException;

    /**
     * Delete image by id.
     *
     * @param idImage the id image
     * @throws DaoException the dao exception
     */
    void deleteImageById(@NotNull Long idImage) throws DaoException;

    /**
     * Find part cars list.
     *
     * @param limit  the limit
     * @param offset the offset
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Car> findPartCars(int limit, int offset) throws DaoException;


    /**
     * Check car boolean.
     *
     * @param idCar the id car
     * @return the boolean
     * @throws DaoException the dao exception
     */
    boolean checkCar(@NotNull Long idCar) throws DaoException;
}
