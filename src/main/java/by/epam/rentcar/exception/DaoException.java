package by.epam.rentcar.exception;

/**
 * The Class DaoException.
 */
public class DaoException extends Exception {

    /**
     * Instantiates a new Dao Exception.     *
     */
    public DaoException() {
        super();
    }

    /**
     * Instantiates a new Dao Exception.
     *
     * @param message the message
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Dao Exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Dao Exception.
     *
     * @param cause the cause
     */
    public DaoException(Throwable cause) {
        super(cause);
    }
}
