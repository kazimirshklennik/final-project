package by.epam.rentcar.exception;

/**
 * The Class LogicException.
 */
public class LogicException extends Exception {

    /**
     * Instantiates a new Logic Exception.     *
     */
    public LogicException() {
        super();
    }


    /**
     * Instantiates a new Logic exception.
     *
     * @param message the message
     */
    public LogicException(String message) {
        super(message);
    }


    /**
     * Instantiates a new Logic exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public LogicException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Logic Exception.
     *
     * @param cause the cause
     */
    public LogicException(Throwable cause) {
        super(cause);
    }
}
