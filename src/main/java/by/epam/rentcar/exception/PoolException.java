package by.epam.rentcar.exception;

/**
 * The Class PoolException.
 */
public class PoolException extends Exception {

    /**
     * Instantiates a new Pool Exception.     *
     */
    public PoolException() {
        super();
    }

    /**
     * Instantiates a new Pool Exception.
     *
     * @param message the message
     */
    public PoolException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Pool Exception.
     *
     * @param message the message
     * @param cause   the cause
     * @cause message the cause
     */
    public PoolException(String message, Throwable cause) {
        super(message, cause);
    }


    /**
     * Instantiates a new Pool Exception.
     *
     * @param cause the cause
     * @cause message the cause
     */
    public PoolException(Throwable cause) {
        super(cause);
    }
}
