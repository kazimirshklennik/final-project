package by.epam.rentcar.logic;

/**
 * The interface Service logic.
 */
public interface ServiceLogic {

    /**
     * Generete password string.
     *
     * @return the string
     */
    String generetePassword();

}
