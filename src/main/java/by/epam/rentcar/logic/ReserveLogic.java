package by.epam.rentcar.logic;

import by.epam.rentcar.entity.Reserve;
import by.epam.rentcar.exception.LogicException;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The interface Reserve logic.
 */
public interface ReserveLogic {
    /**
     * Find by id reserve.
     *
     * @param idReserve the id reserve
     * @return the reserve
     * @throws LogicException the logic exception
     */
    Reserve findById(@NotNull Long idReserve) throws LogicException;

    /**
     * Cancellation reserve.
     *
     * @param reserve the reserve
     * @throws LogicException the logic exception
     */
    void cancellationReserve(@NotNull Reserve reserve) throws LogicException;

    /**
     * Create reserve reserve.
     *
     * @param dateBooking the date booking
     * @param dateReturn  the date return
     * @param idCar       the id car
     * @param idUser      the id user
     * @return the reserve
     */
    Reserve createReserve(@NotNull String dateBooking, @NotNull String dateReturn, @NotNull Long idCar, @NotNull Long idUser);

    /**
     * Reserve car long.
     *
     * @param reserve the reserve
     * @return the long
     * @throws LogicException the logic exception
     */
    Long reserveCar(@NotNull Reserve reserve) throws LogicException;

    /**
     * Find all reserve by id user list.
     *
     * @param idUser the id user
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Reserve> findAllReserveByIdUser(@NotNull Long idUser) throws LogicException;

    /**
     * Check active reserve by id user boolean.
     *
     * @param idUser the id user
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean checkActiveReserveByIdUser(@NotNull Long idUser) throws LogicException;

    /**
     * Check date boolean.
     *
     * @param dateBooking the date booking
     * @param dateReturn  the date return
     * @return the boolean
     */
    boolean checkDate(@NotNull String dateBooking, @NotNull  String dateReturn);
}
