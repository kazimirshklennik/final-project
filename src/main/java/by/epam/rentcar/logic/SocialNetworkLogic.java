package by.epam.rentcar.logic;

import by.epam.rentcar.entity.social.SocialNetwork;
import by.epam.rentcar.exception.LogicException;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The interface Social network logic.
 */
public interface SocialNetworkLogic {

    /**
     * Add long.
     *
     * @param socialNetwork the social network
     * @return the long
     * @throws LogicException the logic exception
     */
    Long add(@NotNull SocialNetwork socialNetwork) throws LogicException;

    /**
     * Create social network.
     *
     * @param idUserInfo the id user info
     * @param url        the url
     * @return the social network
     */
    SocialNetwork create(Long idUserInfo, String url);

    /**
     * Find all social network list.
     *
     * @param idUser the id user
     * @return the list
     * @throws LogicException the logic exception
     */
    List<SocialNetwork> findAllSocialNetwork(@NotNull Long idUser) throws LogicException;

    /**
     * Delete social network boolean.
     *
     * @param idSocialNetwork the id social network
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean deleteSocialNetwork(Long idSocialNetwork) throws LogicException;
}
