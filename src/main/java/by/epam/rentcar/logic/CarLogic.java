package by.epam.rentcar.logic;

import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.LogicException;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.util.List;

/**
 * The interface Car logic.
 */
public interface CarLogic {
    /**
     * Add long.
     *
     * @param car the car
     * @return the long
     * @throws LogicException the logic exception
     */
    Long add(@NotNull Car car) throws LogicException;

    /**
     * Create car car.
     *
     * @param name  the name
     * @param model the model
     * @param info  the info
     * @return the car
     */
    Car createCar(@NotNull String name, @NotNull String model, String info);

    /**
     * Create car car.
     *
     * @param name  the name
     * @param model the model
     * @param info  the info
     * @param price the price
     * @return the car
     * @throws LogicException the logic exception
     */
    Car createCar(@NotNull String name, @NotNull String model, @NotNull String info, int price) throws LogicException;

    /**
     * Default car image input stream.
     *
     * @return the input stream
     * @throws LogicException the logic exception
     */
    InputStream defaultCarImage() throws LogicException;

    /**
     * Find all car list.
     *
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Car> findAllCar() throws LogicException;

    /**
     * Find car by id car.
     *
     * @param idCar the id car
     * @return the car
     * @throws LogicException the logic exception
     */
    Car findCarById(@NotNull Long idCar) throws LogicException;

    /**
     * Find all images car list.
     *
     * @param idCar the id car
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Long> findAllImagesCar(@NotNull Long idCar) throws LogicException;

    /**
     * Find image by id input stream.
     *
     * @param idImage the id image
     * @return the input stream
     * @throws LogicException the logic exception
     */
    InputStream findImageById(@NotNull Long idImage) throws LogicException;

    /**
     * Update car.
     *
     * @param car         the car
     * @param inputStream the input stream
     * @throws LogicException the logic exception
     */
    void updateCar(@NotNull Car car, InputStream inputStream) throws LogicException;

    /**
     * Delete car.
     *
     * @param idCar the id car
     * @throws LogicException the logic exception
     */
    void deleteCar(@NotNull Long idCar) throws LogicException;

    /**
     * Delete image car.
     *
     * @param idImage the id image
     * @throws LogicException the logic exception
     */
    void deleteImageCar(@NotNull Long idImage) throws LogicException;

    /**
     * Find part cars list.
     *
     * @param limit  the limit
     * @param offset the offset
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Car> findPartCars(int limit, int offset) throws LogicException;

    /**
     * Find all image list.
     *
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Long> findAllImage() throws LogicException;

    /**
     * Check car boolean.
     *
     * @param idCar the id car
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean checkCar(@NotNull Long idCar) throws LogicException;
}
