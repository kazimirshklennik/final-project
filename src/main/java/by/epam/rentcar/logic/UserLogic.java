package by.epam.rentcar.logic;

import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.util.List;

/**
 * The interface User logic.
 */
public interface UserLogic {

    /**
     * Add user boolean.
     *
     * @param login    the login
     * @param password the password
     * @param email    the email
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean addUser(@NotNull String login, @NotNull String password, @NotNull String email) throws LogicException;

    /**
     * Create user .
     *
     * @param country   the country
     * @param city      the city
     * @param sex       the sex
     * @param info      the info
     * @param bith      the bith
     * @param firstName the first name
     * @param lastName  the last name
     * @return the user
     * @throws LogicException the logic exception
     */
    User createUser(@NotNull String country, @NotNull String city, @NotNull String sex, @NotNull String info, @NotNull String bith, @NotNull String firstName, @NotNull String lastName) throws LogicException;

    /**
     * Find id user long.
     *
     * @param login the login
     * @return the long
     * @throws LogicException the logic exception
     */
    Long findIdUser(@NotNull String login) throws LogicException;

    /**
     * Find user user.
     *
     * @param login the login
     * @return the user
     * @throws LogicException the logic exception
     */
    User findUser(@NotNull String login) throws LogicException;

    /**
     * Update.
     *
     * @param user        the user
     * @param inputStream the input stream
     * @throws LogicException the logic exception
     */
    void update(@NotNull User user, InputStream inputStream) throws LogicException;

    /**
     * Find user image input stream.
     *
     * @param idUser the id user
     * @return the input stream
     * @throws LogicException the logic exception
     */
    InputStream findUserImage(@NotNull Long idUser) throws LogicException;

    /**
     * Delete.
     *
     * @param login the login
     * @throws LogicException the logic exception
     */
    void delete(@NotNull String login) throws LogicException;

    /**
     * Lock.
     *
     * @param idUser the id user
     * @throws LogicException the logic exception
     */
    void lock(@NotNull Long idUser) throws LogicException;

    /**
     * Un lock.
     *
     * @param idUser the id user
     * @throws LogicException the logic exception
     */
    void unLock(@NotNull Long idUser) throws LogicException;

    /**
     * Sets online.
     *
     * @param idUser the id user
     * @throws LogicException the logic exception
     */
    void setOnline(@NotNull Long idUser) throws LogicException;

    /**
     * Sets offline.
     *
     * @param idUser the id user
     * @throws LogicException the logic exception
     */
    void setOffline(@NotNull Long idUser) throws LogicException;

    /**
     * Find all user list.
     *
     * @return the list
     * @throws LogicException the logic exception
     */
    List<User> findAllUser() throws LogicException;

    /**
     * Find user user.
     *
     * @param login    the login
     * @param password the password
     * @return the user
     * @throws LogicException the logic exception
     */
    User findUser(@NotNull String login, @NotNull String password) throws LogicException;

    /**
     * Check login boolean.
     *
     * @param login the login
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean checkLogin(@NotNull String login) throws LogicException;

    /**
     * Check email boolean.
     *
     * @param email the email
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean checkEmail(@NotNull String email) throws LogicException;

    /**
     * Change password.
     *
     * @param login       the login
     * @param oldPassword the old password
     * @param newPassword the new password
     * @throws LogicException the logic exception
     */
    void changePassword(@NotNull String login, @NotNull String oldPassword, @NotNull String newPassword) throws LogicException;

    /**
     * Check login and password boolean.
     *
     * @param login    the login
     * @param password the password
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean checkLoginAndPassword(String login, String password) throws LogicException;

    /**
     * Default user image input stream.
     *
     * @return the input stream
     * @throws LogicException the logic exception
     */
    InputStream defaultUserImage() throws LogicException;
}
