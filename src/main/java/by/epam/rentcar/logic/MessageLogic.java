package by.epam.rentcar.logic;

import by.epam.rentcar.entity.Message;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The interface Message logic.
 */
public interface MessageLogic {
    /**
     * Find all list.
     *
     * @param idUser the id user
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Message> findAll(@NotNull Long idUser) throws LogicException;

    /**
     * Find chat list.
     *
     * @param idSender    the id sender
     * @param idRecipient the id recipient
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Message> findChat(@NotNull Long idSender, @NotNull Long idRecipient) throws LogicException;

    /**
     * Find all users chat list.
     *
     * @param idUser the id user
     * @return the list
     * @throws LogicException the logic exception
     */
    List<User> findAllUsersChat(@NotNull Long idUser) throws LogicException;

    /**
     * Send message long.
     *
     * @param message the message
     * @return the long
     * @throws LogicException the logic exception
     */
    Long sendMessage(@NotNull Message message) throws LogicException;

    /**
     * Create message message.
     *
     * @param sender    the sender
     * @param recipient the recipient
     * @param text      the text
     * @return the message
     * @throws LogicException the logic exception
     */
    Message createMessage(@NotNull String sender, @NotNull String recipient, @NotNull String text) throws LogicException;

    /**
     * Send email boolean.
     *
     * @param message the message
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean sendEmail(@NotNull Message message) throws LogicException;

    /**
     * Create message email message.
     *
     * @param subject        the subject
     * @param loginRecipient the login recipient
     * @param text           the text
     * @return the message
     * @throws LogicException the logic exception
     */
    Message createMessageEmail(@NotNull String subject, @NotNull String loginRecipient, @NotNull String text) throws LogicException;

    /**
     * Create message authorization message.
     *
     * @param subject        the subject
     * @param emailRecipient the email recipient
     * @param text           the text
     * @return the message
     */
    Message createMessageAuthorization(@NotNull String subject, @NotNull String emailRecipient, @NotNull String text);

    /**
     * Send new password.
     *
     * @param login       the login
     * @param textmessage the textmessage
     * @throws LogicException the logic exception
     */
    void sendNewPassword(@NotNull String login, @NotNull String textmessage) throws LogicException;

    /**
     * Delete message.
     *
     * @param idMessage the id message
     * @throws LogicException the logic exception
     */
    void deleteMessage(@NotNull Long idMessage) throws LogicException;
}
