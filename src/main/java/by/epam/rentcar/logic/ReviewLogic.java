package by.epam.rentcar.logic;

import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.LogicException;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The interface Review logic.
 */
public interface ReviewLogic {
    /**
     * Add review long.
     *
     * @param review the review
     * @return the long
     * @throws LogicException the logic exception
     */
    Long addReview(@NotNull Review review) throws LogicException;

    /**
     * Create review.
     *
     * @param idUser the id user
     * @param idCar  the id car
     * @param text   the text
     * @return the review
     * @throws LogicException the logic exception
     */
    Review createReview(@NotNull Long idUser, @NotNull Long idCar, String text) throws LogicException;

    /**
     * Find all review by car id list.
     *
     * @param idCar the id car
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Review> findAllReviewByIdCar(@NotNull Long idCar) throws LogicException;

    /**
     * Find all reviews list.
     *
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Review> findAllReviews() throws LogicException;

    /**
     * Find part review list.
     *
     * @param limit  the limit
     * @param offset the offset
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Review> findPartReview(int limit, int offset) throws LogicException;

    /**
     * Find part review by id car list.
     *
     * @param limit  the limit
     * @param offset the offset
     * @param idCar  the id car
     * @return the list
     * @throws LogicException the logic exception
     */
    List<Review> findPartReviewByIdCar(int limit, int offset,@NotNull Long idCar) throws LogicException;

    /**
     * Delete review boolean.
     *
     * @param idReview the id review
     * @return the boolean
     * @throws LogicException the logic exception
     */
    boolean deleteReview(@NotNull Long idReview) throws LogicException;
}
