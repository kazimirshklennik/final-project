package by.epam.rentcar.logic.impl;

import by.epam.rentcar.controller.ImageController;
import by.epam.rentcar.dao.CarDao;
import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.DefaultImageDao;
import by.epam.rentcar.dao.impl.DefaultImageDaoImpl;
import by.epam.rentcar.dao.impl.car.CarDaoImpl;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Car logic.
 */
public class CarLogicImpl implements CarLogic {


    /** The Constant LOGGER. */
    private static final Logger LOGGER = LogManager.getLogger(CarLogicImpl.class);

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#add(Car)
     */
    @Override
    public Long add(@NotNull Car car) throws LogicException {
        CarRentDao<Car> carCarRentDao = CarDaoImpl.getInstance();
        Long idCar = 0l;
        try {
            if (car != null) {
                idCar = carCarRentDao.add(car);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method addCar(Car car)", e);
        }
        return idCar;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#createCar(String, String, String)
     */
    @Override
    public Car createCar(@NotNull String name, @NotNull String model, String info) {
        Car car = new Car();
        if (name != null && model != null) {
            car = new Car(name, model);
        }
        if (info != null) {
            car.setInfo(info);
        }
        return car;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#createCar(String, String, String, int)
     */
    @Override
    public Car createCar(@NotNull String name, @NotNull String model, @NotNull String info, int price) throws LogicException {
        Car car = new Car();
        if (name != null && model != null && info != null && price >= 0) {
            car.setName(name);
            car.setModel(model);
            car.setInfo(info);
            car.setPrice(price);
        }
        return car;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarLogic#findAllCar()
     */
    @Override
    public List<Car> findAllCar() throws LogicException {
        CarRentDao<Car> carRentDao = CarDaoImpl.getInstance();
        List<Car> cars;
        try {
            cars = carRentDao.findAll();
        } catch (DaoException e) {
            throw new LogicException("DaoException in method  findAllCar()", e);
        }
        return cars;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#findPartCars(int, int)
     */
    @Override
    public List<Car> findPartCars(int limit, int offset) throws LogicException {
        List<Car> cars = new ArrayList<>();
        CarDao carDao = CarDaoImpl.getInstance();

        try {
            if (limit > 0 && offset >= 0) {
                cars = carDao.findPartCars(limit, offset);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findPartCars(int limit, int offset)", e);
        }
        return cars;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#findCarById(Long)
     */
    @Override
    public Car findCarById(@NotNull Long idCar) throws LogicException {

        Car car = new Car();
        CarRentDao<Car> carRentDao = CarDaoImpl.getInstance();
        try {
            if (idCar != null) {
                car = carRentDao.findById(idCar);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findCarById(@NotNull Long idCar)");
        }
        return car;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#findAllImagesCar(Long)
     */
    @Override
    public List<Long> findAllImagesCar(@NotNull Long idCar) throws LogicException {
        List<Long> images = null;
        CarDao carDao = CarDaoImpl.getInstance();
        try {
            if (idCar != null) {
                images = carDao.findAllImageById(idCar);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in methodfindAllImagesCar(@NotNull Long idCar)", e);
        }
        return images;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#findImageById(Long)
     */
    @Override
    public InputStream findImageById(@NotNull Long idImage) throws LogicException {
        InputStream image = null;
        CarDao carDao = CarDaoImpl.getInstance();
        try {
            if (idImage != null) {
                image = carDao.findImageById(idImage);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findImageByIdUser(@NotNull Long idImage)", e);
        }
        return image;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#updateCar(Car, InputStream)
     */
    @Override
    public void updateCar(@NotNull Car car, InputStream inputStream) throws LogicException {
        CarRentDao<Car> carCarRentDao = CarDaoImpl.getInstance();

        try {
            if (car != null) {
                if (inputStream != null) {
                    addImage(car.getIdCar(), inputStream);
                }
                carCarRentDao.update(car.getIdCar(), car);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method updateCar(@NotNull Car car, InputStream inputStream)", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    LOGGER.warn("IOException in a updateCar() method, close inputStream", e);
                }
            }

        }
    }

    /**
     * Add image.
     *
     * @param idCar       the id car
     * @param inputStream the input stream
     * @throws LogicException the logic exception
     */
    private void addImage(@NotNull Long idCar, @NotNull InputStream inputStream) throws LogicException {
        try {
            CarDao carDao = CarDaoImpl.getInstance();
            carDao.addImageById(idCar, inputStream);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method addImage(@NotNull Long idCar, @NotNull InputStream inputStream)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#deleteImageCar(Long)
     */
    @Override
    public void deleteImageCar(@NotNull Long idImage) throws LogicException {
        CarDao carDao = CarDaoImpl.getInstance();
        try {
            if (idImage != null) {
                carDao.deleteImageById(idImage);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method deleteImageCar(@NotNull Long idImage)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#deleteCar(Long)
     */
    @Override
    public void deleteCar(@NotNull Long idCar) throws LogicException {
        CarRentDao<Car> carRentDao = CarDaoImpl.getInstance();
        try {
            if (idCar != null) {
                carRentDao.delete(idCar);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method deleteCar(Long idCar)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see CarLogic#findAllImage()
     */
    @Override
    public List<Long> findAllImage() throws LogicException {

        CarDao carDao = CarDaoImpl.getInstance();
        List<Long> allImages;
        try {
            allImages = carDao.findAllImage();
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findAllImage()", e);
        }
        return allImages;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.CarLogic#checkCar(Long)
     */
    @Override
    public boolean checkCar(@NotNull Long idCar) throws LogicException {
        CarDao carDao = CarDaoImpl.getInstance();
        boolean result = false;
        if (idCar != null) {
            try {
                result = carDao.checkCar(idCar);
            } catch (DaoException e) {
                throw new LogicException("DaoException in method checkCar(@NotNull Long idCar)", e);
            }
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see CarLogic#defaultCarImage()
     */
    @Override
    public InputStream defaultCarImage() throws LogicException {
        InputStream inputStream;
        DefaultImageDao defaultImageDao = DefaultImageDaoImpl.getInstance();

        try {
            inputStream = defaultImageDao.findDefaultImage(2l);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method defaultCarImage", e);
        }
        return inputStream;
    }
}
