package by.epam.rentcar.logic.impl;

import by.epam.rentcar.dao.CarDao;
import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.DefaultImageDao;
import by.epam.rentcar.dao.UserDao;
import by.epam.rentcar.dao.impl.DefaultImageDaoImpl;
import by.epam.rentcar.dao.impl.car.CarDaoImpl;
import by.epam.rentcar.dao.impl.reserve.ReserveCarDaoImpl;
import by.epam.rentcar.dao.impl.user.UserDaoImpl;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.PasswordEncryption;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * The type User logic.
 */
public class UserLogicImpl implements UserLogic {

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#addUser(String, String, String)
     */
    @Override
    public boolean addUser(@NotNull String login, @NotNull String password, @NotNull String email) throws LogicException {
        boolean result = false;
        if (login != null)
            if (password != null) {
                try {
                    User user = new User(login, password, email);
                    CarRentDao<User> userDao = UserDaoImpl.getInstance();
                    userDao.add(user);
                    result = true;
                } catch (DaoException e) {
                    throw new LogicException("DaoException in method addUser(String login, String password, String email))", e);
                }
            }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#addUser(String, String, String)
     */
    @Override
    public User createUser(@NotNull String country, @NotNull String city, @NotNull String sex, @NotNull String info, @NotNull String bith, @NotNull String firstName, @NotNull String lastName) throws LogicException {
        DateCreator dateCreator = new DateCreator();
        User user = new User();
        user.getInfo().setFirstName(firstName);
        user.getInfo().setLastName(lastName);
        user.getInfo().setBirthday(bith);
        user.getInfo().setUserText(info);
        user.getInfo().setSex(sex);
        user.getInfo().setCity(city);
        user.getInfo().setCountry(country);
        user.getInfo().setAge(dateCreator.getAge(bith));
        return user;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#findIdUser(String)
     */
    @Override
    public Long findIdUser(String login) throws LogicException {
        Long idUser;
        UserDao userDao = UserDaoImpl.getInstance();
        try {
            idUser = userDao.findIdByLogin(login);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findIdUser(String login)", e);
        }
        return idUser;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#findUser(String)
     */
    @Override
    public User findUser(@NotNull String login) throws LogicException {
        UserDao userDao = UserDaoImpl.getInstance();
        SocialNetworkLogicImpl socialNetworkLogic = new SocialNetworkLogicImpl();
        User user = new User();
        try {
            if (login != null) {
                user = userDao.findByLogin(login);
                user.getInfo().setSocialNetworks(socialNetworkLogic.findAllSocialNetwork(user.getIdUser()));
            }

        } catch (DaoException e) {
            throw new LogicException("DaoException in method findUser(@NotNull String login)", e);
        }
        return user;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#update(User, InputStream)
     */
    @Override
    public void update(@NotNull User user, InputStream inputStream) throws LogicException {
        CarRentDao<User> carRentDao = UserDaoImpl.getInstance();
        UserDao userDao = UserDaoImpl.getInstance();
        try {
            if (inputStream != null) {
                userDao.updatePhotoUser(user.getIdUser(), inputStream);

            }
            carRentDao.update(user.getIdUser(), user);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method update(String login, User user)", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw new LogicException("IOException in method update(String login, User user)", e);
                }
            }
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#findUserImage(Long)
     */
    @Override
    public InputStream findUserImage(@NotNull Long idUser) throws LogicException {
        UserDao userDao = UserDaoImpl.getInstance();

        InputStream inputStream;
        try {
            inputStream = userDao.findImageByIdUser(idUser);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findUserImage(Long idUser)", e);
        }
        return inputStream;

    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#delete(String)
     */
    @Override
    public void delete(@NotNull String login) throws LogicException {

        UserDao userDao = UserDaoImpl.getInstance();
        CarRentDao<User> carRentDao = UserDaoImpl.getInstance();

        CarDao carDao = CarDaoImpl.getInstance();
        ReserveCarDaoImpl reserveCarDao = ReserveCarDaoImpl.getInstance();

        try {
            Long idUser = userDao.findIdByLogin(login);
            Long idCar = reserveCarDao.checkActiveReserveByIdUser(idUser);
            carRentDao.delete(idUser);
            if (idCar != null) {
                carDao.unLockCar(idCar);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method delete(@NotNull String login)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#lock(Long)
     */
    @Override
    public void lock(@NotNull Long idUser) throws LogicException {
        UserDao userDao = UserDaoImpl.getInstance();
        try {
            if (idUser != null) {
                userDao.lockUser(idUser);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method lock(@NotNull String login)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#unLock(Long)
     */
    @Override
    public void unLock(@NotNull Long idUser) throws LogicException {
        UserDao userDao = UserDaoImpl.getInstance();
        try {
            if (idUser != null) {
                userDao.unLockUser(idUser);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method unLock(@NotNull String login)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#setOnline(Long)
     */
    @Override
    public void setOnline(@NotNull Long idUser) throws LogicException {
        UserDao userDao = UserDaoImpl.getInstance();
        try {
            if (idUser != null) {
                userDao.setOnline(idUser);
                userDao.addLastActivity(idUser);
            } else {
                throw new LogicException("DaoException in method setOnline(@NotNull Long idUser) null param");
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method setOnline(@NotNull Long idUser)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#setOnline(Long)
     */
    @Override
    public void setOffline(@NotNull Long idUser) throws LogicException {
        UserDao userDao = UserDaoImpl.getInstance();

        try {
            userDao.setOffline(idUser);
            userDao.addLastActivity(idUser);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method setOffline(@NotNull Long idUser)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see UserLogic#findAllUser()
     */
    @Override
    public List<User> findAllUser() throws LogicException {
        CarRentDao<User> carRentDao = UserDaoImpl.getInstance();
        List<User> list;
        try {
            list = carRentDao.findAll();
        } catch (DaoException e) {
            throw new LogicException("DaoException in method  UserLogic", e);

        }
        return list;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#findUser(String)
     */
    @Override
    public User findUser(@NotNull String login, @NotNull String password) throws LogicException {
        User user = null;
        if ((!login.isEmpty() && login != null))
            if (!password.isEmpty() && password != null) {
                try {
                    UserDao userDao = UserDaoImpl.getInstance();
                    user = userDao.findByLoginPassword(login, password);

                } catch (DaoException e) {
                    throw new LogicException("DaoException in method findUser(String login, String password)", e);

                }
            }
        return user;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#checkLogin(String) ch
     */
    @Override
    public boolean checkLogin(@NotNull String login) throws LogicException {
        boolean result = false;
        if ((!login.isEmpty() && login != null)) {
            try {
                if (!login.isEmpty() && login != null) {
                    UserDao userDao = UserDaoImpl.getInstance();
                    result = userDao.checkLogin(login);
                }
            } catch (DaoException e) {
                throw new LogicException("DaoException in method checkLogin(String login)", e);
            }
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#checkEmail(String)
     */
    @Override
    public boolean checkEmail(@NotNull String email) throws LogicException {
        boolean result = false;
        try {
            if (!email.isEmpty() && email != null) {
                UserDao userDao = UserDaoImpl.getInstance();
                result = userDao.checkEmail(email);
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method checkEmail(String email)", e);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#changePassword(String, String, String)
     */
    @Override
    public void changePassword(@NotNull String login, @NotNull String oldPassword, @NotNull String newPassword) throws LogicException {

        UserDao userDao = UserDaoImpl.getInstance();
        try {
            PasswordEncryption passwordEncryption = new PasswordEncryption();

            if (userDao.checkLoginPssword(login, passwordEncryption.create(oldPassword))) {
                userDao.changePassword(login, passwordEncryption.create(newPassword));
            } else {
                throw new DaoException("DaoException in method changePassword(String login, String oldPassword, String newPassword)");
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method changePassword(String login, String oldPassword, String newPassword)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.UserLogic#checkLoginAndPassword(String, String)
     */
    @Override
    public boolean checkLoginAndPassword(@NotNull String login, @NotNull String password) throws LogicException {

        boolean result = false;
        if (login != null)
            if (password != null) {
                try {
                    UserDao userDao = UserDaoImpl.getInstance();

                    if (userDao.checkLogin(login)) {
                        if (userDao.checkLoginPssword(login, password)) {
                            result = true;
                        }
                    }
                } catch (DaoException e) {
                    throw new LogicException("DaoException in method checkLoginAndPassword(String login, String password)", e);
                }
            }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see UserLogic#defaultUserImage()
     */
    @Override
    public InputStream defaultUserImage() throws LogicException {
        InputStream inputStream;
        DefaultImageDao defaultImageDao = DefaultImageDaoImpl.getInstance();
        try {
            inputStream = defaultImageDao.findDefaultImage(1l);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method defaultUserImage()", e);
        }
        return inputStream;

    }
}
