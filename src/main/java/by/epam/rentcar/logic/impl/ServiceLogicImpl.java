package by.epam.rentcar.logic.impl;

import by.epam.rentcar.entity.Review;
import by.epam.rentcar.logic.ServiceLogic;
import by.epam.rentcar.util.PasswordGenerator;

/**
 * The type Service logic.
 */
public class ServiceLogicImpl implements ServiceLogic {

    /**
     * (non-Javadoc)
     *
     * @see ServiceLogic#generetePassword()
     */
    @Override
    public String generetePassword() {
        final int LENGTH_PASSWORD = 6;
        return new PasswordGenerator().generate(LENGTH_PASSWORD);
    }







}
