package by.epam.rentcar.logic.impl;

import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.ReviewDao;
import by.epam.rentcar.dao.impl.car.CarDaoImpl;
import by.epam.rentcar.dao.impl.review.ReviewDaoImpl;
import by.epam.rentcar.dao.impl.user.UserDaoImpl;
import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.ReviewLogic;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The type Review logic.
 */
public class ReviewLogicImpl implements ReviewLogic {

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReviewLogic#addReview(Review)
     */
    @Override
    public Long addReview(@NotNull Review review) throws LogicException {
        CarRentDao<Review> carRentDao = ReviewDaoImpl.getInstance();
        Long idReview = 0L;
        if (review != null) {
            try {
                idReview = carRentDao.add(review);
            } catch (DaoException e) {
                throw new LogicException("DaoException in method  addReview(@NotNull Review review)", e);
            }
        }
        return idReview;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReviewLogic#deleteReview(Long)
     */
    @Override
    public boolean deleteReview(@NotNull Long idReview) throws LogicException {
        CarRentDao<Review> carRentDao = ReviewDaoImpl.getInstance();
        boolean result = false;
        try {
            if (idReview != null) {
                carRentDao.delete(idReview);
                result = true;
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method  addReview(@NotNull Review review)", e);
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReviewLogic#createReview(Long, Long, String)
     */
    @Override
    public Review createReview(@NotNull Long idUser, @NotNull Long idCar, String text) throws LogicException {
        Review review = new Review();
        DateCreator dateCreator = new DateCreator();
        review.setIdUser(idUser);
        review.setDate(dateCreator.create(System.currentTimeMillis(), DateType.DATE_TIME));
        review.setIdCar(idCar);
        review.setReview(text);
        return review;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReviewLogic#findAllReviewByIdCar(Long)
     */
    @Override
    public List<Review> findAllReviewByIdCar(@NotNull Long idCar) throws LogicException {

        List<Review> reviews;
        ReviewDao reviewDao = ReviewDaoImpl.getInstance();
        try {
            reviews = reviewDao.findAllByIdCar(idCar);

            for (Review review : reviews) {
                review.setUser(UserDaoImpl.getInstance().findById(review.getIdUser()));
                review.setCar(CarDaoImpl.getInstance().findById(review.getIdCar()));
            }

        } catch (DaoException e) {
            throw new LogicException("DaoException in method findAllReviewByIdCar(Long idCar)", e);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see ReviewLogic#findAllReviews()
     */
    @Override
    public List<Review> findAllReviews() throws LogicException {
        List<Review> reviews;
        CarRentDao<Review> reviewDao = ReviewDaoImpl.getInstance();
        try {
            reviews = reviewDao.findAll();

            for (Review review : reviews) {
                review.setUser(UserDaoImpl.getInstance().findById(review.getIdUser()));
                review.setCar(CarDaoImpl.getInstance().findById(review.getIdCar()));
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method indAllReviews()", e);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReviewLogic#findPartReview(int, int)
     */
    @Override
    public List<Review> findPartReview(int limit, int offset) throws LogicException {
        List<Review> reviews;
        ReviewDao reviewDao = ReviewDaoImpl.getInstance();
        try {
            reviews = reviewDao.findPartReview(limit, offset);
            for (Review review : reviews) {
                review.setUser(UserDaoImpl.getInstance().findById(review.getIdUser()));
                review.setCar(CarDaoImpl.getInstance().findById(review.getIdCar()));
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findPartReview(int limit, int offset)", e);
        }
        return reviews;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReviewLogic#findPartReviewByIdCar(int, int, Long)
     */
    @Override
    public List<Review> findPartReviewByIdCar(int limit, int offset, @NotNull Long idCar) throws LogicException {
        List<Review> reviews;
        ReviewDao reviewDao = ReviewDaoImpl.getInstance();
        try {
            reviews = reviewDao.findPartReviewByIdCar(limit, offset, idCar);
            for (Review review : reviews) {
                review.setUser(UserDaoImpl.getInstance().findById(review.getIdUser()));
                review.setCar(CarDaoImpl.getInstance().findById(review.getIdCar()));
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findPartReviewByIdCar(int limit, int offset,@NotNull Long idCar)", e);
        }
        return reviews;
    }
}
