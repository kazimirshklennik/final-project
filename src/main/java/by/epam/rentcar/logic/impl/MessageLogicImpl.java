package by.epam.rentcar.logic.impl;

import by.epam.rentcar.command.constant.MessageData;
import by.epam.rentcar.comparator.MessageComparator;
import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.MessageDao;
import by.epam.rentcar.dao.UserDao;
import by.epam.rentcar.dao.impl.message.MessageDaoImpl;
import by.epam.rentcar.dao.impl.user.UserDaoImpl;
import by.epam.rentcar.entity.Message;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.manager.MailData;
import by.epam.rentcar.manager.MailManager;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;
import by.epam.rentcar.util.SenderEmail;

import javax.mail.MessagingException;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Message logic.
 */
public class MessageLogicImpl implements MessageLogic {

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#findAll(Long)
     */
    @Override
    public List<Message> findAll(@NotNull Long idUser) throws LogicException {
        List<Message> allMessages = new ArrayList<>();
        CarRentDao<Message> messageDao = MessageDaoImpl.getInstance();
        CarRentDao<User> userDao = UserDaoImpl.getInstance();
        if (idUser != null) {
            try {
                allMessages = messageDao.findAll(idUser);
                for (Message message : allMessages) {
                    message.setUserSender(userDao.findById(message.getIdSender()));
                    message.setUserRecipient(userDao.findById(message.getIdRecipient()));
                }
            } catch (DaoException e) {
                throw new LogicException("DaoException in method List<Message> findAll(Long idUser)", e);
            }
        }
        return allMessages;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#findChat(Long, Long)
     */
    @Override
    public List<Message> findChat(@NotNull Long idSender, @NotNull Long idRecipient) throws LogicException {

        List<Message> chat = new ArrayList<>();
        MessageDao messageDao = MessageDaoImpl.getInstance();
        CarRentDao<User> userDao = UserDaoImpl.getInstance();
        if (idRecipient != null && idSender != null) {
            try {
                chat = messageDao.findChat(idSender, idRecipient);
                for (Message message : chat) {
                    message.setUserSender(userDao.findById(message.getIdSender()));
                    message.setUserRecipient(userDao.findById(message.getIdRecipient()));
                    message.setDateString(new DateCreator().create(message.getDate(), DateType.DATE_TIME));
                }
                chat.sort(new MessageComparator());
            } catch (DaoException e) {
                throw new LogicException("DaoException in method List<Message> findChat(Long idSender, Long idRecipient)", e);
            }

        }
        return chat;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#findAllUsersChat(Long)
     */
    @Override
    public List<User> findAllUsersChat(@NotNull Long idUser) throws LogicException {
        List<Long> idUsers;
        List<User> users = new ArrayList<>();
        UserDaoImpl userDao = UserDaoImpl.getInstance();
        MessageDao messageDao = MessageDaoImpl.getInstance();
        if (idUser != null) {
            try {
                idUsers = messageDao.findAllUserChat(idUser);
                for (Long id : idUsers) {
                    User user = userDao.findById(id);
                    if (!user.isLockStatus()) {
                        users.add(user);
                    }
                }
            } catch (DaoException e) {
                throw new LogicException("DaoException in method List<User> findAllUsersChat(Long idUser)", e);
            }
        }
        return users;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#sendMessage(Message)
     */
    @Override
    public Long sendMessage(@NotNull Message message) throws LogicException {
        Long idMessage = 0L;
        if (message != null) {
            CarRentDao<Message> msg = MessageDaoImpl.getInstance();
            try {
                idMessage = msg.add(message);
            } catch (DaoException e) {
                throw new LogicException("DaoException in method sendMessage(Message message)", e);
            }
        }
        return idMessage;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#deleteMessage(Long) )
     */
    @Override
    public void deleteMessage(@NotNull Long idMessage) throws LogicException {
        if (idMessage != null) {
            CarRentDao<Message> msg = MessageDaoImpl.getInstance();
            try {
                msg.delete(idMessage);
            } catch (DaoException e) {
                throw new LogicException("DaoException in method sendMessage(Message message)", e);
            }
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#createMessage(String, String, String)
     */
    @Override
    public Message createMessage(@NotNull String sender, @NotNull String recipient, @NotNull String text) throws LogicException {
        Long idSender;
        Long idRecipient;
        Message message = new Message();

        if (!sender.equals(recipient)) {
            try {
                UserDao userDao = UserDaoImpl.getInstance();
                idSender = userDao.findIdByLogin(sender);
                idRecipient = userDao.findIdByLogin(recipient);
            } catch (DaoException e) {
                throw new LogicException("DaoException in method createMessage(String sender, String recipient, String text)", e);
            }
            if (idSender != -1 && idRecipient != -1) {
                if (!idSender.equals(idRecipient)) {
                    message = new Message(idSender, idRecipient, text);
                }
            }
        }
        return message;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#sendEmail(Message)
     */
    @Override
    public boolean sendEmail(@NotNull Message message) throws LogicException {
        boolean result = false;
        MailManager mailManager = new MailManager();
        if (message != null) {
            try {
                SenderEmail sender = new SenderEmail(mailManager.getData(MailData.KEY_LOGIN.getKey()),
                        mailManager.getData(MailData.KEY_PASSWORD.getKey()));
                sender.send(message.getSubject(), message.getText(), message.getEmailRecipient());
                result = true;
            } catch (MessagingException e) {
                throw new LogicException("MessagingException in method sendEmail(Message message)", e);
            }
        }
        return result;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#createMessageEmail(String, String, String)
     */
    @Override
    public Message createMessageEmail(@NotNull String subject, @NotNull String loginRecipient, @NotNull String text) throws LogicException {
        String mailRecipient;
        Message message = new Message();
        try {
            UserDao userDao = UserDaoImpl.getInstance();
            mailRecipient = userDao.findEmailByLogin(loginRecipient);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method createMessageEmail(String loginSender, String loginRecipient, String text)", e);
        }
        if (mailRecipient != null) {
            message = new Message(subject, mailRecipient, text);
        }
        return message;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#createMessageAuthorization(String, String, String)
     */
    @Override
    public Message createMessageAuthorization(@NotNull String subject, @NotNull String emailRecipient, @NotNull String text) {
        Message message = new Message();
        message.setEmailRecipient(emailRecipient);
        message.setSubject(subject);
        message.setText(text);
        return message;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.MessageLogic#sendNewPassword(String, String)
     */
    @Override
    public void sendNewPassword(@NotNull String login, @NotNull String textmessage) throws LogicException {
        String subject = MessageData.NEW_PASSWORD_TEXT;
        Message message = createMessageEmail(subject, login, textmessage);
        sendEmail(message);
    }
}

