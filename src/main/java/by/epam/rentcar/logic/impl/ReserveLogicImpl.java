package by.epam.rentcar.logic.impl;

import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.impl.car.CarDaoImpl;
import by.epam.rentcar.dao.impl.reserve.ReserveCarDaoImpl;
import by.epam.rentcar.entity.Reserve;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.ReserveLogic;
import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * The type Reserve logic.
 */
public class ReserveLogicImpl implements ReserveLogic {

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#findById(Long)
     */
    @Override
    public Reserve findById(@NotNull Long idReserve) throws LogicException {
        Reserve reserve;
        CarRentDao<Reserve> carRentDao = ReserveCarDaoImpl.getInstance();
        try {
            reserve = carRentDao.findById(idReserve);
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findById(@NotNull Long idReserve)", e);
        }
        return reserve;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#cancellationReserve(Reserve)
     */
    @Override
    public void cancellationReserve(@NotNull Reserve reserve) throws LogicException {
        ReserveCarDaoImpl reserveDao = ReserveCarDaoImpl.getInstance();
        try {
            reserveDao.setStatus(reserve.getIdReserve(), true);
            CarDaoImpl.getInstance().unLockCar(reserve.getIdCar());
        } catch (DaoException e) {
            throw new LogicException("DaoException in method cancellationReserve(Long idReserve, boolean  status)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#createReserve(String, String, Long, Long)
     */
    @Override
    public Reserve createReserve(@NotNull String dateBooking, @NotNull String dateReturn, @NotNull Long idCar, @NotNull Long idUser) {
        return new Reserve(dateBooking, dateReturn, idCar, idUser);
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#reserveCar(Reserve)
     */
    @Override
    public Long reserveCar(@NotNull Reserve reserve) throws LogicException {
        ReserveCarDaoImpl reserveCarDao = ReserveCarDaoImpl.getInstance();
        Long idReserve = 0L;
        if (reserve != null) {
            try {
                reserve.setStatus(false);
                idReserve=reserveCarDao.add(reserve);
                CarDaoImpl.getInstance().lockCar(reserve.getIdCar());
            } catch (DaoException e) {
                throw new LogicException("DaoException in method reserve(Reserve reserve)", e);
            }
        }
        return idReserve;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#findAllReserveByIdUser(Long)
     */
    @Override
    public List<Reserve> findAllReserveByIdUser(@NotNull Long idUser) throws LogicException {
        CarRentDao<Reserve> carRentDao = ReserveCarDaoImpl.getInstance();
        DateCreator dateCreator = new DateCreator();
        List<Reserve> result = null;
        if (idUser != null) {
            try {
                result = carRentDao.findAll(idUser);
                for (Reserve reserve : result) {
                    updatekStatusReserve(reserve);
                    reserve.setCar(CarDaoImpl.getInstance().findById(reserve.getIdCar()));
                    int days = dateCreator.getDay(reserve.getDateBooking(),reserve.getDateReturn());
                    reserve.setTotalCost(days * reserve.getCar().getPrice());
                }
            } catch (DaoException e) {
                throw new LogicException("DaoException in method findAllReserveByIdUser(Long idUser)", e);
            }
        }
        return result;
    }


    /**
     * Updatek status reserve.
     *
     * @param reserve the reserve
     * @throws LogicException the logic exception
     */
    private void updatekStatusReserve(@NotNull Reserve reserve) throws LogicException {
        ReserveCarDaoImpl reserveDao = ReserveCarDaoImpl.getInstance();
        DateCreator dateCreator = new DateCreator();
        String currentDate=dateCreator.create(System.currentTimeMillis(),DateType.DATE);
        try {
            if (!reserve.isStatus()) {
                if (dateCreator.createLong(reserve.getDateReturn()) < dateCreator.createLong(currentDate)) {
                    reserveDao.setStatus(reserve.getIdReserve(), true);
                    CarDaoImpl.getInstance().unLockCar(reserve.getIdCar());
                } else {
                    reserveDao.setStatus(reserve.getIdReserve(), false);
                }
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method updatekStatusReserve(Reserve reserve)", e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#checkActiveReserveByIdUser(Long)
     */
    @Override
    public boolean checkActiveReserveByIdUser(@NotNull Long idUser) throws LogicException {
        ReserveCarDaoImpl reserveDao = ReserveCarDaoImpl.getInstance();
        boolean resuilt = false;
        try {
            if (reserveDao.checkActiveReserveByIdUser(idUser) != null) {
                resuilt = true;
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method checkActiveReserveByIdUser(@NotNull Long idUser)", e);
        }
        return resuilt;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.ReserveLogic#checkDate(String, String)
     */
    @Override
    public boolean checkDate(@NotNull String dateBooking, @NotNull String dateReturn) {
        boolean result = false;
        DateCreator dateCreator = new DateCreator();
        Long booking = dateCreator.createLong(dateBooking);
        Long dReturn = dateCreator.createLong(dateReturn);
        Long currentDate = dateCreator.createLong(dateCreator.create(System.currentTimeMillis(), DateType.DATE));

        if (dReturn > booking) {
            if (currentDate <= booking)
                result = true;
        }
        return result;
    }
}
