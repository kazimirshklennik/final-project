package by.epam.rentcar.logic.impl;

import by.epam.rentcar.dao.CarRentDao;
import by.epam.rentcar.dao.impl.social.SocialNetworkImpl;
import by.epam.rentcar.entity.social.SocialNetwork;
import by.epam.rentcar.exception.DaoException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.SocialNetworkLogic;
import by.epam.rentcar.util.UrlParser;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Social network logic.
 */
public class SocialNetworkLogicImpl implements SocialNetworkLogic {

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.SocialNetworkLogic#add(SocialNetwork)
     */
    @Override
    public Long add(@NotNull SocialNetwork socialNetwork) throws LogicException {
        CarRentDao<SocialNetwork> carRentDao = SocialNetworkImpl.getInstance();
        Long idSocialNetwork = 0L;
        if (socialNetwork != null) {
            try {
                idSocialNetwork = carRentDao.add(socialNetwork);
            } catch (DaoException e) {
                throw new LogicException("DaoException in method add(SocialNetwork socialNetwork)", e);
            }
        }
        return idSocialNetwork;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.SocialNetworkLogic#create(Long, String)
     */
    @Override
    public SocialNetwork create(Long idUserInfo, String url) {
        return new SocialNetwork(idUserInfo, url);
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.SocialNetworkLogic#findAllSocialNetwork(Long)
     */
    @Override
    public List<SocialNetwork> findAllSocialNetwork(@NotNull Long idUser) throws LogicException {
        List<SocialNetwork> socialNetworks = new ArrayList<>();
        CarRentDao<SocialNetwork> carRentDao = SocialNetworkImpl.getInstance();
        UrlParser urlParser = new UrlParser();
        try {
            if (idUser != null) {
                socialNetworks = carRentDao.findAll(idUser);
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setType(urlParser.parseSocialNetwork(socialNetwork.getUrl()));
                }
            }
        } catch (DaoException e) {
            throw new LogicException("DaoException in method findAllSocialNetwork(Long idUser)", e);
        }
        return socialNetworks;
    }

    /**
     * (non-Javadoc)
     *
     * @see by.epam.rentcar.logic.SocialNetworkLogic#deleteSocialNetwork(Long)
     */
    @Override
    public boolean deleteSocialNetwork(Long idSocialNetwork) throws LogicException {
        CarRentDao<SocialNetwork> carRentDao = SocialNetworkImpl.getInstance();
        boolean result = false;
        if (idSocialNetwork != null) {
            try {
                carRentDao.delete(idSocialNetwork);
                result = true;
            } catch (DaoException e) {
                throw new LogicException("DaoException in method deleteSocialNetwork(Long idSocialNetwork)", e);
            }
        }
        return result;
    }


}
