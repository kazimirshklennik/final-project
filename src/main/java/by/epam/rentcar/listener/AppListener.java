package by.epam.rentcar.listener;

import by.epam.rentcar.pool.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * The type App listener.
 */
@WebListener
public class AppListener implements ServletContextListener {

    /**
     * The  Connection Pool.
     */
    private ConnectionPool connectionPool = null;

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(AppListener.class);

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.ServletContextListener#contextInitialized(ServletContextEvent)
     **/
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        connectionPool = ConnectionPool.getInstance();
        if (connectionPool != null) {
            LOGGER.info("Connection pool has been initialized.");
        }

        if (connectionPool == null) {
            LOGGER.fatal("Error in initializing the connection pool!");
            throw new RuntimeException("Fatal Error! Connection pool can't be initialized!");
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.ServletContextListener#contextDestroyed(ServletContextEvent)
     **/
    @Override
    public void contextDestroyed(ServletContextEvent sce) {

        if (connectionPool != null) {
            connectionPool.cleanUp();
        }
    }
}