package by.epam.rentcar.listener;

import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.List;

/**
 * The type Resource listener.
 */
@WebListener
public class ResourceListener implements HttpSessionAttributeListener {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(ResourceListener.class);

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
     **/
    @Override
    public void attributeAdded(HttpSessionBindingEvent se) {
        CarLogic carLogic = new CarLogicImpl();
        try {
            List<Car> carList = carLogic.findAllCar();
            List<Long> images = carLogic.findAllImage();
            se.getSession().setAttribute(AttributeName.ALL_IMAGES_CARS.getAttributeName(), images);
            se.getSession().setAttribute(AttributeName.ALL_CAR.getAttributeName(), carList);
        } catch (LogicException e) {
            LOGGER.error("Error added resource", e);
        }
    }

}

