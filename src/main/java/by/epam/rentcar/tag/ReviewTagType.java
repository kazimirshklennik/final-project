package by.epam.rentcar.tag;

/**
 * The enum Review tag type.
 */
public enum ReviewTagType {
    /**
     * Review review tag type.
     */
    REVIEW,
    /**
     * Carreview review tag type.
     */
    CARREVIEW
}

