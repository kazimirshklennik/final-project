package by.epam.rentcar.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.GregorianCalendar;

/**
 * The type Info time tag.
 */
public class InfoTimeTag extends TagSupport {

    /**
     * (non-Javadoc)
     *
     * @see TagSupport#doStartTag()
     **/
    @Override
    public int doStartTag() throws JspException {
        GregorianCalendar gc = new GregorianCalendar();
        String time = "<hr/>Time : <b> " + gc.getTime() + " </b><hr/>";
        try {
            JspWriter out = pageContext.getOut();
            out.write(time);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * (non-Javadoc)
     *
     * @see TagSupport#doEndTag() )
     **/
    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}