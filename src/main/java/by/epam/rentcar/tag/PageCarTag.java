package by.epam.rentcar.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The type Page car tag.
 */
public class PageCarTag extends TagSupport {

    /**
     * The Page number.
     */
    private int pageNumber;

    /**
     * The Max pages.
     */
    private int maxPages;

    /**
     * Sets page number.
     *
     * @param inputPageNumber the input page number
     */
    public void setPageNumber(String inputPageNumber) {
        this.pageNumber = Integer.parseInt(inputPageNumber);
    }

    /**
     * Sets max pages.
     *
     * @param inputMaxPages the input max pages
     */
    public void setMaxPages(String inputMaxPages) {

        this.maxPages = Integer.parseInt(inputMaxPages);
    }

    /**
     * (non-Javadoc)
     *
     * @see TagSupport#doStartTag()
     **/
    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();

        try {
            out.write("<hr/>");
            out.write("<a href=\"do?command=cars-page&page=1"+"\">  << </a>\n");
            out.write("<a href=\"do?command=cars-page&page=" + ((pageNumber > 1) ? (pageNumber - 1) : 1) + "\">  < </a>\n");
            if (pageNumber - 2 >= 1) {
                out.write("<a href=\"do?command=cars-page&page=" + (pageNumber - 2) + "\">" + (pageNumber - 2) + "</a>\n");
            }
            if (pageNumber - 1 >= 1) {
                out.write("<a href=\"do?command=cars-page&page=" + (pageNumber - 1) + "\">" + (pageNumber - 1) + "</a>\n");
            }
            out.write("<a href=\"do?command=cars-page&page=" + pageNumber + "\">" +"-"+pageNumber+"-"+"</a>\n");
            if (pageNumber + 1 <= maxPages) {
                out.write("<a href=\"do?command=cars-page&page=" + (pageNumber + 1) + "\">" + (pageNumber + 1) + "</a>\n");
            }
            if (pageNumber + 2 <= maxPages) {
                out.write("<a href=\"do?command=cars-page&page=" + (pageNumber + 2) + "\">" + (pageNumber + 2) + "</a>\n");
            }
            out.write("<a href=\"do?command=cars-page&page=" + ((pageNumber != maxPages) ? (pageNumber + 1) : maxPages) + "\"> >  </a>\n");
            out.write("<a href=\"do?command=cars-page&page="+maxPages+ "\">  >> </a>\n");

            out.write("<hr/>");
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }

    /**
     * (non-Javadoc)
     *
     * @see TagSupport#doEndTag()
     **/
    @Override
    public int doEndTag() throws JspException {
        return super.doEndTag();
    }
}
