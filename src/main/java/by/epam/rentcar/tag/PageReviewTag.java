package by.epam.rentcar.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The type Page review tag.
 */
public class PageReviewTag extends TagSupport {

    /**
     * The Page number.
     */
    private int pageNumber;

    /**
     * The Max pages.
     */
    private int maxPages;

    /**
     * The Id car.
     */
    private Long idCar;

    /**
     * The Type tag.
     */
    private ReviewTagType typeTag;

    /**
     * Sets page number.
     *
     * @param inputPageNumber the input page number
     */
    public void setPageNumber(String inputPageNumber) {
        this.pageNumber = Integer.parseInt(inputPageNumber);
    }

    /**
     * Sets max pages.
     *
     * @param inputMaxPages the input max pages
     */
    public void setMaxPages(String inputMaxPages) {

        this.maxPages = Integer.parseInt(inputMaxPages);
    }

    /**
     * Set type tag.
     *
     * @param inputType the input type
     */
    public void setTypeTag(String inputType) {
        this.typeTag = ReviewTagType.valueOf(inputType.toUpperCase());

    }

    /**
     * Set id car.
     *
     * @param idCar the id car
     */
    public void setIdCar(String idCar) {

        this.idCar = Long.parseLong(idCar);
    }

    /**
     * (non-Javadoc)
     *
     * @see TagSupport#doStartTag()
     **/
    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        switch (typeTag) {
            case REVIEW:
                reviewTag(out);
                break;
            case CARREVIEW: {
                reviewCarTag(out);
                break;
            }
        }
        return SKIP_BODY;
    }

    /**
     * Review tag.
     *
     * @param out the out
     * @throws JspException the jsp exception
     */
    private void reviewTag(JspWriter out) throws JspException {
        try {

            out.write("<hr/>");
            out.write("<a href=\"do?command=reviews-page&typeTag=review&page=1" + "\">  << </a>\n");
            out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + ((pageNumber > 1) ? (pageNumber - 1) : 1) + "\">  < </a>\n");
            if (pageNumber - 2 >= 1) {
                out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + (pageNumber - 2) + "\">" + (pageNumber - 2) + "</a>\n");
            }
            if (pageNumber - 1 >= 1) {
                out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + (pageNumber - 1) + "\">" + (pageNumber - 1) + "</a>\n");
            }
            out.write("<a mhref=\"do?command=reviews-page&typeTag=review&page=" + pageNumber + "\">" + "-" + pageNumber + "-" + "</a>\n");
            if (pageNumber + 1 <= maxPages) {
                out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + (pageNumber + 1) + "\">" + (pageNumber + 1) + "</a>\n");
            }
            if (pageNumber + 2 <= maxPages) {
                out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + (pageNumber + 2) + "\">" + (pageNumber + 2) + "</a>\n");
            }
            out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + ((pageNumber != maxPages) ? (pageNumber + 1) : maxPages) + "\"> >  </a>\n");
            out.write("<a href=\"do?command=reviews-page&typeTag=review&page=" + maxPages + "\">  >> </a>\n");


            out.write("<hr/>");
        } catch (IOException e) {
            throw new JspException(e);
        }
    }

    /**
     * Review car tag.
     *
     * @param out the out
     * @throws JspException the jsp exception
     */
    private void reviewCarTag(JspWriter out) throws JspException {
        try {
            out.write("<hr/>");
            out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=1\">  << </a>\n");
            out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + ((pageNumber > 1) ? (pageNumber - 1) : 1) + "\">  < </a>\n");
            if (pageNumber - 2 >= 1) {
                out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + (pageNumber - 2) + "\">" + (pageNumber - 2) + "</a>\n");
            }
            if (pageNumber - 1 >= 1) {
                out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + (pageNumber - 1) + "\">" + (pageNumber - 1) + "</a>\n");
            }
            out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + pageNumber + "\">" + "-" + pageNumber + "-" + "</a>\n");
            if (pageNumber + 1 <= maxPages) {
                out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + (pageNumber + 1) + "\">" + (pageNumber + 1) + "</a>\n");
            }
            if (pageNumber + 2 <= maxPages) {
                out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + (pageNumber + 2) + "\">" + (pageNumber + 2) + "</a>\n");
            }
            out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + ((pageNumber != maxPages) ? (pageNumber + 1) : maxPages) + "\"> >  </a>\n");
            out.write("<a href=\"do?command=reviews-page&typeTag=carreview&type=carreview&" + "idCar=" + idCar + "&page=" + maxPages + "\">  >> </a>\n");
            out.write("<hr/>");
        } catch (IOException e) {
            throw new JspException(e);
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see TagSupport#doEndTag()
     **/
    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
