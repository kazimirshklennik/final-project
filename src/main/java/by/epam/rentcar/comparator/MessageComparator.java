package by.epam.rentcar.comparator;

import by.epam.rentcar.entity.Message;

import java.util.Comparator;

/**
 * The class MessageComparator.
 */
public class MessageComparator implements Comparator<Message> {

    /**
     * (non-Javadoc)
     *
     * @see  java.util.Comparator#compare(Object, Object)
     *
     */
    @Override
    public int compare(Message o1, Message o2) {

        if (o1.getIdMessage() > o2.getIdMessage()) {
            return 1;
        } else if (o1.getIdMessage() < o2.getIdMessage()) {
            return -1;
        } else {
            return 0;
        }
    }
}





