package by.epam.rentcar.command;

import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.exception.CommandException;

import java.io.InputStream;

/**
 * The interface Command.
 */
public interface Command {

    /**
     * Execute.
     *
     * @param content the content
     * @return the string
     * @throws CommandException the command exception
     */
    Router execute(RequestContent content) throws CommandException;

    /**
     * Load.
     *
     * @param content the content
     * @return the InputStream
     * @throws CommandException the command exception
     */
    default InputStream load(RequestContent content) throws CommandException {
        throw new UnsupportedOperationException();
    }

}
