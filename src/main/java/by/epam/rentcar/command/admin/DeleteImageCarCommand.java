package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class DeleteImageCarCommand.
 */
public class DeleteImageCarCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String id= (String)content.getRequestParameters(AttributeName.ID_IMAGE.getAttributeName(),0);
        String idCar = (String)  content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0);
        Role role = Role.valueOf((String)content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));
        CarLogic carLogic = new CarLogicImpl();

        try {
            if(role==Role.ADMIN) {
                Long idImage = Long.parseLong(id);
                carLogic.deleteImageCar(idImage);
            }
        } catch (LogicException | NumberFormatException e) {
            throw new CommandException("LogicException in method execute() class DeleteImageCarCommand", e);
        }
        content.putRequestAttribute(AttributeName.ID_CAR.getAttributeName(),idCar);
        String path = UrlManager.getUrl(UrlType.CAR_INFO_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
