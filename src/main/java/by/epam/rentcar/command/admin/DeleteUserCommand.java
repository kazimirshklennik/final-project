package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class DeleteUserCommand.
 */
public class DeleteUserCommand implements Command {


    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login=(String)content.getRequestParameters(AttributeName.LOGIN.getAttributeName(),0);
        Role role = Role.valueOf((String)content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));

        UserLogic logic=new UserLogicImpl();

        try {
            if(role==Role.ADMIN) {
                logic.delete(login);
                content.putRequestAttribute(AttributeName.DELETE_SUCCESSFULLY.getAttributeName(), MessageManager.getProperty(MessageEnum.INFO_SUCCESSFULLY_DELETE, locale));
            }
        } catch (LogicException e) {
            throw  new CommandException("LogicException in method execute() class DeleteUserCommand",e);
        }

        String path=UrlManager.getUrl(UrlType.PROFILE_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
