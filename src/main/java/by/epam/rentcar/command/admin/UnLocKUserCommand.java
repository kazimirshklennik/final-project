package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class UnLocKUserCommand.
 */
public class UnLocKUserCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        Role role = Role.valueOf((String)content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));

        UserLogic logic= new UserLogicImpl();
        try {
            if(role==Role.ADMIN) {
                Long idUser=Long.parseLong((String)content.getRequestParameters(AttributeName.USER_ID.getAttributeName(),0));
                logic.unLock(idUser);
            }
        } catch (LogicException | NumberFormatException e) {
            throw  new CommandException("LogicException in method execute() class UnLocKUserCommand",e);
        }
        String path=UrlManager.getUrl(UrlType.PROFILE_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
