package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.ReviewLogic;
import by.epam.rentcar.logic.impl.ReviewLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class DeleteReviewCommand.
 */
public class DeleteReviewCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String idCar = (String) content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0);
        ReviewLogic reviewLogic = new ReviewLogicImpl();

        try {
            String idReview = (String) content.getRequestParameters(AttributeName.ID_REVIEW.getAttributeName(), 0);
            reviewLogic.deleteReview(Long.parseLong(idReview));
        } catch (LogicException | NumberFormatException e) {
            throw new CommandException("LogicException in method execute() class DeleteReviewCommand", e);
        }

        content.putRequestAttribute(AttributeName.ID_CAR.getAttributeName(), idCar);
        String path = UrlManager.getUrl(UrlType.CAR_INFO_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
