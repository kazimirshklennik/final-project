package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

/**
 * The Class AddCarCommand.
 */
public class AddCarCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent) ()
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String name = (String) content.getRequestParameters(AttributeName.CAR_NAME.getAttributeName(), 0);
        String model = (String) content.getRequestParameters(AttributeName.CAR_MODEL.getAttributeName(), 0);
        String info = (String) content.getRequestParameters(AttributeName.CAR_INFO.getAttributeName(), 0);
        Validator validator= new Validator();
        CarLogic logic = new CarLogicImpl();

        if (validator.checkDataCar(name, model, info)) {
            try {
                Car car = logic.createCar(name, model, info);
                logic.add(car);
                content.putRequestAttribute(AttributeName.SUCCESSFULLY_ADDED.getAttributeName(), MessageManager.getProperty(MessageEnum.ADD_SUCESFULLY, locale));
            } catch (LogicException e) {
                 throw new CommandException("LogicException in method execute() class AddCarCommand", e);
            }
        }else{
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
        }
        String path = UrlManager.getUrl(UrlType.PROFILE_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
