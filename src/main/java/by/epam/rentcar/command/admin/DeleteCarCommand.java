package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class DeleteCarCommand.
 */
public class DeleteCarCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        Role role = Role.valueOf((String)content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));
        CarLogic carLogic= new CarLogicImpl();

        try {
            Long idCar=Long.parseLong((String)content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(),0));
            if(role==Role.ADMIN) {
                carLogic.deleteCar(idCar);
            }
        } catch (LogicException | NumberFormatException e) {
            throw  new CommandException("LogicException in method execute() class DeleteCarCommand",e);
        }
        String path=UrlManager.getUrl(UrlType.PROFILE_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }

}
