package by.epam.rentcar.command.admin;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

import java.io.InputStream;

/**
 * The Class UpdateInfoCarCommand.
 */
public class UpdateInfoCarCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String cartName = (String) content.getRequestParameters(AttributeName.CAR_NAME.getAttributeName(), 0);
        String carModel = (String)  content.getRequestParameters(AttributeName.CAR_MODEL.getAttributeName(), 0);
        String priceString = (String)  content.getRequestParameters(AttributeName.PRICE.getAttributeName(), 0);
        String carInfo = (String)  content.getRequestParameters(AttributeName.CAR_INFO.getAttributeName(), 0);
        Role role = Role.valueOf((String)content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));

        InputStream  inputStream;
        CarLogic carLogic = new CarLogicImpl();
        Validator validator= new Validator();

        if(validator.checkDataUpdateInfoCar(cartName, carModel, carInfo, priceString)&&role==Role.ADMIN) {
            try {
                int price=Integer.parseInt(priceString);
                Long idCar = Long.parseLong((String)  content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0));
                Car car = carLogic.createCar(cartName, carModel, carInfo, price);
                car.setIdCar(idCar);
                inputStream = content.getInputStream(AttributeName.IMAGE.getAttributeName());
                carLogic.updateCar(car, inputStream);
            } catch (LogicException | NumberFormatException e) {
                throw new CommandException("LogicException in method execute() class UpdateInfoCarCommand", e);
            }
        }else{
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
        }
        String path = UrlManager.getUrl(UrlType.CAR_INFO_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
