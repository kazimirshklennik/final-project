package by.epam.rentcar.command;

import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.exception.CommandException;

/**
 * The Class ActionCommand.
 */
public class ActionCommand {

    /**
     * The action command.
     */
    private static ActionCommand actionCommand = new ActionCommand();

    /**
     * Instantiates a new action command.
     */
    private ActionCommand() {
    }

    /**
     * Gets the single instance of ActionCommand.
     *
     * @return single instance of ActionCommand
     */
    public static ActionCommand getInstance() {
        return actionCommand;
    }

    /**
     * Define command.
     *
     * @param content the request
     * @return the i command
     * @throws CommandException the command exception
     */
    public Command define(RequestContent content) throws CommandException {

        Command current;

        String action = (String) content.getRequestParameters(AttributeName.COMMAND.getAttributeName(), 0);
        if(action!=null) {
            try {
                CommandType commandType = CommandType.valueOf(action.replace("-", "_").toUpperCase());
                current = commandType.getCurrentCommand();

            } catch (IllegalArgumentException e) {
                throw new CommandException("CommandException in a ActionFactory class, command does not exist", e);
            }
        }else{
            throw new CommandException("CommandException in a ActionFactory class, command does not exist");
        }
        return current;
    }

}
