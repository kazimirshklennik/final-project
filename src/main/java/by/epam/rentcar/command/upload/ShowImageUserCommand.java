package by.epam.rentcar.command.upload;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;

import java.io.IOException;
import java.io.InputStream;

/**
 * The Class ShowImageUserCommand.
 */
public class ShowImageUserCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#load(RequestContent)
     */
    @Override
    public InputStream load(RequestContent content) throws CommandException {
        Long idUser = Long.parseLong((String)content.getRequestParameters(AttributeName.USER_ID.getAttributeName(),0));
        UserLogic userLogic = new UserLogicImpl();
        InputStream inputStream=null;

        if (idUser != null) {
            try {
                inputStream = userLogic.findUserImage(idUser);
                if (inputStream == null) {
                    inputStream = userLogic.defaultUserImage();
                }
                if(inputStream!=null) {
                    inputStream.close();
                }
            } catch (LogicException e) {
                throw new CommandException("LogicException in method load(RequestContent content) class ShowImageUserCommand", e);
            } catch (IOException e) {
                throw new CommandException("IOException in method load(RequestContent content) class ShowImageUserCommand", e);
            }
        }
        return  inputStream;
    }
}
