package by.epam.rentcar.command.constant;

/**
 * The class MessageData.
 */
public class MessageData {

    /** The Constant LOGIN_TEXT. */
    public static final String LOGIN_TEXT="Login";

    /** The Constant NEW_PASSWORD_TEXT. */
    public static final String NEW_PASSWORD_TEXT ="New password";

    /** The Constant FORMAT_MESSAGE. */
    public static final String FORMAT_MESSAGE="%s-%s\n%s-%s";

    /** The Constant PASSWORD_TEXT. */
    public static final String PASSWORD_TEXT="Password";

    /** The Constant SUBJECT. */
    public static final String SUBJECT="RentCarData";

    /** The Constant FORMAT_MESSAGE_EMAIL. */
    public static final String FORMAT_MESSAGE_EMAIL="Sent by %s\n%s";
}
