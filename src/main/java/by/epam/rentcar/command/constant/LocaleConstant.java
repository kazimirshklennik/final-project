package by.epam.rentcar.command.constant;

/**
 * The enum LocaleConstant.
 */
public enum LocaleConstant {

	/** The Constant RU_BY. */
	RU("ru"),

	/** The Constant EN_GB. */
	EN("en");

	/** The  locale. */
	private String locale;

	/**
	 * Instantiates a locale enum.
	 *
	 * @param locale the localee
	 */
	LocaleConstant(String locale){
		this.locale=locale;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public String getLocale(){
		return locale;
	}
}

