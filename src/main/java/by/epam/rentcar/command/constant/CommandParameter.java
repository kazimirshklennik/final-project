package by.epam.rentcar.command.constant;

/**
 * The class CommandParameter.
 */
public class CommandParameter {

    /** The Constant REVIEW_LIMIT. */
    public static final int REVIEW_LIMIT=4;

    /** The Constant CAR_LIMIT. */
    public static final int CAR_LIMIT=4;

    /** The Constant MAXIMUM_QUANTITY_OF_LINKS. */
    public static final int MAXIMUM_QUANTITY_OF_LINKS=5;

    /** The Constant PAGE. */
    public static final String PAGE = "-page-";

}
