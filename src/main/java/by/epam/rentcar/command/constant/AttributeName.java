package by.epam.rentcar.command.constant;

/**
 * The enum AttributeName.
 */
public enum AttributeName {

    /** The attribute name LOCALE. */
    LOCALE("locale"),

    /** The attribute name LOGIN. */
    LOGIN("login"),

    /** The attribute name USER_ID. */
    USER_ID("idUser"),

    /** The attribute name OLD_PASSWORD. */
    OLD_PASSWORD("oldpassword"),

    /** The attribute name NEW_PASSWORD. */
    NEW_PASSWORD("newpassword"),

    /** The attribute name PASSWORD. */
    PASSWORD("password"),

    /** The attribute name ALL_IMAGES_CARS. */
    ALL_IMAGES_CARS("allimagescar"),

    /** The attribute name ALL_IMAGE_CAR. */
    ALL_IMAGE_CAR("allphoto"),

    /** The attribute name SUCCESFULLY_UPDATED. */
    SUCCESFULLY_UPDATED("succesfullyUpdated"),

    /** The Constant UN_SUCCESFULLY_UPDATED. */
    UN_SUCCESFULLY_UPDATED("unsuccesfullyUpdated"),

    /** The attribute name INCORRECT_PASSWORD. */
    INCORRECT_PASSWORD("incorrectpassword"),

    /** The attribute name ERROR_LOGIN_PASSWORD_MESSAGE. */
    ERROR_LOGIN_PASSWORD_MESSAGE("errorLoginPassMessage"),

    /** The attribute name COMMAND. */
    COMMAND("command"),

    /** The attribute name LOGIN_RECIPIENT. */
    LOGIN_RECIPIENT("loginrecipient"),

    /** The attribute name TEXT_MESSAGE. */
    TEXT_MESSAGE("textmessage"),

    /** The attribute name TEXT_INFO. */
    TEXT_INFO("textinfo"),

    /** The attribute name SUBJECT. */
    SUBJECT("subject"),

    /** The attribute name SEND_SUCCESFYLLY. */
    SEND_SUCCESFYLLY("sendSuccessfully"),

    /** The attribute name ERROR_SEND_MESSAGE. */
    ERROR_SEND_MESSAGE("errorSendMessage"),

    /** The attribute name DELETE_RESERVE. */
    DELETE_RESERVE("deletereserve"),

    /** The attribute name DELETE_SOCIAL_NETWORK. */
    DELETE_SOCIAL_NETWORK("deletesocialnetworkSuccessfully"),

    /** The attribute name ERROR_EMAIL_EXIST. */
    ERROR_EMAIL_EXIST("errorEmailExist"),

    /** The attribute name ERROR_LOGIN_EXIST. */
    ERROR_LOGIN_EXIST("errorLoginExist"),

    /** The attribute name REGISTRATION_SUCCESSFULLY. */
    REGISTRATION_SUCCESSFULLY("registrationSuccessfully"),

    /** The attribute name USER. */
    USER("user"),

    /** The attribute name ID_REVIEW. */
    ID_REVIEW("idReview"),

    /** The attribute name USER_PROFILE. */
    USER_PROFILE("userprofile"),

    /** The attribute name USER_INFO. */
    USER_INFO("userInfo"),

    /** The attribute name USER_INFO_PROFILE. */
    USER_INFO_PROFILE("userInfoprofile"),

    /** The attribute name DATE_REGISTRATION_PROFILE. */
    DATE_REGISTRATION_PROFILE("dateRegistrationprofile"),

    /** The attribute name DATE_REGISTRATION. */
    DATE_REGISTRATION("dateRegistration"),

    /** The attribute name BIRTHDAY. */
    BIRTHDAY("birthday"),

    /** The attribute name BIRTHDAY_PROFILE. */
    BIRTHDAY_PROFILE("birthdayprofile"),

    /** The attribute name ID_SOCIAL_NETWORK. */
    ID_SOCIAL_NETWORK("idSocialNetwork"),

    /** The attribute name USER_DOES_NOT_EXIST. */
    USER_DOES_NOT_EXIST("userdoesnotexist"),

    /** The attribute name SEX. */
    SEX("sex"),

    /** The attribute name CITY. */
    CITY("city"),

    /** The attribute name COUNTRY. */
    COUNTRY("country"),

    /** The attribute name LAST_NAME. */
    LAST_NAME("lastname"),

    /** The attribute name FIRST_NAME. */
    FIRST_NAME("firstname"),

    /** The attribute name EMAIL. */
    EMAIL("email"),

    /** The attribute name DATE_ACTIVE. */
    DATE_ACTIVE("dateActive"),

    /** The attribute name USER_ROLE. */
    USER_ROLE("role"),

    /** The attribute name CAR_NAME. */
    CAR_NAME("carname"),

    /** The attribute name CAR_MODEL. */
    CAR_MODEL("carmodel"),

    /** The attribute name CAR_INFO. */
    CAR_INFO("carinfo"),

    /** The attribute name PRICE. */
    PRICE("price"),

    /** The attribute name ERROR_ADD_CAR. */
    ERROR_ADD_CAR("erroraddcar"),

    /** The attribute name ERROR_ADD_SOCIAL_NETWORK. */
    ERROR_ADD_SOCIAL_NETWORK("erroraddsocial"),

    /** The attribute name SUCCESSFULLY_ADDED. */
    SUCCESSFULLY_ADDED("succesfullyadded"),

    /** The attribute name INCORRECT_DATA. */
    INCORRECT_DATA("incorrectdata"),

    /** The attribute name ERROR_MAX_URL. */
    ERROR_MAX_URL("errormaxurlsocialnetwork"),

    /** The attribute name ALL_USER. */
    ALL_USER("alluser"),

    /** The attribute name ALL_CAR. */
    ALL_CAR("allcar"),

    /** The attribute name DELETE_SUCCESSFULLY. */
    DELETE_SUCCESSFULLY("successfullydeleted"),

    /** The attribute name DELETE_UN_SUCCESSFULLY. */
    DELETE_UN_SUCCESSFULLY("unsuccessfullydeleted"),

    /** The attribute name SOMETHING_WENT_WRONG. */
    SOMETHING_WENT_WRONG("somethingwentwrong"),

    /** The attribute name IMAGE_USER. */
    IMAGE_USER("imguser"),

    /** The attribute name IMAGE_PROFILE. */
    IMAGE_PROFILE("imgprofile"),

    /** The attribute name TYPE_PAGE. */
    TYPE_PAGE("type"),

    /** The attribute name ID_CAR. */
    ID_CAR("idCar"),

    /** The attribute name REVIEW. */
    REVIEW("review"),

    /** The attribute name ALL_REVIEW. */
    ALL_REVIEW("allreviewscar"),

    /** The attribute name ALL_REVIEWS_GLOBAL. */
    ALL_REVIEWS_GLOBAL("allreviews"),

    /** The attribute name CAR. */
    CAR("car"),

    /** The attribute name GENERAL_PHOTO_CAR. */
    GENERAL_PHOTO_CAR("generalcarfoto"),

    /** The attribute name ID_RESERVE. */
    ID_RESERVE("idReserve"),

    /** The attribute name ERROR_LOGIN. */
    ERROR_LOGIN("errorlogin"),

    /** The attribute name RESERVE_CAR_SUCCESSFULLY. */
    RESERVE_CAR_SUCCESSFULLY("reservesuccesfully"),

    /** The attribute name ADD_REVIEW_SUCCESSFULLY. */
    ADD_REVIEW_SUCCESSFULLY("addreviewsuccessfully"),

    /** The attribute name ERROR_ADD_REVIEW. */
    ERROR_ADD_REVIEW("erroraddreview"),

    /** The attribute name ERROR_ACTIVE_RESERVE. */
    ERROR_ACTIVE_RESERVE("erroractivereserve"),

    /** The attribute name DROP_OF_DATE. */
    DROP_OF_DATE("drop-off-date"),

    /** The attribute name PICK_UP_DATE. */
    PICK_UP_DATE("pick-up-date"),

    /** The attribute name CAR_SELECT. */
    CAR_SELECT("car-select"),

    /** The attribute name ERROR_DATE_RESERVE. */
    ERROR_DATE_RESERVE("errordate"),

    /** The attribute name ALL_RESERVE_USER. */
    ALL_RESERVE_USER("reserveall"),

    /** The attribute name BLOCKING_STATUS. */
    BLOCKING_STATUS("blockingstatus"),

    /** The attribute name ALL_CARS_GLOBAL. */
    ALL_CARS_GLOBAL("allcars"),

    /** The attribute name LOGIN_INTERLOCUTOR. */
    LOGIN_INTERLOCUTOR("loginInterlocutor"),

    /** The attribute name ID_INTERLOCUTOR. */
    ID_INTERLOCUTOR("idInterlocutor"),

    /** The attribute name INTERLOCUTORS. */
    INTERLOCUTORS("interlocutors"),

    /** The attribute name MESSAGES. */
    MESSAGES("messages"),

    /** The attribute name INTERLOCUTOR. */
    INTERLOCUTOR("interlocutor"),

    /** The attribute name URL_SOCIAL_NETWORK. */
    URL_SOCIAL_NETWORK("urlsocial"),

    /** The attribute name IMAGE. */
    IMAGE("image"),

    /** The attribute name ID_IMAGE. */
    ID_IMAGE("idImage"),

    /** The attribute name ERROR. */
    ERROR("error"),

    /** The attribute name PAGE. */
    PAGE("page"),

    /** The attribute name PAGE_NUMBER. */
    PAGE_NUMBER("pageNumber"),

    /** The attribute name MAX_PAGE_NUMBER. */
    MAX_PAGE_NUMBER("maxPages"),

    /** The attribute name TYPE_TAG. */
    TYPE_TAG("typeTag");

    /** The attrilbute name. */
    private final String name;

    /**
     * Instantiates a attributlle enum.
     *
     * @param name the attlribute name
     */
    AttributeName(String name) {
        this.name = name;

    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getAttributeName() {
        return this.name;
    }

}

