package by.epam.rentcar.command.page;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.CommandParameter;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.tag.ReviewTagType;

/**
 * The Class ReviewsPageCommand.
 */
public class ReviewsPageCommand implements Command {


    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String path = UrlManager.getUrl(UrlType.SIMPLE_REVIEWS);

        String pageStr = (String) content.getRequestParameters(AttributeName.PAGE.getAttributeName(), 0);
        String typeTag = (String) content.getRequestParameters(AttributeName.TYPE_TAG.getAttributeName(), 0);
        String idCar = (String) content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0);

        if (typeTag != null) {
            ReviewTagType type = ReviewTagType.valueOf(typeTag.toUpperCase());
            switch (type) {
                case REVIEW:
                    if (pageStr != null) {
                        path = UrlManager.getUrl(UrlType.SIMPLE_REVIEWS_PAGE) + pageStr;
                    }
                    break;
                case CARREVIEW:
                    if (idCar != null && pageStr != null) {
                        path = UrlManager.getUrl(UrlType.SIMPLE_REVIEWS_CAR) + idCar + CommandParameter.PAGE + pageStr;
                    } else {
                        if (idCar != null) {
                            path = UrlManager.getUrl(UrlType.SIMPLE_REVIEWS_CAR) + idCar;
                        }
                    }
                    break;
            }
        }

        return new Router(path, TransmisionType.REDIRECT);
    }
}
