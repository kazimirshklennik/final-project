package by.epam.rentcar.command.page;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class MessagesPageCommand.
 */
public class MessagesPageCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String path= UrlManager.getUrl(UrlType.SIMPLE_MESSAGES);
        return new Router(path, TransmisionType.REDIRECT);
    }
}
