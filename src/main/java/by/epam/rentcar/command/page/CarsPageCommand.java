package by.epam.rentcar.command.page;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class CarsPageCommand.
 */
public class CarsPageCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String path = UrlManager.getUrl(UrlType.SIMPLE_CARS);
        String page = (String) content.getRequestParameters(AttributeName.PAGE.getAttributeName(), 0);
        if(page!=null){
            path=UrlManager.getUrl(UrlType.SIMPLE_CARS_PAGE)+page;
        }
        return new Router(path, TransmisionType.REDIRECT);
    }
}
