package by.epam.rentcar.command.page;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class InfoCarPageCommand.
 */
public class InfoCarPageCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String idCar=(String)content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(),0);
        String path= UrlManager.getUrl(UrlType.SIMPLE_CAR_INFO) +idCar;
        return new Router(path, TransmisionType.REDIRECT);
    }
}
