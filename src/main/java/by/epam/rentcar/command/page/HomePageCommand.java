package by.epam.rentcar.command.page;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

import java.util.List;

/**
 * The Class HomePageCommand.
 */
public class HomePageCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        CarLogic carLogic = new CarLogicImpl();
        try {
            List<Car> carList = carLogic.findAllCar();
            content.setSessionAttribute(AttributeName.ALL_CAR.getAttributeName(), carList);
        }catch (LogicException e) {
            throw new CommandException("LogicException in method execute() class HomePageCommand", e);

        }
        String path = UrlManager.getUrl(UrlType.INDEX);
        return new Router(path, TransmisionType.FORWARD);
    }
}
