package by.epam.rentcar.command;


import by.epam.rentcar.command.admin.*;
import by.epam.rentcar.command.global.*;
import by.epam.rentcar.command.page.*;
import by.epam.rentcar.command.upload.ShowImageCarCommand;
import by.epam.rentcar.command.upload.ShowImageUserCommand;

/**
 * The enum CommandType.
 */
public enum CommandType {
    /**
     * The login.
     */
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },

    /**
     * The home.
     */
    TO_HOME {
        {
            this.command = new HomePageCommand();
        }
    },

    /**
     * The logout.
     */
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    /**
     * The register.
     */
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    /**
     * The en locale.
     */
    EN {
        {
            this.command = new LocaleCommand();
        }
    },
    /**
     * The ru locale.
     */
    RU {
        {
            this.command = new LocaleCommand();
        }
    },
    /**
     * The tologin.
     */
    TOLOGIN {
        {
            this.command = new LoginPageCommand();
        }
    },
    /**
     * The profile.
     */
    PROFILE {
        {
            this.command = new ProfileCommand();
        }
    },

    /**
     * The simple profilen.
     */
    SIMPLE_PROFILE {
        {
            this.command = new ProfilePageCommand();
        }
    },

    /**
     * The car page.
     */
    CARS_PAGE {
        {
            this.command = new CarsPageCommand();
        }
    },

    /**
     * The reviews page.
     */
    REVIEWS_PAGE {
        {
            this.command = new ReviewsPageCommand();
        }
    },

    /**
     * The send message.
     */
    SEND_MESSAGE {
        {
            this.command = new SendMessageCommand();
        }
    },

    /**
     * The send mail.
     */
    SEND_MAIL {
        {
            this.command = new MailCommand();
        }
    },
    /**
     * The update info.
     */
    UPDATE_INFO {
        {
            this.command = new UpdateInfoUserCommand();
        }
    },
    /**
     * The change password
     */
    UPDATE_PASSWORD {
        {
            this.command = new ChangePasswordCommand();
        }
    },

    /**
     * The add-car
     */
    ADD_CAR {
        {
            this.command = new AddCarCommand();
        }
    },

    /**
     * The lock user
     */
    LOCK {
        {
            this.command = new LockUserCommand();
        }
    },

    /**
     * The unlock user
     */
    UNLOCK {
        {
            this.command = new UnLocKUserCommand();
        }
    },

    /**
     * The delete user
     */
    DELETE_USER {
        {
            this.command = new DeleteUserCommand();
        }
    },

    /**
     * The info car
     */
    INFO_CAR {
        {
            this.command = new InfoCarCommand();
        }
    },
    /**
     * The add review
     */
    ADD_REVIEW {
        {
            this.command = new AddReviewCommand();
        }
    },

    /**
     * The car reservation.
     */
    CAR_RESERVATION {
        {
            this.command = new ReservationCommand();
        }
    },

    /**
     * The simple message.
     */
    SIMPLE_MESSAGES {
        {
            this.command = new MessagesPageCommand();
        }
    },

    /**
     * The to review.
     */
    TO_REVIEW {
        {
            this.command = new AllReviewsCommand();
        }
    },

    /**
     * The to all cars.
     */
    TO_ALL_CARS {
        {
            this.command = new AllCarsCommand();
        }
    },

    /**
     * The update car info.
     */
    UPDATE_CAR_INFO {
        {
            this.command = new UpdateInfoCarCommand();
        }
    },

    /**
     * The messages.
     */
    MESSAGES {
        {
            this.command = new MessagesCommand();
        }
    },

    /**
     * The delete car.
     */
    DELETE_CAR {
        {
            this.command = new DeleteCarCommand();
        }
    },

    /**
     * The delete reserve.
     */
    DELETE_RESERVE {
        {
            this.command = new DeleteReserveCommand();
        }
    },
    ADD_SOCIAL {
        {
            this.command = new AddSocialNetworkCommand();
        }
    },

    /**
     * The delete social.
     */
    DELETE_SOCIAL {
        {
            this.command = new DeleteSocialNetworkCommand();
        }

    },

    /**
     * The image car.
     */
    IMAGE_CAR {
        {
            this.command = new ShowImageCarCommand();
        }
    },

    /**
     * The image user.
     */
    IMAGE_USER {
        {
            this.command = new ShowImageUserCommand();
        }
    },

    /**
     * The delete image car.
     */
    DELETE_IMAGE_CAR {
        {
            this.command = new DeleteImageCarCommand();
        }
    },

    /**
     * The page info car.
     */
    PAGE_INFO_CAR {
        {
            this.command = new InfoCarPageCommand();
        }
    },

    /**
     * The index
     */
    INDEX {
        {
            this.command = new IndexPageCommand();
        }
    },

    /**
     * The error profile.
     */
    ERROR_PROFILE {
        {
            this.command = new ErrorProfilePageCommand();
        }
    },

    /**
     * The delete review.
     */
    DELETE_REVIEW {
        {
            this.command = new DeleteReviewCommand();
        }
    };

    /**
     * The command.
     */
    Command command;

    /**
     * Gets the current command.
     *
     * @return the current command
     */
    public Command getCurrentCommand() {

        return command;
    }
}
