package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.MessageData;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Message;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.logic.ServiceLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.MessageLogicImpl;
import by.epam.rentcar.logic.impl.ServiceLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.util.PasswordEncryption;
import by.epam.rentcar.validator.Validator;

import java.util.Formatter;

/**
 * The Class RegisterCommand.
 */
public class RegisterCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String login = (String) content.getRequestParameters(AttributeName.LOGIN.getAttributeName(), 0);
        String email = (String) content.getRequestParameters(AttributeName.EMAIL.getAttributeName(), 0);
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        UserLogic userLogic = new UserLogicImpl();
        MessageLogic messageLogic= new MessageLogicImpl();
        ServiceLogic serviceLogic = new ServiceLogicImpl();
        Formatter formatter= new Formatter();
        Validator validator= new Validator();
        Router router;

       if(validator.checkRegisterData(login, email)) {
           try {
               if (!userLogic.checkLogin(login)) {
                   if (!userLogic.checkEmail(email)) {
                       String passwordOriginal = serviceLogic.generetePassword();
                       String passwordEncryption = new PasswordEncryption().create(passwordOriginal);
                       userLogic.addUser(login, passwordEncryption, email);
                       formatter.format(MessageData.FORMAT_MESSAGE, MessageData.LOGIN_TEXT, login, MessageData.PASSWORD_TEXT, passwordOriginal);
                       Message message = messageLogic.createMessageAuthorization(MessageData.SUBJECT, email, formatter.toString());
                       messageLogic.sendEmail(message);

                       content.setSessionAttribute(AttributeName.REGISTRATION_SUCCESSFULLY.getAttributeName(), MessageManager.getProperty(MessageEnum.REGISTRATION_SUCCESSFULLY, locale));
                       String path = UrlManager.getUrl(UrlType.INDEX_AFTER_REGISTER);
                       router = new Router(path, TransmisionType.REDIRECT);

                   } else {
                       content.putRequestAttribute(AttributeName.ERROR_EMAIL_EXIST.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_EXIST_MAIL, locale));
                       String path = UrlManager.getUrl(UrlType.SIMPLE_LOGIN);
                       router = new Router(path, TransmisionType.FORWARD);
                   }
               } else {
                   content.putRequestAttribute(AttributeName.ERROR_LOGIN_EXIST.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_EXIST_LOGIN, locale));
                   String path = UrlManager.getUrl(UrlType.SIMPLE_LOGIN);
                   router = new Router(path, TransmisionType.FORWARD);
               }
           } catch (LogicException e) {
               throw new CommandException("LogicException in method execute() class RegisterCommand", e);
           }
       }else{
           content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
           String path = UrlManager.getUrl(UrlType.SIMPLE_LOGIN);
           router = new Router(path, TransmisionType.FORWARD);
       }
        return router;
    }
}

