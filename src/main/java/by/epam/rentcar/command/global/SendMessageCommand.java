package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Message;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.MessageLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

import java.util.List;

/**
 * The Class SendMessageCommand.
 */
public class SendMessageCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String recipient = (String) content.getRequestParameters(AttributeName.LOGIN_RECIPIENT.getAttributeName(), 0);
        String textmessage = (String) content.getRequestParameters(AttributeName.TEXT_MESSAGE.getAttributeName(), 0);
        String sender = (String) content.getSessionAttribute(AttributeName.LOGIN.getAttributeName());

        Validator validator = new Validator();
        MessageLogic messageLogic= new MessageLogicImpl();
        UserLogic userLogic= new UserLogicImpl();
        Router router = null;

        if (validator.checkMessage(textmessage)) {
            try {
                if (userLogic.checkLogin(recipient)) {
                    Message message = messageLogic.createMessage(sender, recipient, textmessage);
                    messageLogic.sendMessage(message);
                    content.putRequestAttribute(AttributeName.SEND_SUCCESFYLLY.getAttributeName(), MessageManager.getProperty(MessageEnum.MESSAGE_SEND_SUCCESSFULLY, locale));
                } else {
                    content.putRequestAttribute(AttributeName.ERROR_SEND_MESSAGE.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_SEND_MESSAGE, locale));
                }
            } catch (LogicException e) {
                 throw new CommandException("LogicException in method doAction() class SendMessageCommand", e);
            }
        }
        if (content.getRequestParameters(AttributeName.TYPE_PAGE.getAttributeName(), 0) == null) {
            if (content.getRequestParameters(AttributeName.USER.getAttributeName(), 0) == null) {
                String path = UrlManager.getUrl(UrlType.SIMPLE_PROFILE_USER)+recipient;
                router = new Router(path, TransmisionType.FORWARD);
            }
        } else {
            List<Message> messages;
            try {
                Long idUser = Long.parseLong((String) content.getSessionAttribute(AttributeName.USER_ID.getAttributeName()));
                Long idProfile = userLogic.findIdUser(recipient);
                List<User> interlocutors = messageLogic.findAllUsersChat(idUser);
                messages = messageLogic.findChat(idUser, idProfile);
                User interlocutor = userLogic.findUser(recipient);

                content.putRequestAttribute(AttributeName.INTERLOCUTORS.getAttributeName(), interlocutors);
                content.putRequestAttribute(AttributeName.MESSAGES.getAttributeName(), messages);
                content.putRequestAttribute(AttributeName.INTERLOCUTOR.getAttributeName(), interlocutor);

            } catch (LogicException | NumberFormatException e) {
                throw new CommandException("LogicException in method execute() class SendMessageCommand", e);
            }
            String path = UrlManager.getUrl(UrlType.MESSAGES);
            router = new Router(path, TransmisionType.FORWARD);
        }
        return router;
    }
}
