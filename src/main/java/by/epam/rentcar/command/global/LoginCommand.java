package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.util.PasswordEncryption;
import by.epam.rentcar.validator.Validator;

public class LoginCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String login = (String) content.getRequestParameters(AttributeName.LOGIN.getAttributeName(), 0);
        String password = new PasswordEncryption().create((String) content.getRequestParameters(AttributeName.PASSWORD.getAttributeName(), 0));
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        UserLogic userLogic = new UserLogicImpl();
        Validator validator= new Validator();
        Router router;

        if(validator.checkDataLogin(login,password)) {
            try {
                if (userLogic.checkLoginAndPassword(login, password)) {
                    User user = userLogic.findUser(login, password);
                    if (!user.isLockStatus()) {
                        content.setSessionAttribute(AttributeName.USER_ROLE.getAttributeName(), String.valueOf(user.getRole()));
                        content.setSessionAttribute(AttributeName.LOGIN.getAttributeName(), String.valueOf(user.getLogin()));
                        content.setSessionAttribute(AttributeName.USER_ID.getAttributeName(), String.valueOf(user.getIdUser()));
                        userLogic.setOnline(user.getIdUser());
                        String path = UrlManager.getUrl(UrlType.HOME);
                        router = new Router(path, TransmisionType.REDIRECT);
                    } else {
                        content.setSessionAttribute(AttributeName.BLOCKING_STATUS.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_BLOCKING_STATUS, locale));
                        String path = UrlManager.getUrl(UrlType.ERROR_BLOCKING);
                        router = new Router(path, TransmisionType.REDIRECT);
                    }

                } else {
                    content.putRequestAttribute(AttributeName.ERROR_LOGIN_PASSWORD_MESSAGE.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_INCORRECT_PASSWORD_OR_LOGIN, locale));
                    String path = UrlManager.getUrl(UrlType.SIMPLE_LOGIN);
                    router = new Router(path, TransmisionType.FORWARD);
                }
            } catch (LogicException e) {
                throw new CommandException("LogicException in method execute() class LoginCommand", e);
            }
        }else{
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
            String path = UrlManager.getUrl(UrlType.SIMPLE_LOGIN);
            router = new Router(path, TransmisionType.FORWARD);
        }
        return router;
    }
}

