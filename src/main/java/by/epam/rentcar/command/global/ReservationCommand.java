package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Reserve;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.ReserveLogic;
import by.epam.rentcar.logic.impl.ReserveLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class ReservationCommand.
 */
public class ReservationCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String dropOffDate = (String) content.getRequestParameters(AttributeName.DROP_OF_DATE.getAttributeName(), 0);
        String pickUpDate = (String) content.getRequestParameters(AttributeName.PICK_UP_DATE.getAttributeName(), 0);
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login = (String) content.getSessionAttribute(AttributeName.LOGIN.getAttributeName());
        Router router;
        ReserveLogic reserveLogic = new ReserveLogicImpl();

        if (login != null) {
            if (reserveLogic.checkDate(pickUpDate, dropOffDate)) {
                try {
                    Long idUser = Long.parseLong((String) content.getSessionAttribute(AttributeName.USER_ID.getAttributeName()));
                    if (!reserveLogic.checkActiveReserveByIdUser(idUser)) {
                        Long idCar = Long.parseLong((String) content.getRequestParameters(AttributeName.CAR_SELECT.getAttributeName(), 0));
                        Reserve reserve = reserveLogic.createReserve(pickUpDate, dropOffDate, idCar, idUser);
                        reserveLogic.reserveCar(reserve);
                        content.putRequestAttribute(AttributeName.RESERVE_CAR_SUCCESSFULLY.getAttributeName(), MessageManager.getProperty(MessageEnum.RESERVE_CAR_SUCCESSFULLY, locale));
                    } else {
                        content.putRequestAttribute(AttributeName.ERROR_ACTIVE_RESERVE.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_ACTIVE_RESERVE, locale));
                    }
                } catch (LogicException | NumberFormatException e) {
                    throw new CommandException("LogicException in method execute() class ReservationCommand", e);
                }
                String path = UrlManager.getUrl(UrlType.HOME);
                router=new Router(path, TransmisionType.FORWARD);
            } else {
                content.putRequestAttribute(AttributeName.ERROR_DATE_RESERVE.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_DATE_RESERVE, locale));
                String path = UrlManager.getUrl(UrlType.HOME);
                router=new Router(path, TransmisionType.FORWARD);
            }
        } else {
            String path = UrlManager.getUrl(UrlType.ERROR);
            content.setSessionAttribute(AttributeName.ERROR_LOGIN.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR, locale));
            router=new Router(path, TransmisionType.REDIRECT);
        }
        return router;
    }
}

