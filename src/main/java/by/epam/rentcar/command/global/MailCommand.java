package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.MessageData;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Message;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.MessageLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

import java.util.Formatter;

/**
 * The Class MailCommand.
 */
public class MailCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login=(String)content.getSessionAttribute(AttributeName.LOGIN.getAttributeName()) ;
        String recipient = (String) content.getRequestParameters(AttributeName.LOGIN_RECIPIENT.getAttributeName(), 0);
        String textmessage = (String) content.getRequestParameters(AttributeName.TEXT_MESSAGE.getAttributeName(), 0);
        String subject = (String) content.getRequestParameters(AttributeName.SUBJECT.getAttributeName(), 0);
        MessageLogic messageLogic= new MessageLogicImpl();
        UserLogic userLogic = new UserLogicImpl();
        Formatter formatter= new Formatter();
        Validator validator = new Validator();

        if (validator.checkMessage(textmessage)) {
            try {
                if (userLogic.checkLogin(recipient)) {
                    formatter.format(MessageData.FORMAT_MESSAGE_EMAIL,login,textmessage);
                    Message message = messageLogic.createMessageEmail(subject, recipient, formatter.toString());
                    messageLogic.sendEmail(message);
                    content.putRequestAttribute(AttributeName.SEND_SUCCESFYLLY.getAttributeName(), MessageManager.getProperty(MessageEnum.MESSAGE_SEND_SUCCESSFULLY, locale));
                } else {
                    content.putRequestAttribute(AttributeName.ERROR_SEND_MESSAGE.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_SEND_MESSAGE, locale));
                }
            } catch (LogicException e) {
                throw new CommandException("LogicException in method execute() class MailCommand", e);
            }
        } else {
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));

        }
        String path = UrlManager.getUrl(UrlType.SIMPLE_PROFILE_USER)+recipient;
        return new Router(path, TransmisionType.FORWARD);
    }
}
