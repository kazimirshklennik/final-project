package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.ReviewLogic;
import by.epam.rentcar.logic.impl.ReviewLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

/**
 * The Class AddReviewCommand.
 */
public class AddReviewCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String locale=(String)content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String text = (String) content.getRequestParameters(AttributeName.REVIEW.getAttributeName(), 0);
        String idUserString=(String)content.getSessionAttribute(AttributeName.USER_ID.getAttributeName());
        Validator validator= new Validator();
        ReviewLogic reviewLogic = new ReviewLogicImpl();

        if(validator.checkDataReview(text)) {
            try {
                Long idUser=Long.parseLong(idUserString);
                Long idCar = Long.parseLong((String) content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0));
                Review review = reviewLogic.createReview(idUser, idCar, text);
                reviewLogic.addReview(review);
                content.putRequestAttribute(AttributeName.ADD_REVIEW_SUCCESSFULLY.getAttributeName(), MessageManager.getProperty(MessageEnum.ADD_SUCESFULLY, locale));

            } catch (LogicException | NumberFormatException e) {
                 throw new CommandException("LogicException in method execute() class AddReviewCommand", e);
            }
        }else{
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
        }
        String path = UrlManager.getUrl(UrlType.CAR_INFO_COMMAND);
       return new Router(path, TransmisionType.FORWARD);
    }

}
