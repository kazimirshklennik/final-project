package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class LogoutCommand.
 */
public class LogoutCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        UserLogic userLogic = new UserLogicImpl();
        try {
            Long idUser = Long.parseLong((String) content.getSessionAttribute(AttributeName.USER_ID.getAttributeName()));
            userLogic.setOffline(idUser);
            content.invalidateSession();
        } catch (LogicException | NumberFormatException e) {
            throw new CommandException("LogicException in method execute() class LogoutCommand", e);
        }

        String path = UrlManager.getUrl(UrlType.HOME);
        return new Router(path, TransmisionType.FORWARD);
    }
}
