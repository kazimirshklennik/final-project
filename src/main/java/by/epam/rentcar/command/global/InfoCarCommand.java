package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.ReviewLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.logic.impl.ReviewLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

import java.util.List;

/**
 * The Class InfoCarCommand.
 */
public class InfoCarCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        Car car;
        List<Long> images;
        List<Review> reviews;
        CarLogic carLogic = new CarLogicImpl();
        ReviewLogic reviewLogic = new ReviewLogicImpl();

        try {
            Long idCar = Long.parseLong((String) content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0));
           if(carLogic.checkCar(idCar)) {
               images = new CarLogicImpl().findAllImagesCar(idCar);
               car = carLogic.findCarById(idCar);
               reviews = reviewLogic.findAllReviewByIdCar(idCar);

               if (!images.isEmpty()) {
                   car.setIdGeneralPhoto(images.get(0));
               }
               content.putRequestAttribute(AttributeName.ALL_REVIEW.getAttributeName(), reviews);
               content.putRequestAttribute(AttributeName.CAR.getAttributeName(), car);
               content.putRequestAttribute(AttributeName.ALL_IMAGE_CAR.getAttributeName(), images);
           }else{
               String path = UrlManager.getUrl(UrlType.ERROR_CAR_INFO);
               return new Router(path, TransmisionType.REDIRECT);
           }

        } catch (LogicException | NumberFormatException e) {
            throw new CommandException("LogicException in method execute() class InfoCarCommand", e);
        }

        String path = UrlManager.getUrl(UrlType.CAR_INFO);
        return new Router(path, TransmisionType.FORWARD);
    }
}
