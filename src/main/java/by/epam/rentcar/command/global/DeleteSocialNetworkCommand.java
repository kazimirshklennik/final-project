package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.SocialNetworkLogic;
import by.epam.rentcar.logic.impl.SocialNetworkLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class DeleteSocialNetworkCommand.
 */
public class DeleteSocialNetworkCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        SocialNetworkLogic socialNetworkLogic = new SocialNetworkLogicImpl();
        Role role = Role.valueOf((String) content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));

        if (role == Role.USER || role == Role.ADMIN) {
            try {
                Long idSocialNetwork = Long.parseLong((String) content.getRequestParameters(AttributeName.ID_SOCIAL_NETWORK.getAttributeName(), 0));
                socialNetworkLogic.deleteSocialNetwork(idSocialNetwork);
                content.putRequestAttribute(AttributeName.DELETE_SOCIAL_NETWORK.getAttributeName(), MessageManager.getProperty(MessageEnum.DELETE_SOCIAL_NETWORK, locale));
            } catch (LogicException | NumberFormatException e) {
                throw new CommandException("LogicException in method execute() class DeleteSocialNetworkCommand", e);
            }
        }
        String path = UrlManager.getUrl(UrlType.PROFILE_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}

