package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.ReserveLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.logic.impl.ReserveLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class ProfileCommand.
 */
public class ProfileCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login = (String) content.getSessionAttribute(AttributeName.LOGIN.getAttributeName());
        String loginRequest = (String) content.getRequestParameters(AttributeName.LOGIN.getAttributeName(), 0);
        UserLogic userLogic = new UserLogicImpl();
        CarLogic carLogic = new CarLogicImpl();
        ReserveLogic reserveLogic = new ReserveLogicImpl();
        Router router;

        try {
            if (login != null) {
                if (loginRequest == null || login.equals(loginRequest) || content.getRequestParameters(AttributeName.TYPE_PAGE.getAttributeName(), 0) == null) {

                    User user = userLogic.findUser(login);
                    content.setSessionAttribute(AttributeName.ALL_RESERVE_USER.getAttributeName(), reserveLogic.findAllReserveByIdUser(user.getIdUser()));
                    content.setSessionAttribute(AttributeName.USER.getAttributeName(), user);

                    if (user.getRole() == Role.ADMIN) {
                        try {
                            content.setSessionAttribute(AttributeName.ALL_USER.getAttributeName(), userLogic.findAllUser());
                            content.setSessionAttribute(AttributeName.ALL_CAR.getAttributeName(), carLogic.findAllCar());
                        } catch (LogicException e) {
                            throw new CommandException("LogicException in method execute() class  ProfileCommand", e);
                        }
                    }
                    String path = UrlManager.getUrl(UrlType.PROFILE);
                    router = new Router(path, TransmisionType.FORWARD);

                } else {
                    User user = userLogic.findUser(loginRequest);
                    if (user.getLogin() != null) {
                        content.putRequestAttribute(AttributeName.ALL_RESERVE_USER.getAttributeName(), reserveLogic.findAllReserveByIdUser(user.getIdUser()));
                        content.putRequestAttribute(AttributeName.USER_PROFILE.getAttributeName(), user);
                        content.putRequestAttribute(AttributeName.IMAGE_PROFILE.getAttributeName(), user.getIdUser());

                        String path = UrlManager.getUrl(UrlType.USER_PROFILE);
                        router = new Router(path, TransmisionType.FORWARD);
                    } else {
                        content.putRequestAttribute(AttributeName.USER_DOES_NOT_EXIST.getAttributeName(), MessageManager.getProperty(MessageEnum.USER_DOES_NOT_EXIST, locale));
                        String path = UrlManager.getUrl(UrlType.ERROR_PROFILE);
                        router = new Router(path, TransmisionType.REDIRECT);
                    }
                }
            } else {
                String path = UrlManager.getUrl(UrlType.ERROR);
                content.setSessionAttribute(AttributeName.ERROR_LOGIN.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR, locale));
                router = new Router(path, TransmisionType.REDIRECT);
            }
        } catch (LogicException e) {
            throw new CommandException("LogicException in method doAction() class  ProfileCommand", e);
        }
        return router;
    }
}
