package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Reserve;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.impl.ReserveLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class DeleteReserveCommand.
 */
public class DeleteReserveCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        Role role = Role.valueOf((String)content.getSessionAttribute(AttributeName.USER_ROLE.getAttributeName()));

       ReserveLogicImpl reserveLogic = new ReserveLogicImpl();
        if (role == Role.USER || role == Role.ADMIN) {
         try {
             Long idReserve = Long.parseLong((String) content.getRequestParameters(AttributeName.ID_RESERVE.getAttributeName(), 0));
             Reserve reserve = reserveLogic.findById(idReserve);
             reserveLogic.cancellationReserve(reserve);
             content.putRequestAttribute(AttributeName.DELETE_RESERVE.getAttributeName(), MessageManager.getProperty(MessageEnum.DELETE_RESERVE, locale));

         } catch (LogicException | NumberFormatException e) {
             throw new CommandException("LogicException in method execute() class DeleteReserveCommand", e);
         }
     }
        String path = UrlManager.getUrl(UrlType.PROFILE_COMMAND);
       return new Router(path, TransmisionType.FORWARD);
    }
}
