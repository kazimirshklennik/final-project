package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

import java.io.InputStream;

/**
 * The Class UpdateInfoUserCommand.
 */
public class UpdateInfoUserCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String locale = (String) content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login = (String) content.getSessionAttribute(AttributeName.LOGIN.getAttributeName());

        String firstName = (String) content.getRequestParameters(AttributeName.FIRST_NAME.getAttributeName(), 0);
        String lastName = (String) content.getRequestParameters(AttributeName.LAST_NAME.getAttributeName(), 0);
        String country = (String) content.getRequestParameters(AttributeName.COUNTRY.getAttributeName(), 0);
        String city = (String) content.getRequestParameters(AttributeName.CITY.getAttributeName(), 0);
        String sex = (String) content.getRequestParameters(AttributeName.SEX.getAttributeName(), 0);
        String date = (String) content.getRequestParameters(AttributeName.BIRTHDAY.getAttributeName(), 0);
        String textInfo = (String) content.getRequestParameters(AttributeName.TEXT_INFO.getAttributeName(), 0);

        InputStream inputStream;
        UserLogic userLogic = new UserLogicImpl();
        Validator validator = new Validator();

        if (validator.checkUpdateUserInfoData(firstName, lastName, country, city, sex, textInfo)) {
            try {
                Long idUser = Long.parseLong((String) content.getSessionAttribute(AttributeName.USER_ID.getAttributeName()));
                User user = userLogic.createUser(country, city, sex, textInfo, date, firstName, lastName);
                content.putRequestAttribute(AttributeName.SUCCESFULLY_UPDATED.getAttributeName(), MessageManager.getProperty(MessageEnum.INFO_SUCCESSFUL_UPDATE, locale));
                user.setIdUser(idUser);
                inputStream = content.getInputStream(AttributeName.IMAGE.getAttributeName());
                userLogic.update(user, inputStream);
            } catch (LogicException | NumberFormatException e) {
                content.putRequestAttribute(AttributeName.UN_SUCCESFULLY_UPDATED.getAttributeName(), MessageManager.getProperty(MessageEnum.INFO_UNSUCCESSFUL_UPDATE, locale));
                throw new CommandException("LogicException in method execute() class UpdateInfoUserCommand", e);
            }
        } else {
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
        }
        String path = UrlManager.getUrl(UrlType.SIMPLE_PROFILE_USER)+login;
        return new Router(path, TransmisionType.FORWARD);
    }
}
