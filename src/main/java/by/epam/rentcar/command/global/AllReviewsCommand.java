package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.CommandParameter;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Review;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.ReviewLogic;
import by.epam.rentcar.logic.impl.ReviewLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.util.PageCreator;

import java.util.List;

/**
 * The Class AllReviewsCommand.
 */
public class AllReviewsCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        int maxPages;
        String typePage = (String) content.getRequestParameters(AttributeName.TYPE_PAGE.getAttributeName(), 0);
        String pageStr = (String) content.getRequestParameters(AttributeName.PAGE.getAttributeName(), 0);
        String typeTag = (String) content.getRequestParameters(AttributeName.TYPE_TAG.getAttributeName(), 0);
        PageCreator pageCreator= new PageCreator();
        Integer pageNumber;
        try {
            pageNumber = (pageStr == null ? 1 : Integer.parseInt(pageStr));
        } catch (NumberFormatException e) {
            pageNumber = 1;
        }
        ReviewLogic reviewLogic = new ReviewLogicImpl();
        int offset;

        try {
            List<Review> reviews;
            List<Review> reviewsMax;

            if (typePage == null) {
                reviewsMax = reviewLogic.findAllReviews();
                maxPages = pageCreator.maxNumberPage(reviewsMax.size(), CommandParameter.REVIEW_LIMIT);
                pageNumber = pageCreator.checkPageNumber(pageNumber, maxPages);
                offset = CommandParameter.REVIEW_LIMIT * (pageNumber - 1);

                reviews = reviewLogic.findPartReview(CommandParameter.REVIEW_LIMIT, offset);
                content.putRequestAttribute(AttributeName.TYPE_TAG.getAttributeName(), typeTag);
                content.putRequestAttribute(AttributeName.ID_CAR.getAttributeName(), 0);
            } else {
                Long idCar = Long.parseLong((String) content.getRequestParameters(AttributeName.ID_CAR.getAttributeName(), 0));
                reviewsMax = reviewLogic.findAllReviewByIdCar(idCar);
                maxPages = pageCreator.maxNumberPage(reviewsMax.size(), CommandParameter.REVIEW_LIMIT);
                pageNumber = pageCreator.checkPageNumber(pageNumber, maxPages);
                offset = CommandParameter.REVIEW_LIMIT * (pageNumber - 1);

                reviews = reviewLogic.findPartReviewByIdCar(CommandParameter.REVIEW_LIMIT, offset, idCar);
                content.putRequestAttribute(AttributeName.TYPE_TAG.getAttributeName(), typePage);
                content.putRequestAttribute(AttributeName.ID_CAR.getAttributeName(), idCar);
            }

            content.putRequestAttribute(AttributeName.ALL_REVIEWS_GLOBAL.getAttributeName(), reviews);
            content.putRequestAttribute(AttributeName.PAGE_NUMBER.getAttributeName(), pageNumber);
            content.putRequestAttribute(AttributeName.MAX_PAGE_NUMBER.getAttributeName(), maxPages);
        } catch (LogicException e) {
            throw new CommandException("LogicException in method execute() class AllReviewsCommand", e);
        }
        String path = UrlManager.getUrl(UrlType.REVIEWS);
        return new Router(path, TransmisionType.FORWARD);
    }

}

