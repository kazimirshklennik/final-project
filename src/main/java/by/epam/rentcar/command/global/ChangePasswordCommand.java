package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.MessageData;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.MessageLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.util.PasswordEncryption;
import by.epam.rentcar.validator.Validator;

import java.util.Formatter;

/**
 * The Class ChangePasswordCommand.
 */
public class ChangePasswordCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String locale =(String)content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login=(String)content.getSessionAttribute(AttributeName.LOGIN.getAttributeName());
        String currentPassword=(String)content.getRequestParameters(AttributeName.OLD_PASSWORD.getAttributeName(),0);
        String newPassword=(String)content.getRequestParameters(AttributeName.NEW_PASSWORD.getAttributeName(),0);
        MessageLogic messageLogic= new MessageLogicImpl();
        UserLogic userLogic= new UserLogicImpl();
        Validator validator= new Validator();

        if(validator.checkDataChangePassword(login,currentPassword,newPassword)) {
            try {
                if (userLogic.checkLoginAndPassword(login, new PasswordEncryption().create(currentPassword))) {
                    userLogic.changePassword(login, currentPassword, newPassword);
                    content.putRequestAttribute(AttributeName.SUCCESFULLY_UPDATED.getAttributeName(), MessageManager.getProperty(MessageEnum.INFO_SUCCESSFUL_UPDATE, locale));

                    Formatter formatter = new Formatter();
                    formatter.format(MessageData.FORMAT_MESSAGE, MessageData.LOGIN_TEXT, login, MessageData.NEW_PASSWORD_TEXT, newPassword);
                    messageLogic.sendNewPassword(login, formatter.toString());
                }else{
                    content.putRequestAttribute(AttributeName.INCORRECT_PASSWORD.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_PASSWORD, locale));
                }
            } catch (LogicException e) {
                content.putRequestAttribute(AttributeName.UN_SUCCESFULLY_UPDATED.getAttributeName(), MessageManager.getProperty(MessageEnum.INFO_UNSUCCESSFUL_UPDATE, locale));
                throw new CommandException("LogicException in method execute() class ChangePasswordCommand", e);
            }
        }else{
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
        }
        String path = UrlManager.getUrl(UrlType.PROFILE);
        return new Router(path, TransmisionType.FORWARD);
    }
}
