package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.CommandParameter;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.SocialNetworkLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.SocialNetworkLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.MessageEnum;
import by.epam.rentcar.manager.MessageManager;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.validator.Validator;

/**
 * The Class AddSocialNetworkCommand.
 */
public class AddSocialNetworkCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String locale=(String)content.getSessionAttribute(AttributeName.LOCALE.getAttributeName());
        String login = (String) content.getSessionAttribute(AttributeName.LOGIN.getAttributeName());
        String url=(String)content.getRequestParameters(AttributeName.URL_SOCIAL_NETWORK.getAttributeName(),0);
        Validator validator= new Validator();
        UserLogic userLogic= new UserLogicImpl();
        SocialNetworkLogic socialNetworkLogic= new SocialNetworkLogicImpl();
        url=url.trim();

       if(validator.checkDataSocialNetwork(url)) {
            try {
                User user = userLogic.findUser(login);
                if (user.getInfo().getSocialNetworks().size() <= CommandParameter.MAXIMUM_QUANTITY_OF_LINKS) {
                    socialNetworkLogic.add(socialNetworkLogic.create(user.getInfo().getIdUserInfo(), url));
                    content.putRequestAttribute(AttributeName.SUCCESSFULLY_ADDED.getAttributeName(), MessageManager.getProperty(MessageEnum.ADD_SUCESFULLY, locale));
                } else {
                    content.putRequestAttribute(AttributeName.ERROR_MAX_URL.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_MAX_URL, locale));
                }
            } catch (LogicException e) {
                content.putRequestAttribute(AttributeName.ERROR_ADD_SOCIAL_NETWORK.getAttributeName(), MessageManager.getProperty(MessageEnum.ERROR_ADDED, locale));
                throw new CommandException("LogicException in method execute() class AddSocialNetworkCommand", e);
            }
        }else{
            content.putRequestAttribute(AttributeName.INCORRECT_DATA.getAttributeName(), MessageManager.getProperty(MessageEnum.INCORRECT_DATA, locale));
        }
        String path = UrlManager.getUrl(UrlType.PROFILE_COMMAND);
        return new Router(path, TransmisionType.FORWARD);
    }
}
