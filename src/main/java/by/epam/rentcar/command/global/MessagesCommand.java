package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Message;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.MessageLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

import java.util.List;

/**
 * The Class MessagesCommand.
 */
public class MessagesCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        String idInterlocutorString = (String) content.getRequestParameters(AttributeName.ID_INTERLOCUTOR.getAttributeName(), 0);
        String idUserString = (String) content.getSessionAttribute(AttributeName.USER_ID.getAttributeName());
        String loginInterlocutor = (String) content.getRequestParameters(AttributeName.LOGIN_INTERLOCUTOR.getAttributeName(), 0);
        UserLogic userLogic = new UserLogicImpl();
        MessageLogic messageLogic = new MessageLogicImpl();

        if (idInterlocutorString != null) {
            try {
                Long idUser = Long.parseLong(idUserString);
                User interlocutor = userLogic.findUser(loginInterlocutor);
                Long idInterlocutor = Long.parseLong(idInterlocutorString);
                List<User> interlocutors = messageLogic.findAllUsersChat(idUser);
                List<Message> messages = messageLogic.findChat(idUser, idInterlocutor);

                content.putRequestAttribute(AttributeName.INTERLOCUTORS.getAttributeName(), interlocutors);
                content.putRequestAttribute(AttributeName.INTERLOCUTOR.getAttributeName(), interlocutor);
                content.putRequestAttribute(AttributeName.MESSAGES.getAttributeName(), messages);

            } catch (LogicException | NumberFormatException e) {
                throw new CommandException("LogicException in method execute() class MessagesCommand", e);
            }
        } else {
            List<User> interlocutors;
            try {
                Long idUser = Long.parseLong(idUserString);
                interlocutors = messageLogic.findAllUsersChat(idUser);
                content.putRequestAttribute(AttributeName.INTERLOCUTORS.getAttributeName(), interlocutors);
            } catch (LogicException | NumberFormatException e) {
                throw new CommandException("LogicException in method execute() class MessagesCommand", e);
            }
        }
        String path = UrlManager.getUrl(UrlType.MESSAGES);
        return new Router(path, TransmisionType.FORWARD);
    }
}
