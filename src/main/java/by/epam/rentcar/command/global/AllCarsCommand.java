package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.CommandParameter;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import by.epam.rentcar.util.PageCreator;

import java.util.List;

/**
 * The Class AllCarsCommand.
 */
public class AllCarsCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {

        int maxPages;
        int offset;
        List<Car> carsAll;
        CarLogic carLogic= new CarLogicImpl();
        PageCreator pageCreator= new PageCreator();
        List<Long> images;
        String page=(String)content.getRequestParameters(AttributeName.PAGE.getAttributeName(), 0);
        Integer pageNumber;
        try {
            pageNumber = (page == null ? 1 : Integer.parseInt(page));
        } catch (NumberFormatException e) {
            pageNumber = 1;
        }
        try {
            carsAll=carLogic.findAllCar();
            maxPages= pageCreator.maxNumberPage(carsAll.size(), CommandParameter.CAR_LIMIT);
            pageNumber=pageCreator.checkPageNumber(pageNumber,maxPages);

            offset=CommandParameter.CAR_LIMIT*(pageNumber-1);
            List<Car> carsPart=carLogic.findPartCars(CommandParameter.CAR_LIMIT,offset);

            for(Car car:carsPart){
                images=carLogic.findAllImagesCar(car.getIdCar());
                if(!images.isEmpty()) {
                    car.setIdGeneralPhoto(images.get(0));
                }
            }
            content.putRequestAttribute(AttributeName.ALL_CARS_GLOBAL.getAttributeName(),carsPart);
            content.putRequestAttribute(AttributeName.PAGE_NUMBER.getAttributeName(),  pageNumber);
            content.putRequestAttribute(AttributeName.MAX_PAGE_NUMBER.getAttributeName(), maxPages);

        } catch (LogicException e) {
            throw new CommandException("LogicException in method execute() class AllCarsCommand",e);
        }
        String path = UrlManager.getUrl(UrlType.CARS);
        return new Router(path, TransmisionType.FORWARD);
    }
}
