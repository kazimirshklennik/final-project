package by.epam.rentcar.command.global;

import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.LocaleConstant;
import by.epam.rentcar.controller.RequestContent;
import by.epam.rentcar.controller.Router;
import by.epam.rentcar.controller.TransmisionType;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;

/**
 * The Class LocaleCommand.
 */
public class LocaleCommand implements Command {

    /** (non-Javadoc)
     * @see by.epam.rentcar.command.Command#execute(RequestContent)
     */
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String commandLocale = (String)content.getRequestParameters(AttributeName.COMMAND.getAttributeName(),0);
        LocaleConstant locale = (commandLocale == null ? LocaleConstant.EN : LocaleConstant.valueOf(commandLocale.toUpperCase()));

        switch (locale) {
            case EN:
                content.setSessionAttribute(AttributeName.LOCALE.getAttributeName(), LocaleConstant.EN.getLocale());
                break;
            case RU:
                content.setSessionAttribute(AttributeName.LOCALE.getAttributeName(), LocaleConstant.RU.getLocale());
                break;
        }

        String path = UrlManager.getUrl(UrlType.HOME);
        return new Router(path, TransmisionType.REDIRECT);
    }
}

