package by.epam.rentcar.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * The type Sender email.
 */
public class SenderEmail {

    /**
     * The Username.
     */
    private String username;

    /**
     * The Password.
     */
    private String password;

    /**
     * The Props.
     */
    private Properties props;

    /**
     * The constant AUTH.
     */
    private static final String AUTH="mail.smtp.auth";

    /**
     * The constant HOST.
     */
    private static final String HOST="mail.smtp.host";

    /**
     * The constant HOST_NAME.
     */
    private static final String HOST_NAME="smtp.mail.ru";

    /**
     * The constant PORT.
     */
    private static final String PORT="mail.smtp.port";

    /**
     * The constant PORT_NUMBER.
     */
    private static final String PORT_NUMBER="587";

    /**
     * The constant STARTTIS.
     */
    private static final String STARTTIS="mail.smtp.starttls.enable";


    /**
     * Instantiates a new Sender email.
     *
     * @param username the username
     * @param password the password
     */
    public SenderEmail(String username, String password) {
        this.username = username;
        this.password = password;

        props = new Properties();
        props.put(AUTH, "true");
        props.put(STARTTIS, "true");
        props.put(HOST, HOST_NAME);
        props.put(PORT, PORT_NUMBER);
    }

    /**
     * Send.
     *
     * @param subject the subject
     * @param text    the text
     * @param toEmail the to email
     * @throws MessagingException the messaging exception
     */
    public void send(String subject, String text, String toEmail) throws MessagingException{
        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
    }
}
