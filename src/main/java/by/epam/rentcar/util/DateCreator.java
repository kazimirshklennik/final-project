package by.epam.rentcar.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * The type Date creator.
 */
public class DateCreator {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(DateCreator.class);

    /**
     * Instantiates a new Date creator.
     */
    public DateCreator() {
    }

    /**
     * Create string.
     *
     * @param dateLong the date long
     * @param type     the type
     * @return the string
     */
    public String create(@NotNull Long dateLong, DateType type) {
        String result = null;
        SimpleDateFormat dateFormat;

        switch (type) {
            case DATE_TIME:
                dateFormat = new SimpleDateFormat(DateType.DATE_TIME.getFormat());
                break;
            default:
                dateFormat = new SimpleDateFormat(DateType.DATE.getFormat());
        }
        if (dateLong != null) {
            Date date = new Date(dateLong);
            result = dateFormat.format(date);
        }
        return result;
    }

    /**
     * Create long long.
     *
     * @param dateTime the date time
     * @return the long
     */
    public Long createLong(String dateTime) {
        Date date = new Date();

        DateFormat format = new SimpleDateFormat(DateType.DATE.getFormat());
        try {
            date = format.parse(dateTime);
        } catch (ParseException e) {
            LOGGER.warn("ParseException in method createLong(String dateTime)", e);
        }
        return date.getTime();
    }

    /**
     * Gets age.
     *
     * @param date the date
     * @return the age
     */
    public int getAge(String date) {
        Date birthday = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateType.DATE.getFormat());
        int resilt=0;
        try {
            birthday = dateFormat.parse(date);
        } catch (ParseException e) {
            LOGGER.warn("ParseException in method createLong(String dateTime)", e);
        }
        GregorianCalendar today = new GregorianCalendar();
        GregorianCalendar bday = new GregorianCalendar();
        GregorianCalendar bdayThisYear = new GregorianCalendar();
        bday.setTime(birthday);
        bdayThisYear.setTime(birthday);
        bdayThisYear.set(Calendar.YEAR, today.get(Calendar.YEAR));

        resilt = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);

        if (today.getTimeInMillis() < bdayThisYear.getTimeInMillis())
            resilt--;

        return resilt;
    }

    /**
     * Get day int.
     *
     * @param dateBooking the date booking
     * @param dateReturn  the date return
     * @return the int
     */
    public int getDay(@NotNull String dateBooking, @NotNull String dateReturn){

        Date booking = new Date();
        Date dReturn = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat(DateType.DATE.getFormat());

        int resilt=0;
        try {
            booking = dateFormat.parse(dateBooking);
            dReturn=dateFormat.parse(dateReturn);

        } catch (ParseException e) {
            LOGGER.warn("ParseException in method createLong(String dateTime)", e);
        }
        GregorianCalendar gcReturn = new GregorianCalendar();
        GregorianCalendar gcBooking = new GregorianCalendar();
        GregorianCalendar returnThisDay = new GregorianCalendar();
        gcBooking.setTime(booking );
        gcReturn.setTime(dReturn);
        returnThisDay.setTime(dReturn);
        returnThisDay.set(Calendar.DAY_OF_YEAR, gcReturn.get(Calendar.DAY_OF_YEAR));

        resilt = gcReturn.get(Calendar.DAY_OF_YEAR) - gcBooking.get(Calendar.DAY_OF_YEAR);
        if (gcReturn.getTimeInMillis() < returnThisDay.getTimeInMillis())
            resilt--;

        return resilt;
    }
}


