package by.epam.rentcar.util;

/**
 * The type Page creator.
 */
public class PageCreator {


    /**
     * Max number page int.
     *
     * @param size  the size
     * @param limit the limit
     * @return the int
     */
    public int maxNumberPage(int size, int limit){
        int maxPages=size/limit;
        maxPages=maxPages<(size/(float)limit)?maxPages+=1:maxPages;
        return maxPages;
    }

    /**
     * Check page number int.
     *
     * @param pageNumber the page number
     * @param maxPages   the max pages
     * @return the int
     */
    public int checkPageNumber(int pageNumber, int maxPages){
        pageNumber=(pageNumber>maxPages)?maxPages:pageNumber;
        pageNumber=(pageNumber<=0)?1:pageNumber;
        return pageNumber;
    }
}
