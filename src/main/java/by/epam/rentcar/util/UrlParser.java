package by.epam.rentcar.util;

import by.epam.rentcar.controller.Controller;
import by.epam.rentcar.entity.social.SocialNotworkType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Url parser.
 */
public class UrlParser {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(UrlParser.class);

    /**
     * The constant GOOGLE_REGEX.
     */
    private static final String GOOGLE_REGEX = "google";

    /**
     * The constant FASEBOOK_REGEX.
     */
    private static final String FASEBOOK_REGEX = "facebook";

    /**
     * The constant TWITTER_REGEX.
     */
    private static final String TWITTER_REGEX = "twitter";

    /**
     * The constant INSTAGRAM_REGEX.
     */
    private static final String INSTAGRAM_REGEX = "instagram";

    /**
     * The constant VK_REGEX.
     */
    private static final String VK_REGEX = "vk.com";

    /**
     * The constant SOCIAL_NETWORK_REGEX.
     */
    private static final List<String> SOCIAL_NETWORK_REGEX;

    static {
        SOCIAL_NETWORK_REGEX = new ArrayList<>();
        SOCIAL_NETWORK_REGEX.add(GOOGLE_REGEX);
        SOCIAL_NETWORK_REGEX.add(FASEBOOK_REGEX);
        SOCIAL_NETWORK_REGEX.add(TWITTER_REGEX);
        SOCIAL_NETWORK_REGEX.add(INSTAGRAM_REGEX);
        SOCIAL_NETWORK_REGEX.add(VK_REGEX);
    }

    /**
     * Parse social network social notwork type.
     *
     * @param url the url
     * @return the social notwork type
     */
    public SocialNotworkType parseSocialNetwork(String url) {

        SocialNotworkType socialNotworkTyp = SocialNotworkType.UNKNOWN;
        if (url != null) {
                for (String regex : SOCIAL_NETWORK_REGEX) {
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(url);
                    if (matcher.find()) {
                        try {
                            socialNotworkTyp = SocialNotworkType.valueOf(regex.replace(".", "_").toUpperCase());
                        } catch (IllegalArgumentException e) {
                            LOGGER.warn("IllegalArgumentException in a parseSocialNetworks, unknown url - "+url+" ", e);
                        }
                    }
                }

        }
        return socialNotworkTyp;
    }
}
