package by.epam.rentcar.util;

/**
 * The enum Date type.
 */
public enum DateType {
    /**
     * Date time date type.
     */
    DATE_TIME("yyyy-MM-dd / HH:mm"),
    /**
     * Date date type.
     */
    DATE("yyyy-MM-dd");

    /**
     * The Format.
     */
    private String format;

    /**
     * Instantiates a new Date type.
     *
     * @param format the format
     */
    DateType(String format){
        this.format=format;
    }

    /**
     * Get format string.
     *
     * @return the string
     */
    public String getFormat(){
        return format;
    }
}
