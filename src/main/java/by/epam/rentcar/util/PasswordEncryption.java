package by.epam.rentcar.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The type Password encryption.
 */
public class PasswordEncryption {


    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER= LogManager.getLogger(PasswordEncryption.class);

    /**
     * The constant ONE.
     */
    private static final String ONE="one";

    /**
     * The constant SHA.
     */
    private static  final String SHA_1="SHA-1";

    /**
     * Instantiates a new Password encryption.
     */
    public  PasswordEncryption(){}


    /**
     * Create string.
     *
     * @param encrypted the encrypted
     * @return the string
     */
    public String create(String encrypted) {
        String result = "";
        if (!encrypted.isEmpty()) {
            MessageDigest messageDigest;
            byte[] bytesEncoded;
            BigInteger bigInteger;
            try {
                messageDigest = MessageDigest.getInstance(SHA_1);
                messageDigest.update(encrypted.getBytes(StandardCharsets.UTF_8));
                bytesEncoded = messageDigest.digest();
            } catch (NoSuchAlgorithmException e) {
                LOGGER.warn(e);
                throw new RuntimeException(e);
            }
            if (bytesEncoded != null) {
                bigInteger = new BigInteger(1, bytesEncoded);
                result = bigInteger.toString(16);
            }
        } else {
            result = ONE;
        }
        return result;
    }
}
