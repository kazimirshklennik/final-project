package by.epam.rentcar.util;

import java.util.Random;

/**
 * The type Password generator.
 */
public class PasswordGenerator {

    /**
     * Instantiates a new Password generator.
     */
    public PasswordGenerator() {}

    /**
     * Generate string.
     *
     * @param length the length
     * @return the string
     */
    public String generate(int length){
        Random r = new Random();
        String result = r.ints(48, 122)
                .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
                .mapToObj(i -> (char) i)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();

        return result;
    }
}
