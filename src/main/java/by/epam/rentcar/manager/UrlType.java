package by.epam.rentcar.manager;

/**
 * The enum Url type.
 */
public enum UrlType {
    /**
     * Simple login url type.
     */
    SIMPLE_LOGIN("page.simple.login"),
    /**
     * Index url type.
     */
    INDEX("page.index"),
    /**
     * Index command url type.
     */
    INDEX_COMMAND("page.index.command"),

    /**
     * None profile url type.
     */
    NONE_PROFILE("page.none.profile"),
    /**
     * Profile url type.
     */
    PROFILE("page.profile"),
    /**
     * Default url type.
     */
    DEFAULT("page.default"),
    /**
     * Register url type.
     */
    REGISTER("page.register"),
    /**
     * Index after register url type.
     */
    INDEX_AFTER_REGISTER("page.indexAfterRegister"),
    /**
     * User profile url type.
     */
    USER_PROFILE("page.user.profile"),
    /**
     * Profile command url type.
     */
    PROFILE_COMMAND("page.profile.command"),
    /**
     * Car info url type.
     */
    CAR_INFO("page.car.info"),
    /**
     * Car info command url type.
     */
    CAR_INFO_COMMAND("page.car.info.command"),
    /**
     * Error url type.
     */
    ERROR("page.error"),
    /**
     * Error blocking url type.
     */
    ERROR_BLOCKING("page.error.blocking"),
    /**
     * Error profile url type.
     */
    ERROR_PROFILE("page.error.profile"),
    /**
     * Error car info url type.
     */
    ERROR_CAR_INFO("page.error.car"),
    /**
     * Reviews url type.
     */
    REVIEWS("page.reviews"),
    /**
     * Cars url type.
     */
    CARS("page.all.cars"),
    /**
     * Messages url type.
     */
    MESSAGES("page.messages"),
    /**
     * Errors url type.
     */
    ERRORS("page.errors"),
    /**
     * Simple cars page url type.
     */
    SIMPLE_CARS_PAGE("page.simple.cars.page"),
    /**
     * Simple cars url type.
     */
    SIMPLE_CARS("page.simple.cars"),
    /**
     * Simple car info url type.
     */
    SIMPLE_CAR_INFO("page.simple.car.info"),
    /**
     * Simple profile user url type.
     */
    SIMPLE_PROFILE_USER("page.simple.profile.user"),
    /**
     * Simple reviews url type.
     */
    SIMPLE_REVIEWS("page.simple.reviews"),
    /**
     * Simple reviews page url type.
     */
    SIMPLE_REVIEWS_PAGE("page.simple.reviews.page"),
    /**
     * Simple reviews car url type.
     */
    SIMPLE_REVIEWS_CAR("page.simple.reviews.car"),
    /**
     * Simple messages url type.
     */
    SIMPLE_MESSAGES("page.simple.messages"),
    /**
     * Home url type.
     */
    HOME("page.home");

    private String type;

    UrlType(String type) {
        this.type = type;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }
}


