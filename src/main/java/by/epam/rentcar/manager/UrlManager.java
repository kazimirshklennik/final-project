package by.epam.rentcar.manager;


import java.util.ResourceBundle;

/**
 * The type Url manager.
 */
public class UrlManager {


    /**
     * The Constant SESOURSE_FILE.
     */
    private static final String SESOURSE_FILE = "url";

    /**
     * The Constant resourceBundle.
     */
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle(SESOURSE_FILE);


    /**
     * Instantiates a new url manager.
     */
    private UrlManager() {
    }

    /**
     * Gets the property.
     *
     * @param key the key
     * @return the property
     */
    public static String getUrl(UrlType key) {

        String value;
        if (key != null) {
            value = resourceBundle.getString(key.getType());
        } else {
            value = resourceBundle.getString(UrlType.DEFAULT.getType());
        }
        return value;
    }
}
