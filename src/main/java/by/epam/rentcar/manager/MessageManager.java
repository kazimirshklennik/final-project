package by.epam.rentcar.manager;

import by.epam.rentcar.command.constant.LocaleConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The type Message manager.
 */
public class MessageManager {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(MessageManager.class);

    /**
     * The Constant SESOURSE_FILE.
     */
    private static final String SESOURSE_FILE="message.messages";

    /**
     * The Constant resourceBundleRuBy.
     */
    private static final ResourceBundle resourceBundleRuBy = ResourceBundle.getBundle(SESOURSE_FILE,
            new Locale("ru", "BY"));

    /**
     * The Constant resourceBundleEnGb.
     */
    private static final ResourceBundle resourceBundleEnGb = ResourceBundle.getBundle(SESOURSE_FILE, Locale.UK);

    /**
     * Instantiates a new message manager.
     */
    private MessageManager() {
    }

    /**
     * Gets the property.
     *
     * @param key        the key
     * @param localeType the locale type
     * @return the property
     */
    public static String getProperty(MessageEnum key, String localeType) {

        String value = null;

        if (localeType == null) {
            localeType = LocaleConstant.EN.getLocale();
        }

        switch (LocaleConstant.valueOf(localeType.toUpperCase())) {
            case RU:
                value = resourceBundleRuBy.getString(key.getMessage());
                break;

            case EN:
                value = resourceBundleEnGb.getString(key.getMessage());
                break;
            default:
                LOGGER.info("Unknown locale: " + localeType);
        }
        return value;
    }
}


