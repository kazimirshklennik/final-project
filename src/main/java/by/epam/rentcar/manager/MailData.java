package by.epam.rentcar.manager;

/**
 * The enum Mail data.
 */
public enum MailData {

    /**
     * Path mail data.
     */
    PATH("mail"),

    /**
     * Key login mail data.
     */
    KEY_LOGIN("mail.admin.login"),

    /**
     * Key password mail data.
     */
    KEY_PASSWORD("mail.admin.password");

    private String property;

    MailData(String property){
        this.property=property;
    }

    /**
     * Gets key.
     *
     * @return the key
     */
    public String getKey() {
        return property;
    }
}
