package by.epam.rentcar.manager;

import java.util.ResourceBundle;

/**
 * The type Mail manager.
 */
public class MailManager {

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle(MailData.PATH.getKey());

    /**
     * Get data string.
     *
     * @param key the key
     * @return the string
     */
    public String getData(String key){
        return resourceBundle.getString(key);
    }
}
