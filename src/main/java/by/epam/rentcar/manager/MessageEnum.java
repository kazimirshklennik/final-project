package by.epam.rentcar.manager;

/**
 * The Enum MessageEnum.
 */
public enum MessageEnum {

    /**
     * The info successful update.
     */
    INFO_SUCCESSFUL_UPDATE("info.successful.update"),

    /**
     * The info successful delete.
     */
    INFO_SUCCESSFULLY_DELETE("info.successful.delete"),

    /**
     * The info unsuccessful delete.
     */
    INFO_UN_SUCCESSFULLY_DELETE("info.unsuccessful.delete"),

    /**
     * The info successful create.
     */
    INFO_SUCCESSFUL_CREATE("info.successful.create"),

    /**
     * The info unsuccessful update.
     */
    INFO_UNSUCCESSFUL_UPDATE("info.unsuccessful.update"),

    /**
     * Incorrect password message enum.
     */
    INCORRECT_PASSWORD("error.incorrect.password"),

    /**
     * The info successful find.
     */
    INFO_SUCCESSFUL_FIND("info.successful.find"),

    /**
     * The server error.
     */
    SERVER_ERROR("server.error"),

    /**
     * The error wrong data.
     */
    ERROR_WRONG_DATA("error.wrong.data"),

    /**
     * The server error null.
     */
    SERVER_ERROR_NULL("server.error.null"),

    /**
     * The error incorrect password or email.
     */
    ERROR_INCORRECT_PASSWORD_OR_LOGIN("error.incorrect.password.or.login"),

    /**
     * The error no item found.
     */
    ERROR_NO_ITEM_FOUND("error.no.item.found"),

    /**
     * The info incorrect id.
     */
    INFO_INCORRECT_ID("info.successful.login"),

    /**
     * The info successful login.
     */
    INFO_SUCCESSFUL_LOGIN("info.successful.login"),

    /**
     * The info already login.
     */
    INFO_ALREADY_LOGIN("info.already.login"),

    /**
     * The error access denied.
     */
    ERROR_ACCESS_DENIED("error.access.denied"),

    /**
     * The info successful logout.
     */
    INFO_SUCCESSFUL_LOGOUT("info.successful.logout"),

    /**
     * The info already logout.
     */
    INFO_ALREADY_LOGOUT("info.already.logout"),

    /**
     * The info english locale.
     */
    INFO_ENGLISH_LOCALE("info.english.locale"),

    /**
     * The info russian locale.
     */
    INFO_RUSSIAN_LOCALE("info.russian.locale"),

    /**
     * The info error locale.
     */
    INFO_ERROR_LOCALE("info.error.locale"),

    /**
     * The role admin.
     */
    ROLE_ADMIN("role.admin"),

    /**
     * The role manager.
     */
    ROLE_USER("role.user"),

    /**
     * The role customer.
     */
    ROLE_UNKNOWN("role.unknowm"),

    /**
     * The error wrong email.
     */
    ERROR_WRONG_EMAIL("error.wrong.email"),

    /**
     * The error change role.
     */
    ERROR_CHANGE_ROLE("error.role.change"),

    /**
     * The error change null page.
     */
    ERROR_NULL_PAGE("path.page.err"),

    /**
     * Message error null page message enum.
     */
    MESSAGE_ERROR_NULL_PAGE("message.nullpage"),

    /**
     * Error exist login message enum.
     */
    ERROR_EXIST_LOGIN("error.login.exist"),

    /**
     * Error exist mail message enum.
     */
    ERROR_EXIST_MAIL("error.mail.exist"),

    /**
     * Invalid command message enum.
     */
    INVALID_COMMAND("invalid.command"),

    /**
     * Error send message message enum.
     */
    ERROR_SEND_MESSAGE("error.send.message"),

    /**
     * Delete reserve message enum.
     */
    DELETE_RESERVE("delete.reserve"),

    /**
     * Delete social network message enum.
     */
    DELETE_SOCIAL_NETWORK("delete.social.network"),

    /**
     * User does not exist message enum.
     */
    USER_DOES_NOT_EXIST("error.user.does.not.exist"),

    /**
     * Message send successfully message enum.
     */
    MESSAGE_SEND_SUCCESSFULLY("info.sent.successfully"),

    /**
     * Updated successfully message enum.
     */
    UPDATED_SUCCESSFULLY("info.successful.update"),

    /**
     * Error update message enum.
     */
    ERROR_UPDATE("info.unsuccessful.update"),

    /**
     * Registration successfully message enum.
     */
    REGISTRATION_SUCCESSFULLY("info.registration.succesfully"),

    /**
     * Error added message enum.
     */
    ERROR_ADDED("info.unsuccessful.add"),

    /**
     * Incorrect data message enum.
     */
    INCORRECT_DATA("error.incorrect.data"),

    /**
     * Add sucesfully message enum.
     */
    ADD_SUCESFULLY("info.successful.add"),

    /**
     * Error max url message enum.
     */
    ERROR_MAX_URL("error.max.url"),

    /**
     * Error message enum.
     */
    ERROR("error.log"),

    /**
     * Error date reserve message enum.
     */
    ERROR_DATE_RESERVE("error.date.reserve"),

    /**
     * Reserve car successfully message enum.
     */
    RESERVE_CAR_SUCCESSFULLY("page.reserve.car.succesfully"),

    /**
     * Error blocking status message enum.
     */
    ERROR_BLOCKING_STATUS("page.blocking.status"),

    /**
     * Error active reserve message enum.
     */
    ERROR_ACTIVE_RESERVE("error.active.reserve");




    /** The message. */
    private final String message;

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Instantiates a new message enum.
     *
     * @param message
     *            the message
     */
    MessageEnum(String message) {
        this.message = message;
    }

    /**
     * From string.
     *
     * @param message the message
     * @return the message enum
     */
    public static MessageEnum fromString(String message) {
        MessageEnum value = null;
        for (MessageEnum messageEnum : MessageEnum.values()) {
            if (message.equalsIgnoreCase(messageEnum.message)) {
                value = messageEnum;
            }
        }
        return value;
    }
}