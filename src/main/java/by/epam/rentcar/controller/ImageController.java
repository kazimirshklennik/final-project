package by.epam.rentcar.controller;

import by.epam.rentcar.command.ActionCommand;
import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * The Class ImageController.
 */
@WebServlet("/image")
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024, maxRequestSize = 1024 * 1024) //1 MB
public class ImageController extends Controller {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LogManager.getLogger(ImageController.class);

    /** The Constant CONTENT_TYPE. */
    private static final String CONTENT_TYPE = "image/jpeg";

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, new RequestContent(request));
    }

    /**
     * Process request.
     *
     * @param request the request
     * @param response the response
     * @throws ServletException the servlet exception
     * @throws IOException  Signals that an I/O exception has occurred.
     * @throws CommandException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        RequestContent content = new RequestContent(request);
        byte []imageBytes;
        try {
            Command optionalCommand = ActionCommand.getInstance().define(content);
            InputStream inputStream=optionalCommand.load(content);
            imageBytes = new byte[inputStream.available()];
            inputStream.read(imageBytes);

            response.setContentType(CONTENT_TYPE);
            response.setContentLength(imageBytes.length);
            response.getOutputStream().write(imageBytes);
        } catch (CommandException e) {
            LOGGER.warn("CommandException in a Controller class, command does not exist", e);
            request.setAttribute(AttributeName.ERROR.getAttributeName(),e);
            request.getRequestDispatcher(UrlManager.getUrl(UrlType.INDEX)).forward(request, response);
        }
    }
}

