package by.epam.rentcar.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class RequestContent.
 */
public class RequestContent {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(RequestContent.class);

    /**
     * The request attributes.
     */
    private Map<String, Object> requestAttributes = new HashMap<>();

    /**
     * The request parameters.
     */
    private Map<String, String[]> requestParameters = new HashMap<>();

    /**
     * The session attributes.
     */
    private Map<String, Object> sessionAttributes = new HashMap<>();

    /**
     * The parts.
     */
    private Map<String, Part> parts = new HashMap<>();

    /**
     * The invalidateSession.
     */
    private boolean invalidateSession;

    /**
     * Instantiates a new Request Content.
     */
    public RequestContent(HttpServletRequest request) {
        extractValues(request);
    }

    /**
     * extract Values
     *
     * @param request the request
     * @throws ServletException
     * @throws IOException
     * @throws IllegalStateException
     */
    public void extractValues(HttpServletRequest request) {

        Enumeration<String> attrNames = request.getAttributeNames();

        while (attrNames.hasMoreElements()) {
            String name = attrNames.nextElement();
            Object attr = request.getAttribute(name);
            requestAttributes.put(name, attr);
        }

        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement();
            String[] param = request.getParameterValues(name);
            requestParameters.put(name, param);
        }
        Enumeration<String> sessionAttrNames = request.getSession().getAttributeNames();
        while (sessionAttrNames.hasMoreElements()) {
            String name = sessionAttrNames.nextElement();
            Object sessionAttr = request.getSession().getAttribute(name);
            sessionAttributes.put(name, sessionAttr);
        }
        Collection<Part> parts;
        try {
            parts = request.getParts();
            for (Part part : parts) {
                String key = part.getName();
                this.parts.put(key, part);
            }
        } catch (IOException e) {
            LOGGER.warn("IOException in a RequestContent class", e);
        } catch (ServletException e) {
            LOGGER.warn("ServletException in a RequestContent class", e);
        } catch (IllegalStateException e) {
            LOGGER.info("IllegalStateException in a RequestContent class", e);
        }

    }

    /**
     * insert Attributes
     * (non-Javadoc)
     *
     * @param request the request
     */
    public void insertAttributes(HttpServletRequest request) {
        if (invalidateSession) {
            sessionAttributes.clear();
            request.getSession(false).invalidate();
        } else {
            for (Map.Entry<String, Object> entry : requestAttributes.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                request.setAttribute(key, value);
            }
            if (request.getSession(false) != null) {
                for (Map.Entry<String, Object> entry : sessionAttributes.entrySet()) {
                    String k = entry.getKey();
                    Object v = entry.getValue();
                    request.getSession().setAttribute(k, v);
                }
            }
        }
    }


    /**
     * put Request Attribute
     *
     * @param name the name
     * @param attr the attr
     */
    public void putRequestAttribute(String name, Object attr) {
        requestAttributes.put(name, attr);
    }

    /**
     * get Request Parameters.
     *
     * @param name  the name
     * @param index the index
     * @return the Object
     */
    public Object getRequestParameters(String name, int index) {
        return requestParameters.get(name) == null ? null : requestParameters.get(name)[index];
    }

    /**
     * get Session Attribute.
     *
     * @param name the name
     * @return the Object
     */
    public Object getSessionAttribute(String name) {
        return sessionAttributes.get(name);
    }

    /**
     * set Session Attribute.
     *
     * @param key   the key
     * @param value the value
     */
    public void setSessionAttribute(String key, Object value) {
        sessionAttributes.put(key, value);
    }

    /**
     * invalidate Session
     */
    public void invalidateSession() {
        this.invalidateSession = true;
    }

    /**
     * get InputStream.
     *
     * @param name the name
     * @return the InputStream
     */
    public InputStream getInputStream(String name) {
        InputStream inputStream = null;
        try {
            if (parts.get(name).getSize() > 0) {
                inputStream = parts.get(name).getInputStream();
            }
        } catch (IOException e) {
            LOGGER.error("IOException in a RequestContent class", e);
        }
        return inputStream;
    }
}