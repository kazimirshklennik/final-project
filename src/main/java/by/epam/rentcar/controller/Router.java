package by.epam.rentcar.controller;

/**
 * The Class Router.
 */
public class Router {

    /**
     * The path.
     */
    private String path;

    /**
     * The transmision Type.
     */
    private TransmisionType transmisionType;

    /**
     * Router.
     *
     * @param path            path
     * @param transmisionType transmisionType
     * @return the Router
     */
    public Router(String path, TransmisionType transmisionType) {
        this.path = path;
        this.transmisionType = transmisionType;
    }

    /**
     * get Path.
     *
     * @return the String
     */
    public String getPath() {
        return path;
    }

    /**
     * get Transmision Type.
     *
     * @return the TransmisionType
     */
    public TransmisionType getTransmisionType() {
        return transmisionType;
    }
}

