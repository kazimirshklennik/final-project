package by.epam.rentcar.controller;

import by.epam.rentcar.command.ActionCommand;
import by.epam.rentcar.command.Command;
import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.exception.CommandException;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Class Controller.
 */
@WebServlet(name = "do", urlPatterns = "/do")
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024, maxRequestSize = 1024 * 1024) //1 MB
public class Controller extends HttpServlet {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, new RequestContent(request));
    }

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     * */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, new RequestContent(request));
    }

    /**
     * Process request.
     *
     * @param request  the request
     * @param response the response
     * @param content  the content
     * @throws ServletException the servlet exception
     * @throws IOException      Signals that an I/O exception has occurred.
     */
    void processRequest(HttpServletRequest request, HttpServletResponse response, RequestContent content) throws ServletException, IOException {

        try {
            Command command = ActionCommand.getInstance().define(content);
            if (command != null) {
                Router router = command.execute(content);
                content.insertAttributes(request);
                if (router.getTransmisionType().equals(TransmisionType.FORWARD)) {
                    request.getRequestDispatcher(router.getPath()).forward(request, response);
                } else {
                    response.sendRedirect(request.getContextPath() + router.getPath());
                }
            } else {
                request.getRequestDispatcher(UrlManager.getUrl(UrlType.INDEX)).forward(request, response);
            }

        } catch (CommandException e) {
            LOGGER.error("CommandException in a Controller class, command does not exist", e);
            request.setAttribute(AttributeName.ERROR.getAttributeName(), e);
            request.getRequestDispatcher(UrlManager.getUrl(UrlType.INDEX)).forward(request, response);
        }
    }
}


