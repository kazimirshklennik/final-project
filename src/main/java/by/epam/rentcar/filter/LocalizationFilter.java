package by.epam.rentcar.filter;

import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.command.constant.LocaleConstant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type Localization filter.
 */
@WebFilter(filterName = "LocalizationFilter", urlPatterns = "/*")
public class LocalizationFilter implements Filter {

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
     * javax.servlet.ServletResponse, javax.servlet.FilterChain)
     **/
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();

        if (session.getAttribute(AttributeName.LOCALE.getAttributeName()) == null) {
            session.setAttribute(AttributeName.LOCALE.getAttributeName(), LocaleConstant.EN.getLocale());
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}

