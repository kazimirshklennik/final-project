package by.epam.rentcar.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * The Class EncodingFilter.
 */
@WebFilter(urlPatterns = { "/*" })
public class EncodingFilter implements Filter {

	/** The Constant UTF_8. */
	private static final String UTF_8 = "UTF-8";

	/**
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 **/
	public void init(FilterConfig config) throws ServletException {

	}

	/**
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 **/
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		request.setCharacterEncoding(UTF_8);
		response.setCharacterEncoding(UTF_8);
		filterChain.doFilter(request, response);
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#destroy()
	 **/
	public void destroy() {
	}
}