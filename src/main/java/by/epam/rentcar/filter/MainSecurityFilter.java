package by.epam.rentcar.filter;

import by.epam.rentcar.command.constant.AttributeName;
import by.epam.rentcar.entity.user.Role;
import by.epam.rentcar.manager.UrlManager;
import by.epam.rentcar.manager.UrlType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static by.epam.rentcar.command.CommandType.*;

/**
 * The type security filter.
 */
@WebFilter(urlPatterns = {"/*"})
public class MainSecurityFilter extends GenericFilter {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(MainSecurityFilter.class);

    /**
     * The Constant UNKNOWN_COMMAND.
     */
    private static final String UNKNOWN_COMMAND = "Exception in a filter, command does not exist";

    /**
     * Allowed commands for GUEST
     */
    private static final Set<String> ALLOWED_GUEST_PATHS = Collections.unmodifiableSet(new HashSet<>(
            Arrays.asList(REGISTER.name(), EN.name(), RU.name(), TOLOGIN.name(), TO_REVIEW.name(),
                    LOGIN.name(), TO_ALL_CARS.name(), INFO_CAR.name(), PROFILE.name(),
                    CAR_RESERVATION.name(), TO_HOME.name(), IMAGE_USER.name(), IMAGE_CAR.name(), CARS_PAGE.name(),
                    REVIEWS_PAGE.name(), PAGE_INFO_CAR.name(), SIMPLE_PROFILE.name(), INDEX.name())));

    /**
     * Allowed commands for USER
     */
    private static final Set<String> ALLOWED_USER_PATHS = new HashSet<>(
            Arrays.asList(REGISTER.name(), EN.name(), RU.name(), TOLOGIN.name(), TO_REVIEW.name(),
                    LOGIN.name(), TO_ALL_CARS.name(), LOGOUT.name(), PROFILE.name(), SEND_MESSAGE.name(),
                    SEND_MAIL.name(), UPDATE_INFO.name(), UPDATE_PASSWORD.name(), INFO_CAR.name(),
                    ADD_REVIEW.name(), CAR_RESERVATION.name(), TO_REVIEW.name(), TO_ALL_CARS.name(), MESSAGES.name(),
                    DELETE_RESERVE.name(), ADD_SOCIAL.name(), DELETE_SOCIAL.name(), IMAGE_USER.name(), TO_HOME.name(),
                    IMAGE_USER.name(), IMAGE_CAR.name(), SIMPLE_PROFILE.name(), SIMPLE_MESSAGES.name(), CARS_PAGE.name(),
                    REVIEWS_PAGE.name(), PAGE_INFO_CAR.name(), INDEX.name()));

    /**
     * Allowed commands for ADMIN
     */
    private static final Set<String> ALLOWED_ADMIN_PATHS = new HashSet<>(
            Arrays.asList(REGISTER.name(), EN.name(), RU.name(), TOLOGIN.name(), TO_REVIEW.name(),
                    LOGIN.name(), TO_ALL_CARS.name(), LOGOUT.name(), PROFILE.name(), SEND_MESSAGE.name(),
                    SEND_MAIL.name(), UPDATE_INFO.name(), UPDATE_PASSWORD.name(), INFO_CAR.name(),
                    ADD_REVIEW.name(), CAR_RESERVATION.name(), TO_REVIEW.name(), TO_ALL_CARS.name(), MESSAGES.name(),
                    DELETE_RESERVE.name(), DELETE_CAR.name(), UPDATE_CAR_INFO.name(), DELETE_USER.name(), UNLOCK.name(),
                    LOCK.name(), ADD_CAR.name(), ADD_SOCIAL.name(), DELETE_SOCIAL.name(), IMAGE_CAR.name(), DELETE_IMAGE_CAR.name(),
                    IMAGE_USER.name(), TO_HOME.name(), DELETE_REVIEW.name(), SIMPLE_PROFILE.name(), SIMPLE_MESSAGES.name(), CARS_PAGE.name(),
                    REVIEWS_PAGE.name(), PAGE_INFO_CAR.name(), INDEX.name()));

    /**
     * (non-Javadoc)
     *
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
     * javax.servlet.ServletResponse, javax.servlet.FilterChain)
     **/
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpServletRequest.getSession();
        String type = (String) session.getAttribute(AttributeName.USER_ROLE.getAttributeName());
        String command = httpServletRequest.getParameter(AttributeName.COMMAND.getAttributeName());
        Role role = (type == null ? Role.UNKNOWN : Role.valueOf(type.toUpperCase()));
        try {
            if (command != null) {
                command = command.replace("-", "_").toUpperCase();
                switch (role) {
                    case ADMIN:
                        if (ALLOWED_ADMIN_PATHS.contains(command)) {
                            filterChain.doFilter(httpServletRequest, httpServletResponse);
                        } else {
                            LOGGER.warn(UNKNOWN_COMMAND);
                            httpServletRequest.setAttribute(AttributeName.ERROR.getAttributeName(), UNKNOWN_COMMAND);
                            httpServletRequest.getRequestDispatcher(UrlManager.getUrl(UrlType.ERRORS)).forward(httpServletRequest, httpServletResponse);
                        }
                        break;

                    case USER:
                        if (ALLOWED_USER_PATHS.contains(command)) {
                            filterChain.doFilter(httpServletRequest, httpServletResponse);
                        } else {
                            LOGGER.warn(UNKNOWN_COMMAND);
                            httpServletRequest.setAttribute(AttributeName.ERROR.getAttributeName(), UNKNOWN_COMMAND);
                            httpServletRequest.getRequestDispatcher(UrlManager.getUrl(UrlType.ERRORS)).forward(httpServletRequest, httpServletResponse);
                        }
                        break;

                    case UNKNOWN:
                        if (ALLOWED_GUEST_PATHS.contains(command)) {
                            filterChain.doFilter(httpServletRequest, httpServletResponse);
                        } else {
                            LOGGER.warn(UNKNOWN_COMMAND);
                            httpServletRequest.setAttribute(AttributeName.ERROR.getAttributeName(), UNKNOWN_COMMAND);
                            httpServletRequest.getRequestDispatcher(UrlManager.getUrl(UrlType.ERRORS)).forward(httpServletRequest, httpServletResponse);
                        }
                        break;
                }
            } else {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }

        } catch (IllegalArgumentException e) {
            LOGGER.warn(e);
            httpServletRequest.setAttribute(AttributeName.ERROR.getAttributeName(), e);
            httpServletRequest.getRequestDispatcher(UrlManager.getUrl(UrlType.ERRORS)).forward(httpServletRequest, httpServletResponse);
        }
    }
}

