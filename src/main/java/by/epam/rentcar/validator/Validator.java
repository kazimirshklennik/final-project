package by.epam.rentcar.validator;

/**
 * The type Validator.
 */
public class Validator {

    /**
     * The constant MAX_SIZE_PASSWORD.
     */
    private static final int MAX_SIZE_PASSWORD = 16;

    /**
     * The constant MAX_SIZE_NAME.
     */
    private static final int MAX_SIZE_NAME = 20;

    /**
     * The constant MAX_SIZE_INFO.
     */
    private static final int MAX_SIZE_INFO = 200;

    /**
     * The constant MAX_SIZE_URL.
     */
    private static final int MAX_SIZE_URL = 80;

    /**
     * The constant MAX_SIZE_REVIEW.
     */
    private static final int MAX_SIZE_REVIEW = 1000;

    /**
     * The constant MAX_SIZE_MESSAGE.
     */
    private static final int MAX_SIZE_MESSAGE = 500;

    /**
     * The constant MAX_SIZE_EMAIL.
     */
    private static final int MAX_SIZE_EMAIL = 30;

    /**
     * The constant MAX_SIZE_FIRST_NAME.
     */
    private static final int MAX_SIZE_FIRST_NAME = 10;

    /**
     * The constant MAX_SIZE_LAST_NAME.
     */
    private static final int MAX_SIZE_LAST_NAME = 10;

    /**
     * The constant MAX_SIZE_CITY.
     */
    private static final int MAX_SIZE_CITY = 10;

    /**
     * The constant MAX_SIZE_COUNTRY.
     */
    private static final int MAX_SIZE_COUNTRY = 10;

    /**
     * The constant MAX_SIZE_SEX.
     */
    private static final int MAX_SIZE_SEX = 10;

    /**
     * The constant MAX_SIZE_USER_INFO.
     */
    private static final int MAX_SIZE_USER_INFO = 200;


    /**
     * Check data update info car boolean.
     *
     * @param cartName    the cart name
     * @param carModel    the car model
     * @param carInfo     the car info
     * @param priceString the price string
     * @return the boolean
     */
    public boolean checkDataUpdateInfoCar(String cartName, String carModel, String carInfo, String priceString) {
        boolean result = false;
        int price;
        try{
            price=Integer.parseInt(priceString);
        }catch (NumberFormatException e){
            return false;
        }

        if (ValidatorType.CAR_MODEL.check(carModel) && ValidatorType.CAR_NAME.check(cartName)
                && (ValidatorType.TEXT_DATA.check(carInfo) | carInfo.isEmpty()) && price >= 0) {
            if (cartName.length() < MAX_SIZE_NAME && carModel.length() < MAX_SIZE_NAME &&
                    carInfo.length() < MAX_SIZE_INFO && price < Integer.MAX_VALUE) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Check data car boolean.
     *
     * @param name  the name
     * @param model the model
     * @param info  the info
     * @return the boolean
     */
    public boolean checkDataCar(String name, String model, String info) {
        boolean result = false;
        if (ValidatorType.CAR_MODEL.check(model) && ValidatorType.CAR_NAME.check(name)
                && (ValidatorType.TEXT_DATA.check(info) || info.isEmpty())) {
            if (name.length() < MAX_SIZE_NAME && model.length() < MAX_SIZE_NAME && info.length() < MAX_SIZE_INFO) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Check data review boolean.
     *
     * @param review the review
     * @return the boolean
     */
    public boolean checkDataReview(String review) {
        boolean result = false;
        if (ValidatorType.TEXT_DATA.check(review)) {
            if (review.length() < MAX_SIZE_REVIEW) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Check data social network boolean.
     *
     * @param url the url
     * @return the boolean
     */
    public boolean checkDataSocialNetwork(String url) {
        boolean result = false;
        if (ValidatorType.URL.check(url)) {
            if (url.length() < MAX_SIZE_URL) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Check data change password boolean.
     *
     * @param login           the login
     * @param currentPassword the current password
     * @param newPassword     the new password
     * @return the boolean
     */
    public boolean checkDataChangePassword(String login, String currentPassword, String newPassword) {
        boolean result = false;

        if (ValidatorType.TEXT_DATA.check(login) && ValidatorType.TEXT_DATA.check(currentPassword) &&
                ValidatorType.TEXT_DATA.check(newPassword)) {
            if (login.length() < MAX_SIZE_NAME && currentPassword.length() < MAX_SIZE_PASSWORD &&
                    newPassword.length() < MAX_SIZE_PASSWORD) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Check data login boolean.
     *
     * @param login    the login
     * @param password the password
     * @return the boolean
     */
    public boolean checkDataLogin(String login, String password) {
        boolean result = false;
        if (ValidatorType.LOGIN.check(login) && ValidatorType.TEXT_DATA.check(password)) {
            result = true;
        }
        return result;
    }

    /**
     * Check message boolean.
     *
     * @param textmessage the textmessage
     * @return the boolean
     */
    public boolean checkMessage(String textmessage) {
        boolean result = false;
        if (ValidatorType.TEXT_DATA.check(textmessage)) {
            if (textmessage.length() < MAX_SIZE_MESSAGE) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Check register data boolean.
     *
     * @param login the login
     * @param email the email
     * @return the boolean
     */
    public boolean checkRegisterData(String login, String email) {
        boolean result = false;
        if (ValidatorType.LOGIN.check(login) && ValidatorType.EMAIL.check(email)) {
            if (login.length() < MAX_SIZE_NAME && email.length() < MAX_SIZE_EMAIL)
                result = true;
        }
        return result;
    }

    /**
     * Check update user info data boolean.
     *
     * @param firstName the first name
     * @param lastName  the last name
     * @param country   the country
     * @param city      the city
     * @param sex       the sex
     * @param textInfo  the text info
     * @return the boolean
     */
    public boolean checkUpdateUserInfoData(String firstName, String lastName, String country, String city, String sex, String textInfo) {
        boolean result = false;
        if (ValidatorType.FIRST_NAME.check(firstName) && ValidatorType.LAST_NAME.check(lastName) &&
                ValidatorType.COUNTRY.check(country) && ValidatorType.CITY.check(city) &&
                ValidatorType.SEX.check(sex) && ValidatorType.TEXT_DATA.check(textInfo)) {
            if (firstName.length() < MAX_SIZE_FIRST_NAME && lastName.length() < MAX_SIZE_LAST_NAME &&
                    country.length() < MAX_SIZE_COUNTRY && city.length() < MAX_SIZE_CITY &&
                    sex.length() < MAX_SIZE_SEX && textInfo.length() < MAX_SIZE_USER_INFO) {
                result = true;
            }

        }
        return result;
    }

}
