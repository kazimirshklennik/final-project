package by.epam.rentcar.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Enum ValidatorType.
 */
public enum ValidatorType {

	/**
	 * The email.
	 */
	EMAIL("^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$"),

	/**
	 * Text data validator type.
	 */
	TEXT_DATA("^[a-zA-Zа-яёЁА-Я0-9-_\\.\\(\\)\\:\\;\\%\\$\\@\\-\\+\\*\\,\\!\\?\\№()\\s]*$"),

	/**
	 * Url validator type.
	 */
	URL("^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$"),

	/**
	 * First name validator type.
	 */
	FIRST_NAME("^[a-zA-Zа-яёЁА-Я]*$"),


	/**
	 * Login validator type.
	 */
	LOGIN("^[0-9a-zA-Zа-яёЁА-Я]*$"),

	/**
	 * Last name validator type.
	 */
	LAST_NAME("^[a-zA-Zа-яёЁА-Я]*$"),

	/**
	 * Country validator type.
	 */
	COUNTRY("^[a-zA-Zа-яёЁА-Я]*$"),

	/**
	 * City validator type.
	 */
	CITY("^[a-zA-Zа-яёЁА-Я]*$"),

	/**
	 * Sex validator type.
	 */
	SEX("^[a-zA-Zа-яёЁА-Я]*$"),

	/**
	 * Car name validator type.
	 */
	CAR_NAME("^[a-zA-Zа-яёЁА-Я]*$"),

	/**
	 * Car model validator type.
	 */
	CAR_MODEL("^[a-zA-Zа-яёЁА-Я0-9-]*$");

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(ValidatorType.class);

	/** The pattern. */
	private Pattern pattern;
	
	/**
	 * Instantiates a new data validator.
	 *
	 * @param regex the regular expression
	 */
	ValidatorType(String regex) {
		this.pattern = Pattern.compile(regex);
	}

	/**
	 * Check.
	 *
	 * @param data the data
	 * @return true, if successful
	 */
	public boolean check(String data) {
		boolean value = false;
		if (data != null) {
			Matcher matcher = this.pattern.matcher(data);
			if (matcher.matches()) {
				value = true;
			} else {
				LOGGER.info("Validaton failed: " + this + " - '" + data + "'");
			}
		} else {
			LOGGER.info( "Validaton failed: " + this + " - null");
		}
		return value;
	}
}