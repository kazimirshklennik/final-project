package by.epam.rentcar.pool;

/**
 * The enum Data base resource manager type.
 */
public enum DataBaseResourceManagerType {

    /**
     * Db driver data base resource manager type.
     */
    DB_DRIVER("db.driver"),
    /**
     * Db url data base resource manager type.
     */
    DB_URL("db.url"),
    /**
     * Db login data base resource manager type.
     */
    DB_LOGIN("db.login"),
    /**
     * Db password data base resource manager type.
     */
    DB_PASSWORD("db.password"),
    /**
     * Path data base resource manager type.
     */
    PATH("database");

    private String value;

    DataBaseResourceManagerType(String value){
        this.value=value;
    }

    /**
     * Get value string.
     *
     * @return the string
     */
    public String getValue(){
        return value;
    }
}
