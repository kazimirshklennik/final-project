package by.epam.rentcar.pool;

import java.util.ResourceBundle;

/**
 * The type Data base resource manager.
 */
public class DataBaseResourceManager {

    private final static DataBaseResourceManager instance = new DataBaseResourceManager();

    private ResourceBundle jdbcProperties = ResourceBundle.getBundle(DataBaseResourceManagerType.PATH.getValue());

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DataBaseResourceManager getInstance() {
        return instance;
    }

    /**
     * Gets value.
     *
     * @param key the key
     * @return the value
     */
    public String getValue(String key) {
        return jdbcProperties.getString(key);
    }
}
