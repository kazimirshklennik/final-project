package by.epam.rentcar.pool;

import by.epam.rentcar.exception.PoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;

/**
 * The type Connection verifier.
 */
public class ConnectionVerifier extends TimerTask {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LogManager.getLogger(ConnectionVerifier.class);

    @Override
    public void run() {
        checkConnections(ConnectionPool.getInstance());
      }

    /**
     * Check connections.
     *
     * @param connectionPool the connection Pool
     */
    private void checkConnections(ConnectionPool connectionPool) {
        BlockingQueue<ConnectionProxy> connections = connectionPool.getPool();
        for (Connection connection : connections) {
            try {
                if (!connection.isValid(0)) {
                    connections.remove(connection);
                    if (!connection.isClosed()) {
                        connection.close();
                    }
                    connectionPool.createSingleConnection();
                }
            } catch (SQLException e) {
                LOGGER.error("Error while verifying connection pool", e);
            } catch (PoolException e) {
                LOGGER.error("Error while verifying connection pool", e);
            }
        }
    }
}
