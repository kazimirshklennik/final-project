package by.epam.rentcar.pool;

import by.epam.rentcar.exception.PoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The type Connection pool.
 */
public class ConnectionPool {

    /** The Constant LOGGER. */
    private static Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    /**
     * The constant instance.
     */
    private static ConnectionPool instance;

    /**
     * The constant POOL_SIZE.
     */
    private static final int POOL_SIZE = 20;

    /**
     * The constant DELAY.
     */
    private static final Long DELAY = 3L * 1000L;

    /**
     * The constant PERIOD.
     */
    private static final Long PERIOD = 3600L * 1000L;

    /**
     * The lock.
     */
    private static Lock lock = new ReentrantLock();

    /**
     * The Pool.
     */
    private static BlockingQueue<ConnectionProxy> pool;

    /**
     * The constant isCreatePool.
     */
    private static AtomicBoolean isCreatePool = new AtomicBoolean(false);

    /**
     * The constant isTakeConnection.
     */
    private static AtomicBoolean isTakeConnection = new AtomicBoolean(true);

    /**
     * Instantiates a new connection pool.
     *
     * @throws SQLException, PoolException
     *                       the connection pool exception
     */
    private ConnectionPool() throws PoolException {
        init();
        TimerTask connectionVerifier = new ConnectionVerifier();
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(connectionVerifier, DELAY, PERIOD);
    }

    /**
     * Represents double-checked singleton pattern that gets the only instance
     * of {@code ConnectionPool}.
     *
     * @return single instance of {@code ConnectionPool}.
     */
    public static ConnectionPool getInstance() {

        if (!isCreatePool.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    isCreatePool.set(true);
                }
            } catch (PoolException e) {
                LOGGER.warn("Error while Connection Pool initialization. Trying init again...", e);
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Inits the.
     *
     * @throws PoolException the connection pool exception
     */
    private void init() throws PoolException {
        try {
            registerDrivers();
            String url = DataBaseResourceManager.getInstance().getValue(DataBaseResourceManagerType.DB_URL.getValue());
            String login = DataBaseResourceManager.getInstance().getValue(DataBaseResourceManagerType.DB_LOGIN.getValue());
            String password = DataBaseResourceManager.getInstance().getValue(DataBaseResourceManagerType.DB_PASSWORD.getValue());
            pool = new ArrayBlockingQueue<>(POOL_SIZE);
            for (int i = 0; i <= POOL_SIZE; i++) {
                ConnectionProxy connection = new ConnectionProxy(DriverManager.getConnection(url, login, password));
                pool.offer(connection);
            }
        } catch (SQLException e) {
            throw new PoolException(e);
        }

    }

    /**
     * Create single connection.
     *
     * @throws PoolException the pool exception
     */
    protected void createSingleConnection() throws PoolException {

        ConnectionProxy connection;
        try {
            registerDrivers();
            String url = DataBaseResourceManager.getInstance().getValue(DataBaseResourceManagerType.DB_URL.getValue());
            String login = DataBaseResourceManager.getInstance().getValue(DataBaseResourceManagerType.DB_LOGIN.getValue());
            String password = DataBaseResourceManager.getInstance().getValue(DataBaseResourceManagerType.DB_PASSWORD.getValue());

            connection = new ConnectionProxy(DriverManager.getConnection(url, login, password));
        } catch (SQLException e) {
            throw new PoolException(e);
        }
        pool.offer(connection);
    }

    /**
     * Register drivers.
     */
    private void registerDrivers() {

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOGGER.fatal("Error while registering mysql driver", e);
            throw new RuntimeException("Error while registering mysql driver", e);
        }
    }

    /**
     * Deregister drivers.
     */
    private void deregisterDrivers() {
       /* DriverManager.getDrivers().asIterator().forEachRemaining(driver -> {
            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                LOGGER.error(String.format("Error while trying to deregister the %s driver", driver), e);
            }
        });*/ //fixme
    }

    /**
     * Gets an instance of {@code ConnectionImpl} from the pool using non-
     * blocking {@code poll()} method.
     *
     * @return an instance of {@code ConnectionImpl}.
     */
    public ConnectionProxy takeConnection() {

        ConnectionProxy connection = null;
        if (isTakeConnection.get()) {
            try {
                connection = pool.take();
            } catch (InterruptedException e) {
                LOGGER.error("InterruptedException in ConnectionPool class, takeConnection()", e);
            }
        }
        return connection;
    }

    /**
     * Gives back a non-null connection to the connection pool through the use
     * of {@code add()} method.
     *
     * @param connection is an instance of {@code ConnectionImpl}.
     */
    public void returnConnection(ConnectionProxy connection) {
        try {
            if (!connection.isClosed()) {
                connection.setAutoCommit(true);
                pool.offer(connection);
            }
        } catch (SQLException e) {
            LOGGER.error("SQLException in ConnectionPool class, returnConnection(Connection connection)", e);
        }
    }

    /**
     * Get pool blocking queue.
     *
     * @return the blocking queue
     */
    protected BlockingQueue<ConnectionProxy> getPool() {
        return this.pool;
    }

    /**
     * Shuts down the connection pool by closing all the connections in it. Uses
     * {@code sleep} method for compulsory delay before cleaning the pool.
     */
    public void cleanUp() {
        isTakeConnection = new AtomicBoolean(false);
        final int WAITING_RETURN_THREADS_TO_POOL = 50;
        try {
            TimeUnit.MILLISECONDS.sleep(WAITING_RETURN_THREADS_TO_POOL);
            Iterator<ConnectionProxy> iterator = pool.iterator();
            while (iterator.hasNext()) {
                ConnectionProxy connection = iterator.next();
                if (connection != null) {
                    connection.destruct();
                }
                iterator.remove();
            }
            deregisterDrivers();
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException in ConnectionPool class, cleanUp() method", e);
        } catch (SQLException e) {
            LOGGER.error("SQLException in ConnectionPool class , cleanUp method", e);
        }
    }
}














