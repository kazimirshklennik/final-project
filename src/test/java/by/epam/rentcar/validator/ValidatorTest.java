package by.epam.rentcar.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ValidatorTest {

    private static final Logger LOGGER = LogManager.getLogger(ValidatorTest.class);
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String NEGAVIVE_TEXT = "Negative test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";
    private Validator validator;

    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
        validator= new Validator();
    }

    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        validator=null;
    }

    @Test(groups = {POSITIVE}, dataProvider = "positiveDataRegisterValidator", dataProviderClass = DataForValidator.class)
    public void testRegisterValidatorPositive(String actual) {
        Assert.assertTrue(validator.checkRegisterData("login",actual));
        LOGGER.info(POSITIVE_TEST);
    }

    @Test(groups = {NEGATIVE},dataProvider = "negativeDataRegisterValidator", dataProviderClass = DataForValidator.class)
    public void testRegisterValidatorNegative(String actual){
        Assert.assertFalse(validator.checkRegisterData("login",actual));
        LOGGER.info(NEGAVIVE_TEXT);
    }

    @Test(groups = {POSITIVE}, dataProvider = "positiveDataMessageValidator", dataProviderClass = DataForValidator.class)
    public void testMessageValidatorPositive(String actual) {
        Assert.assertTrue(validator.checkMessage(actual));
        LOGGER.info(POSITIVE_TEST);
    }

    @Test(groups = {NEGATIVE},dataProvider = "negativeDataMessageValidator", dataProviderClass = DataForValidator.class)
    public void testMessageValidatorNegative(String actual){
        Assert.assertFalse(validator.checkMessage(actual));
        LOGGER.info(NEGAVIVE_TEXT);
    }

    @Test(groups = {POSITIVE}, dataProvider = "positiveDataUrlValidator", dataProviderClass = DataForValidator.class)
    public void testUrlValidatorPositive(String actual) {
        Assert.assertTrue(validator.checkDataSocialNetwork(actual));
        LOGGER.info(POSITIVE_TEST);
    }

    @Test(groups = {NEGATIVE},dataProvider = "negativeDataUrlValidator", dataProviderClass = DataForValidator.class)
    public void testUrlValidatorNegative(String actual){
        Assert.assertFalse(validator.checkDataSocialNetwork(actual));
        LOGGER.info(NEGAVIVE_TEXT);
    }


}
