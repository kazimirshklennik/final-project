package by.epam.rentcar.validator;

import org.testng.annotations.DataProvider;

public class DataForValidator {


    @DataProvider(name = "positiveDataRegisterValidator")
    public static Object[] providerPositiveActualEmail() {
        return new Object[]{
                "hander222@gmail.com",
                "myemail@mail.ru",
                "myemail1212_33@bk.ru",
                "myemail1212_33@inbox.ru"};
        }

    @DataProvider(name = "negativeDataRegisterValidator")
    public static Object[] providerNegativeActualEmail() {
        return new Object[]{
                "hander222gmail.com",
                "myemail@mail",
                "myemail1212_33",
                "myemail1212_33@inboxru"};
    }

    @DataProvider(name = "positiveDataMessageValidator")
    public static Object[] providerPositiveActualText() {
        return new Object[]{
                "Hi, how are you?",
                "The Working Party welcomed the publication of the " +
                        "consolidated text of the Agreement in English, " +
                        "rench and Russian languages.",
                "Britain is one of the most highly industrialised countries in " +
                        "the world: for every person employed in agriculture 12 are" +
                        " employed in industry. The original base of British industry was " +
                        "coal-mining, iron and steel and textiles. Today the most productive " +
                        "sectors include high-tech industries, chemicals, finance and the " +
                        "service sectors, especially banking, insurance and tourism.",

                "Birmingham developed engineering, chemicals, electronics and car manufacture." +
                        "Cambridge is famous for software engineering (making programs for computers)" +
                        "and bio-chemical and bio-genetic products. Cattle-farming is the speciality of" +
                        "the west of England, Northern Ireland and Scotland. Wheat and fruit are widely" +
                        "grown in the south-east of England. Near the east and north-east coast of England"};
    }


    @DataProvider(name = "negativeDataMessageValidator")
    public static Object[] providerNegativeActualText() {
        return new Object[]{
                "$(document).ready(function () {" +
                        "    $(\"#simple-account-dropdown > .account\").click(function () {" +
                        "        $(\"#simple-account-dropdown > .dropdown\").fadeToggle(\"fast\", function () {" +
                        "            if ($(this).css('display') == \"none\")" +
                        "                $(\"#simple-account-dropdown > .account\").removeClass(\"active\");" +
                        "            else" +
                        "                $(\"#simple-account-dropdown > .account\").addClass(\"active\");" +
                        "        });" +
                        "    });" +
                        "});",
                " function b(b) {" +
                        "        return this.each(function () {" +
                        "            var c = a(this), e = c.data(\"bs.alert\");" +
                        "            e || c.data(\"bs.alert\", e = new d(this)), \"string\" == typeof b && e[b].call(c)" +
                        "        })\n" +
                        "    }",
                "<script>$(document).ready(function(){" +
                        "    $('#action_menu_btn').click(function()" +
                        "        $('.action_menu').toggle();" +
                        "    });" +
                        "});</script>",
                "var $elem = this," +
                        "windowHeight = $(window).height();"};
    }

    @DataProvider(name = "positiveDataUrlValidator")
    public static Object[] providerPositiveActualUrl() {
        return new Object[]{
                "https://github.com/",
                "https://www.youtube.com/watch?v=pwfUgL20C",
                "https://www.facebook.com/campaign/landing.php",
                "https://www.google.com/search"};
    }


    @DataProvider(name = "negativeDataUrlValidator")
    public static Object[] providerNegativeActualUrl() {
        return new Object[]{
                "httpsgithub,com/",
                "https://wwwyoutube/watch?v=pwfUgL20C",
                "https://wwwfac",
                "https/www.google.com/search"};
    }

}

