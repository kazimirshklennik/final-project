package by.epam.rentcar.logic.social;

import by.epam.rentcar.entity.social.SocialNetwork;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.SocialNetworkLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.SocialNetworkLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.logic.user.UserLogicTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * The type Social network test.
 */
public class SocialNetworkTest {

    private static final Logger LOGGER = LogManager.getLogger(UserLogicTest.class);

    private UserLogic userLogic;
    private User user;
    private SocialNetworkLogic socialNetworkLogic;
    private SocialNetwork socialNetwork;
    private final String TEST_LOGIN = "test22";
    private final String TEST_EMAIL = "test@test.ru";
    private final String TEST_URL="http://test-url.by";
    private final String TEST_PASSWORD = "12345";
    private final String NEGATIVE_TEST = "Negative test is completed successfully";
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";

    /**
     * Sets up.
     */
    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
       socialNetworkLogic = new SocialNetworkLogicImpl();
       userLogic= new UserLogicImpl();
        try {
            userLogic.addUser(TEST_LOGIN, TEST_PASSWORD, TEST_EMAIL);
            user=userLogic.findUser(TEST_LOGIN);
            socialNetwork=socialNetworkLogic.create(user.getInfo().getIdUserInfo(),TEST_URL);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod setUp");
        }
    }

    /**
     * Tear down.
     */
    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        try {
            userLogic.delete(TEST_LOGIN);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod tearDown");
        }
        userLogic = null;
        socialNetwork=null;
        socialNetworkLogic = null;
    }

    /**
     * Test add url positive.
     */
    @Test(groups = {POSITIVE})
    public void testAddUrlPositive() {
        try {
            Long actual = socialNetworkLogic.add(socialNetwork);
            Long expected = 0L;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test add url negative.
     */
    @Test(groups = {NEGATIVE})
    public void testAddUrlNegative() {
        try {
            Long actual = socialNetworkLogic.add(null);
            Long expected = 0L;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test find all social network positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindAllSocialNetworkPositive() {
        try {
            socialNetworkLogic.add(socialNetwork);
            socialNetworkLogic.add(socialNetwork);
            socialNetworkLogic.add(socialNetwork);
            int actual = socialNetworkLogic.findAllSocialNetwork(user.getIdUser()).size();
            int expected = 3;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test delete social network positive.
     */
    @Test(groups = {POSITIVE})
    public void testDeleteSocialNetworkPositive() {
        try {
            Long social1=socialNetworkLogic.add(socialNetwork);
            Long social2=socialNetworkLogic.add(socialNetwork);
            Long social3=socialNetworkLogic.add(socialNetwork);
            socialNetworkLogic.deleteSocialNetwork(social1);
            socialNetworkLogic.deleteSocialNetwork(social2);
            socialNetworkLogic.deleteSocialNetwork(social3);

            int actual = socialNetworkLogic.findAllSocialNetwork(user.getIdUser()).size();
            int expected = 0;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test delete social network negative.
     */
    @Test(groups = {NEGATIVE})
    public void testDeleteSocialNetworkNegative() {
        try {
            boolean actual=socialNetworkLogic.deleteSocialNetwork(null);
            Assert.assertFalse(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }



}
