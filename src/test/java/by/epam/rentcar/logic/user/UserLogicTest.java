package by.epam.rentcar.logic.user;

import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.util.PasswordEncryption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * The type User logic test.
 */
public class UserLogicTest {

    private static final Logger LOGGER = LogManager.getLogger(UserLogicTest.class);

    /**
     * The Users.
     */
    List<User> users;
    private UserLogic userLogic;
    private User user;
    private final String TEST_LOGIN = "test22";
    private final String TEST_LOGIN_2 = "test";
    private final String TEST_EMAIL = "test@test.ru";
    private final String TEST_PASSWORD = "12345";
    private final String NEGATIVE_TEST = "Negative test is completed successfully";
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";

    /**
     * Sets up.
     */
    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
        userLogic = new UserLogicImpl();
        users= new ArrayList<>();
        user = new User();
    }

    /**
     * Tear down.
     */
    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        users = null;
        userLogic = null;
        user=null;
    }

    /**
     * Test create user positive.
     */
    @Test(groups = {POSITIVE})
    public void testCreateUserPositive() {
        PasswordEncryption passwordEncryption = new PasswordEncryption();
        String password = passwordEncryption.create(TEST_PASSWORD);

        try {
            boolean actual=userLogic.addUser(TEST_LOGIN, password, TEST_EMAIL);
            Assert.assertTrue(actual);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test create user negative.
     */
    @Test(groups = {NEGATIVE} )
    public void testCreateUserNegative() {
        PasswordEncryption passwordEncryption = new PasswordEncryption();
        String password = passwordEncryption.create(TEST_PASSWORD);
        try {
            boolean actual=userLogic.addUser(null, password, TEST_EMAIL);
            Assert.assertFalse(actual);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test find user positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindUserPositive() {
        try {
            user = userLogic.findUser(TEST_LOGIN);
            String actual=user.getLogin();
            String expected=TEST_LOGIN;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find user negative.
     */
    @Test(groups = {NEGATIVE})
    public void testFindUserNegative() {
        try {
            user = userLogic.findUser(TEST_LOGIN);
            String actual=user.getLogin();
            String expected=TEST_LOGIN_2;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test check user positive.
     */
    @Test(groups = {POSITIVE})
    public void testCheckUserPositive() {
        try {
            boolean actual=userLogic.checkLogin(TEST_LOGIN);
            Assert.assertTrue(actual);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test check user negative.
     */
    @Test(groups = {NEGATIVE})
    public void testCheckUserNegative() {
        try {
            boolean actual=userLogic.checkLogin(TEST_LOGIN_2);
            Assert.assertFalse(actual);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test check email positive.
     */
    @Test(groups = {POSITIVE})
    public void testCheckEmailPositive() {
        try {
            boolean actual=userLogic.checkEmail(TEST_EMAIL);
            Assert.assertTrue(actual);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test delete user positive.
     */
    @Test(groups = {POSITIVE})
    public void testDeleteUserPositive() {
        try {
            userLogic.delete(TEST_LOGIN);
            boolean actual=userLogic.checkLogin(TEST_LOGIN);
            Assert.assertFalse(actual);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find id user positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindIdUserPositive() {
        try {
            Long actual = userLogic.findIdUser(TEST_LOGIN);
            Long expected=-1L;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test lock user positive.
     */
    @Test(groups = {POSITIVE})
    public void testLockUserPositive() {
        try {
            user = userLogic.findUser(TEST_LOGIN);
            userLogic.lock(user.getIdUser());
            user = userLogic.findUser(TEST_LOGIN);
            boolean actual=user.isLockStatus();
            if (user.getIdUser() != null) {
                Assert.assertTrue(actual);
            } else {
                Assert.fail("Can not find in test database user with login");
            }
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Testun lock user positive.
     */
    @Test(groups = {POSITIVE})
    public void testunLockUserPositive() {
        try {
            user = userLogic.findUser(TEST_LOGIN);
            userLogic.unLock(user.getIdUser());
            user = userLogic.findUser(TEST_LOGIN);
            boolean actual=user.isLockStatus();
            if (user.getIdUser() != null) {
                Assert.assertFalse(actual);
            } else {
                Assert.fail("Can not find in test database user with login");
            }
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test set online user positive.
     */
    @Test(groups = {POSITIVE})
    public void testSetOnlineUserPositive() {
        try {
            user = userLogic.findUser(TEST_LOGIN);
            userLogic.setOnline(user.getIdUser());
            user = userLogic.findUser(TEST_LOGIN);
            boolean actual=user.isStatusOnline();
            if (user.getIdUser() != null) {
                Assert.assertTrue(actual);
            } else {
                Assert.fail("Can not find in test database user with login");
            }
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test set offline user positive.
     */
    @Test(groups = {POSITIVE})
    public void testSetOfflineUserPositive() {
        try {
            user = userLogic.findUser(TEST_LOGIN);
            userLogic.setOffline(user.getIdUser());
            user = userLogic.findUser(TEST_LOGIN);
            boolean actual=user.isStatusOnline();
            if (user.getIdUser() != null) {
                Assert.assertFalse(actual);
            } else {
                Assert.fail("Can not find in test database user with login");
            }
        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }


    /**
     * Test find all user positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindAllUserPositive() {
        try {
            int actual=userLogic.findAllUser().size();
            int expected=0;
            Assert.assertNotEquals(actual,expected);

        } catch (LogicException e) {
            Assert.fail();
        }
        LOGGER.info(POSITIVE_TEST);
    }

}
