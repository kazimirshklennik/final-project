package by.epam.rentcar.logic.review;

import by.epam.rentcar.entity.Car;
import by.epam.rentcar.entity.Review;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.ReviewLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.logic.impl.ReviewLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import by.epam.rentcar.logic.reserve.ReserveLogicTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * The type Review logic test.
 */
public class ReviewLogicTest {

    private static final Logger LOGGER = LogManager.getLogger(ReserveLogicTest.class);

    private ReviewLogic reviewLogic;
    private UserLogic userLogic;
    private CarLogic carLogic;
    private Review review;
    private Car car;
    private User user;
    private Long idCar;

    private final String TEST_CAR_NAME = "test-name";
    private final String TEST_CAR_MODEL = "test-model";
    private final String TEST_LOGIN = "test";
    private final String TEST_EMAIL = "test@test.ru";
    private final String TEST_PASSWORD = "12345";
    private final String TEST_TEXT = "test-text";
    private final String NEGATIVE_TEST = "Negative test is completed successfully";
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";

    /**
     * Sets up.
     */
    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
        carLogic = new CarLogicImpl();
        userLogic = new UserLogicImpl();
        reviewLogic = new ReviewLogicImpl();
        car = new Car(TEST_CAR_NAME, TEST_CAR_MODEL);
        try {
            idCar = carLogic.add(car);
            userLogic.addUser(TEST_LOGIN, TEST_EMAIL, TEST_PASSWORD);
            user = userLogic.findUser(TEST_LOGIN);
            review= reviewLogic.createReview(user.getIdUser(),idCar,TEST_TEXT);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod setUp");
        }

    }

    /**
     * Tear down.
     */
    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        try {
            userLogic.delete(TEST_LOGIN);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod tearDown");
        }
        review=null;
        user = null;
        carLogic = null;
        userLogic = null;
        idCar = null;
        car = null;
    }

    /**
     * Test add review positive.
     */
    @Test(groups = {POSITIVE})
    public void testAddReviewPositive() {
        try {
            Long actual = reviewLogic.addReview(review);
            Long expected = 0L;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test add review negative.
     */
    @Test(groups = {NEGATIVE})
    public void testAddReviewNegative() {
        try {
            Long actual = reviewLogic.addReview(null);
            Long expected = 0L;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test delete review positive.
     */
    @Test(groups = {POSITIVE})
    public void testDeleteReviewPositive() {
        try {
            Long idReview = reviewLogic.addReview(review);
            reviewLogic.deleteReview(idReview);
            int actual=reviewLogic.findAllReviewByIdCar(idCar).size();
            int expected = 0;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find all review positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindAllReviewPositive() {
        try {
            int actual=reviewLogic.findAllReviews().size();
            int expected = 0;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }
}
