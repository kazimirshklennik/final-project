package by.epam.rentcar.logic.message;

import by.epam.rentcar.entity.Message;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.MessageLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.MessageLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * The type Message logic test.
 */
public class MessageLogicTest {

    private static final Logger LOGGER = LogManager.getLogger(MessageLogicTest.class);

    private UserLogic userLogic;
    private MessageLogic messageLogic;
    private User sender;
    private User recipient;
    private Message message;
    private final String TEST_LOGIN_1 = "test11";
    private final String TEST_LOGIN_2 = "test22";
    private final String TEST_EMAIL_1 = "test1@test.ru";
    private final String TEST_EMAIL_2 = "test2@test.ru";
    private final String TEST_EMAIL_3 = "rent-car00@mail.ru";
    private final String TEST_PASSWORD = "12345";
    private final String TEST_TEXT = "test-text";
    private final String NEGATIVE_TEST = "Negative test is completed successfully";
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";

    /**
     * Sets up.
     */
    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
        userLogic = new UserLogicImpl();
        messageLogic = new MessageLogicImpl();

        try {
            userLogic.addUser(TEST_LOGIN_1, TEST_PASSWORD, TEST_EMAIL_1);
            userLogic.addUser(TEST_LOGIN_2, TEST_PASSWORD, TEST_EMAIL_2);
            sender = userLogic.findUser(TEST_LOGIN_1);
            recipient = userLogic.findUser(TEST_LOGIN_2);
            message = messageLogic.createMessage(sender.getLogin(), recipient.getLogin(), TEST_TEXT);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod setUp");
        }
    }

    /**
     * Tear down.
     */
    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        try {
            userLogic.delete(TEST_LOGIN_1);
            userLogic.delete(TEST_LOGIN_2);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod tearDown");
        }
        userLogic = null;
        messageLogic = null;
        sender = null;
        recipient = null;
        message = null;
    }

    /**
     * Test send message positive.
     */
    @Test(groups = {POSITIVE})
    public void testSendMessagePositive() {
        try {
            Long actual = messageLogic.sendMessage(message);
            Long expected = 0L;
            Assert.assertNotEquals(actual, expected);
            messageLogic.deleteMessage(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test send message negative.
     */
    @Test(groups = {NEGATIVE})
    public void testSendMessageNegative() {
        try {
            Long actual = messageLogic.sendMessage(null);
            Long expected = 0L;
            Assert.assertEquals(actual, expected);
            messageLogic.deleteMessage(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test find all message positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindAllMessagePositive() {
        try {
            messageLogic.sendMessage(message);
            messageLogic.sendMessage(message);
            int actual = messageLogic.findAll(recipient.getIdUser()).size();
            Long expected = 2L;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find chat positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindChatPositive() {
        try {
            messageLogic.sendMessage(message);
            messageLogic.sendMessage(message);
            int actual = messageLogic.findChat(sender.getIdUser(),recipient.getIdUser()).size();
            Long expected = 2L;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test send email positive.
     */
    @Test(groups = {POSITIVE})
    public void testSendEmailPositive() {
        try {
            message.setEmailRecipient(TEST_EMAIL_3);
           boolean actual = messageLogic.sendEmail(message);
            Assert.assertTrue(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test send email negative.
     *
     * @throws LogicException the logic exception
     */
    @Test(groups = {NEGATIVE}, expectedExceptions = LogicException.class)
    public void testSendEmailNegative() throws LogicException {
        message.setEmailRecipient(TEST_EMAIL_2);
        messageLogic.sendEmail(message);
        LOGGER.info(NEGATIVE_TEST);
    }

}
