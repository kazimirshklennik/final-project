package by.epam.rentcar.logic.reserve;

import org.testng.annotations.DataProvider;

/**
 * The type Data for reserve logic.
 */
public class DataForReserveLogic {

    /**
     * Provider actual date object [ ] [ ].
     *
     * @return the object [ ] [ ]
     */
    @DataProvider(name = "date")
    public static Object[][] providerActualDate() {
        return new Object[][]{
                {"2020-06-20","2020-06-22"},
                {"2020-06-21","2020-06-25"},
                {"2020-06-03","2020-07-10"}
        };
    }
}
