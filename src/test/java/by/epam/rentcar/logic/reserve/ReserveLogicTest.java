package by.epam.rentcar.logic.reserve;

import by.epam.rentcar.entity.Car;
import by.epam.rentcar.entity.Reserve;
import by.epam.rentcar.entity.user.User;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.ReserveLogic;
import by.epam.rentcar.logic.UserLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import by.epam.rentcar.logic.impl.ReserveLogicImpl;
import by.epam.rentcar.logic.impl.UserLogicImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ReserveLogicTest {

    private static final Logger LOGGER = LogManager.getLogger(ReserveLogicTest.class);

    private ReserveLogic reserveLogic;
    private UserLogic userLogic;
    private CarLogic carLogic;
    private Reserve reserve;
    private Car car;
    private User user;
    private Long idCar;

    private final String TEST_CAR_NAME = "test-name";
    private final String TEST_CAR_MODEL = "test-model";
    private final String TEST_LOGIN = "test";
    private final String TEST_EMAIL = "test@test.ru";
    private final String TEST_PASSWORD = "12345";
    private final String TEST_DATE_BOOKING = "2019-08-01";
    private final String TEST_DATE_RETURN = "2019-08-05";
    private final String NEGATIVE_TEST = "Negative test is completed successfully";
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";

    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
        carLogic = new CarLogicImpl();
        userLogic = new UserLogicImpl();
        reserveLogic = new ReserveLogicImpl();
        car = new Car(TEST_CAR_NAME, TEST_CAR_MODEL);
        try {
            idCar = carLogic.add(car);
            userLogic.addUser(TEST_LOGIN, TEST_EMAIL, TEST_PASSWORD);
            user = userLogic.findUser(TEST_LOGIN);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod setUp");
        }
        reserve = reserveLogic.createReserve(TEST_DATE_BOOKING, TEST_DATE_RETURN, idCar, user.getIdUser());
    }

    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        try {
            userLogic.delete(TEST_LOGIN);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            LOGGER.error("LogicException in metod tearDown");
        }
        user = null;
        reserve = null;
        carLogic = null;
        userLogic = null;
        idCar = null;
        car = null;
    }

    @Test(groups = {POSITIVE})
    public void testReserveCarPositive() {
        try {
            Long actual = reserveLogic.reserveCar(reserve);
            Long expected = 0L;
            Assert.assertNotEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    @Test(groups = {NEGATIVE})
    public void testReserveCarNegative() {
        try {
            Long actual = reserveLogic.reserveCar(null);
            Long expected = 0L;
            Assert.assertEquals(actual, expected);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    @Test(groups = {POSITIVE})
    public void testCheckActiveReservePositive() {
        try {
            reserveLogic.reserveCar(reserve);
            boolean actual = reserveLogic.checkActiveReserveByIdUser(user.getIdUser());
            Assert.assertTrue(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException", e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    @Test(groups = {POSITIVE}, dataProvider = "date", dataProviderClass = DataForReserveLogic.class)
    public void testCheckDatePositive(String dateBooking,String dateReturn) {
        boolean actual = reserveLogic.checkDate(dateBooking, dateReturn);
        Assert.assertTrue(actual);
        LOGGER.info(POSITIVE_TEST);
    }


}
