package by.epam.rentcar.logic.car;

import by.epam.rentcar.entity.Car;
import by.epam.rentcar.exception.LogicException;
import by.epam.rentcar.logic.CarLogic;
import by.epam.rentcar.logic.impl.CarLogicImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Car logic test.
 */
public class CarLogicTest {

    private static final Logger LOGGER = LogManager.getLogger(CarLogicTest.class);

    /**
     * The Cars.
     */
    List<Car> cars;
    private CarLogic carLogic;
    private Car car;
    private Long idCar;
    private final String TEST_CAR_NAME = "test-name";
    private final String TEST_CAR_NAME_2 = "test-name2";
    private final String TEST_CAR_MODEL = "test-model";
    private final String NEGATIVE_TEST = "Negative test is completed successfully";
    private final String POSITIVE_TEST = "Positive test is completed successfully";
    private final String POSITIVE = "positive_test";
    private final String NEGATIVE = "negative_test";

    /**
     * Sets up.
     */
    @BeforeClass(groups = {POSITIVE, NEGATIVE})
    public void setUp() {
        carLogic = new CarLogicImpl();
        cars = new ArrayList<>();
        car = new Car(TEST_CAR_NAME,TEST_CAR_MODEL);
    }

    /**
     * Tear down.
     */
    @AfterClass(groups = {POSITIVE, NEGATIVE})
    public void tearDown() {
        cars = null;
        carLogic = null;
        car=null;
    }

    /**
     * Test add car positive.
     */
    @Test(groups = {POSITIVE})
    public void testAddCarPositive() {
        try {
            Long actual=carLogic.add(car);
            Long expected=0L;
            Assert.assertNotEquals(actual,expected);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test check car positive.
     */
    @Test(groups = {POSITIVE})
    public void testCheckCarPositive() {
        try {
            idCar=carLogic.add(car);
            boolean actual=carLogic.checkCar(idCar);
            Assert.assertTrue(actual);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test check car negative.
     */
    @Test(groups = {NEGATIVE})
    public void testCheckCarNegative() {
        try {
            boolean actual=carLogic.checkCar(idCar);
            Assert.assertFalse(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test add car negative.
     */
    @Test(groups = {NEGATIVE})
    public void testAddCarNegative() {
        try {
            Long actual=carLogic.add(null);
            Long expected=0L;
            Assert.assertEquals(actual,expected);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test delete car positive.
     */
    @Test(groups = {NEGATIVE})
    public void testDeleteCarPositive() {
        try {
            idCar=carLogic.add(car);
            carLogic.deleteCar(idCar);
            Long actual=carLogic.findCarById(idCar).getIdCar();
            Assert.assertNull(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find all car positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindAllCarPositive() {
        try {
           int actual=carLogic.findAllCar().size();
           int expected=0;
            Assert.assertNotEquals(actual,expected);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find car by id positive.
     */
    @Test(groups = {POSITIVE})
    public void testFindCarByIdPositive() {
        try {
            idCar=carLogic.add(car);
            Long actual=carLogic.findCarById(idCar).getIdCar();
            Assert.assertNotNull(actual);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test find car by id negative.
     */
    @Test(groups = {NEGATIVE})
    public void testFindCarByIdNegative() {
        try {
            Long actual=carLogic.findCarById(idCar).getIdCar();
            Assert.assertNull(actual);
        } catch (LogicException e) {
            Assert.fail("LogicException",e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }

    /**
     * Test update car positive.
     */
    @Test(groups = {POSITIVE})
    public void testUpdateCarPositive() {
        try {
            idCar=carLogic.add(car);
            car=carLogic.findCarById(idCar);
            car.setName(TEST_CAR_NAME_2);
            carLogic.updateCar(car,null);
            car=carLogic.findCarById(idCar);
            String actual=car.getName();
            String expected=TEST_CAR_NAME_2;
            Assert.assertEquals(actual,expected);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            try {
                carLogic.deleteCar(idCar);
            } catch (LogicException ex) {
                Assert.fail("LogicException",ex);
            }
            Assert.fail("LogicException",e);
        }
        LOGGER.info(POSITIVE_TEST);
    }

    /**
     * Test update car negative.
     */
    @Test(groups = {NEGATIVE})
    public void testUpdateCarNegative() {
        try {
            idCar=carLogic.add(car);
            car=carLogic.findCarById(idCar);
            car.setName("");
            carLogic.updateCar(null,null);
            car=carLogic.findCarById(idCar);
            String actual=car.getName();
            String expected=TEST_CAR_NAME;
            Assert.assertEquals(actual,expected);
            carLogic.deleteCar(idCar);
        } catch (LogicException e) {
            try {
                carLogic.deleteCar(idCar);
            } catch (LogicException ex) {
                Assert.fail("LogicException",e);
            }
            Assert.fail("LogicException",e);
        }
        LOGGER.info(NEGATIVE_TEST);
    }
}
