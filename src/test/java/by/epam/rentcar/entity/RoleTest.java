package by.epam.rentcar.entity;

import by.epam.rentcar.entity.test.RoleEntityTest;
import service.RoleService;
import config.DbUnitConfig;
import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

public class RoleTest extends DbUnitConfig {

    private RoleService service = new RoleService();
    private EntityManager em = Persistence.createEntityManagerFactory("DBUnitEx").createEntityManager();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        beforeData = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("by.epam.rentcar/entity/role/role-data.xml"));

        tester.setDataSet(beforeData);
        tester.onSetup();
    }

    public RoleTest(String role) {
        super(role);
    }

    @Test
    public void testGetAll() throws Exception {
        List<RoleEntityTest> roles = service.getAll();

        IDataSet expectedData = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("by.epam.rentcar/entity/role/role-data.xml"));

        //IDataSet actualData = tester.getConnection().createDataSet();
        //Assertion.assertEquals(expectedData, actualData);

        Assert.assertEquals(expectedData.getTable("role").getRowCount(),roles.size());
    }

    @Test
    public void testSave() throws Exception {
        RoleEntityTest role = new RoleEntityTest();
        role.setRole("admintest");
        service.save(role);
        service.save(role);

        IDataSet expectedData = new FlatXmlDataSetBuilder().build(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("by.epam.rentcar/entity/role/role-data-save.xml"));

        IDataSet actualData = tester.getConnection().createDataSet();

        String[] ignore = {"id_role"};
        Assertion.assertEqualsIgnoreCols(expectedData, actualData, "role", ignore);
    }

    @Test
    public void testDelete() throws Exception {
        RoleEntityTest role = new RoleEntityTest();
        role.setRole("admintest");
        role = em.merge( role);
       int id = role.getId_role();
        service.delete(role);
        RoleEntityTest roleFromDb = em.find( RoleEntityTest.class,id);
        Assert.assertNull(roleFromDb);
    }
}
