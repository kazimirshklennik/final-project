package by.epam.rentcar.util.url;

import by.epam.rentcar.util.PasswordEncryption;
import by.epam.rentcar.util.UrlParser;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UrlParserTest {

    @Test(dataProvider = "dataUrlParser", dataProviderClass = DataForUrlParser.class)
    public void testCreateStringDate(String expected, String actual) {
        UrlParser urlParser= new UrlParser();
        Assert.assertEquals(urlParser.parseSocialNetwork(actual).name(), expected);
    }
}
