package by.epam.rentcar.util.url;

import org.testng.annotations.DataProvider;

public class DataForUrlParser {

    @DataProvider(name = "dataUrlParser")
    public static Object[][] providerActualDataEncription() {
        return new Object[][]{
                {"VK_COM", "https://vk.com/id78855768"},
                {"GOOGLE", "https://get.google.com/albumarchive/101795470"},
                {"INSTAGRAM", "https://www.instagram.com"},
                {"TWITTER", "https://twitter.com/Kazimir67205077"},
                {"UNKNOWN", " "},
                {"UNKNOWN", ""},
                {"UNKNOWN", null},
        };
    }
}
