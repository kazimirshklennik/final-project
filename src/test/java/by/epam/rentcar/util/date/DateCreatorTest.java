package by.epam.rentcar.util.date;

import by.epam.rentcar.util.DateCreator;
import by.epam.rentcar.util.DateType;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * The type Date creator test.
 */
public class DateCreatorTest {

    /**
     * Test create string date.
     *
     * @param expected the expected
     * @param actual   the actual
     */
    @Test(dataProvider = "dataStringCreator", dataProviderClass = DataForDateCreator.class)
    public void testCreateStringDate(String expected, String actual) {
        Long date=Long.parseLong(actual);
        DateCreator dateCreator= new DateCreator();
        Assert.assertEquals(dateCreator.create(date, DateType.DATE), expected);
    }

    /**
     * Test create long date.
     *
     * @param expected the expected
     * @param actual   the actual
     */
    @Test(dataProvider = "dataLongCreator", dataProviderClass = DataForDateCreator.class)
    public void testCreateLongDate(String expected, String actual) {
        DateCreator dateCreator= new DateCreator();
        Assert.assertEquals(dateCreator.createLong(actual).toString(), expected);
    }

    /**
     * Test age.
     *
     * @param expected the expected
     * @param actual   the actual
     */
    @Test(dataProvider = "dataAge", dataProviderClass = DataForDateCreator.class)
    public void testAge(String expected, String actual) {
        DateCreator dateCreator= new DateCreator();
        Assert.assertEquals(String.valueOf(dateCreator.getAge(actual)), expected);
    }
}
