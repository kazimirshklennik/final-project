package by.epam.rentcar.util.date;

import org.testng.annotations.DataProvider;

/**
 * The type Data for date creator.
 */
public class DataForDateCreator {

    @DataProvider(name = "dataStringCreator")
    public static Object[][] providerActualStringCreator() {
        return new Object[][]{
                {"2019-06-20","1561039343228"},
                {"2019-06-21","1561124067677"},
                {"2019-07-03","1562103765005"}
        };
    }

    @DataProvider(name = "dataLongCreator")
    public static Object[][] providerActualLongCreator() {
        return new Object[][]{
                {"1560978000000","2019-06-20"},
                {"1561064400000","2019-06-21"},
                {"1562101200000","2019-07-03"}
        };
    }

    @DataProvider(name = "dataAge")
    public static Object[][] providerActualAge() {
        return new Object[][]{

                {"26","1993-06-24"},
                {"36","1983-07-14"},
                {"41","1978-07-07"}
        };
    }

}
