package by.epam.rentcar.util.password;

import org.testng.annotations.DataProvider;

/**
 * The type Data for password encription.
 */
public class DataForPasswordEncription {

    /**
     * Provider actual data encription object [ ] [ ].
     *
     * @return the object [ ] [ ]
     */
    @DataProvider(name = "dataPaddwordEncriprion")
    public static Object[][] providerActualDataEncription() {
        return new Object[][]{
                {"cb45c671cbc500627ea424eea5f91996221b5935","qazwsx"},
                {"283d47a9338ed1100b5fe2a5aff2d1f7c799bfd0","newPassword"},
                {"1b307acba4f54f55aafc33bb06bbbf6ca803e9a","1234567890"},
                {"b858cb282617fb0956d960215c8e84d1ccf909c6"," "}
        };
    }
}
