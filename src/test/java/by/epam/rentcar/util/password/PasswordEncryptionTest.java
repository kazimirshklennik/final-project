package by.epam.rentcar.util.password;

import by.epam.rentcar.util.PasswordEncryption;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * The type Password encryption test.
 */
public class PasswordEncryptionTest {

    /**
     * Test create string date.
     *
     * @param expected the expected
     * @param actual   the actual
     */
    @Test(dataProvider = "dataPaddwordEncriprion", dataProviderClass = DataForPasswordEncription.class)
    public void testCreateStringDate(String expected, String actual) {
        PasswordEncryption passwordEncryption= new PasswordEncryption();
        Assert.assertEquals(passwordEncryption.create(actual).toString(), expected);
    }
}
