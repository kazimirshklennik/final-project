package service;

import by.epam.rentcar.entity.test.RoleEntityTest;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class RoleService {
    private EntityManager em = Persistence.createEntityManagerFactory("DBUnitEx").createEntityManager();

    public void save(RoleEntityTest role){
        em.getTransaction().begin();
        em.persist(role);
        em.getTransaction().commit();
    }

    public void delete(RoleEntityTest role) {
        em.getTransaction().begin();
        em.remove(role);
        em.getTransaction().commit();
    }

    public RoleEntityTest get(int id) {
        return em.find(RoleEntityTest.class, id);
    }

    public void update(RoleEntityTest role) {
        em.getTransaction().begin();
        em.merge(role);
        em.getTransaction().commit();
    }

    public List<RoleEntityTest> getAll() {
        TypedQuery<RoleEntityTest> namedQuery = em.createNamedQuery("DbUnitRoleEntity.getAll", RoleEntityTest.class);

        return namedQuery.getResultList();
    }

}
